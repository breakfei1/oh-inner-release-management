
 OpenHarmony社区版本发布计划





# OpenHarmony社区版本发布计划：

| **迭代计划** | **版本**号             | **版本构建** | **版本转测试** | **版本测试完成** |
| ------------ | ---------------------- | ------------ | -------------- | ---------------- |
| IT1（API Level：6）          | OpenHarmony 2.2.0.1  | 2021/4/21 | 2021/4/21      | 2021/5/11        |
|              | OpenHarmony 2.2.0.2  | 2021/5/12    | 2021/5/12      | 2021/5/18        |
|              | OpenHarmony 2.2.0.3 | 2021/5/19    | 2021/5/19      | 2021/5/25 |
|              | OpenHarmony 2.2.0.5 | 2021/6/2 | 2021/6/2 | **2021/6/7** |
|              | OpenHarmony 2.3.0.1 | 2021/6/23   | 2021/6/23     | 2021/7/6      |
|              | OpenHarmony 2.3.0.2 | 2021/7/7  | 2021/7/7    | 2021/7/13     |
|              | OpenHarmony 2.3.0.3 | 2021/7/14 | 2021/7/14   | 2021/7/20 |
|              | OpenHarmony 2.2 Beta2 | 2021/7/21 | 2021/7/21 | **2021/7/30** |
| IT2（API Level：7）          | OpenHarmony 3.0.0.1 | 2021/7/28   | 2021/7/28     | 2021/8/3         |
|              | OpenHarmony 3.0.0.2 | 2021/8/4     | 2021/8/4       | 2021/8/10        |
|              | OpenHarmony 3.0.0.3 | 2021/8/11  | 2021/8/11    | 2021/8/17       |
|              | OpenHarmony 3.0 Beta1 | 2021/8/19   | 2021/8/19     | 2021/8/24      |
|              | OpenHarmony 3.0.0.6 | 2021/8/25   | 2021/8/25     | 2021/9/7        |
|              | OpenHarmony 3.0.0.7 | 2021/9/8    | 2021/9/8      | 2021/9/14        |
|              | OpenHarmony 3.0.0.8 | 2021/9/15   | 2021/9/15     | 2021/9/21   |
|              | OpenHarmony 3.0 LTS | 2021/9/22  | 2021/9/22 | **2021/9/28** |
| IT3（API Level：8）          | OpenHarmony 3.1.0.1   | 2021/10/20    | 2021/10/20      | 2021/11/2       |
|              | OpenHarmony 3.1.0.2   | 2021/10/27   | 2021/10/27     | 2021/11/2       |
|              | OpenHarmony 3.1.1.1   | 2021/11/3   | 2021/11/3     | 2021/11/9         |
|              | OpenHarmony 3.1.1.2 | 2021/11/10    | 2021/11/10      | **2021/11/16**    |
|              | OpenHarmony 3.1.1.3   | 2021/11/17    | 2021/11/17      | 2021/11/23      |
|              | OpenHarmony 3.1.1.5   | 2021/11/24   | 2021/11/24     | 2021/11/30       |
|IT4（API Level：8）       | OpenHarmony 3.1.2.1   | 2021/12/1   | 2021/12/1     | 2021/12/6         |
|              | OpenHarmony 3.1.2.2 | 2021/12/8    | 2021/12/8      | 2021/12/13    |
|              | OpenHarmony 3.1.2.3 | 2021/12/15    | 2021/12/15     | 2021/12/20    |
|              | OpenHarmony 3.1.2.5(Beta) | 2021/12/22    | 2021/12/22     | **2021/12/31**    |
| IT5（API Level：8） | OpenHarmony 3.1.0.1          | 2021/10/20   | 2021/10/20     | 2021/11/2        |
|                     | OpenHarmony 3.1.0.2          | 2021/10/27   | 2021/10/27     | 2021/11/2        |
| IT6（API Level：8） | OpenHarmony 3.1.1.1          | 2021/11/3    | 2021/11/3      | 2021/11/9        |
|                     | OpenHarmony 3.1.1.2          | 2021/11/10   | 2021/11/10     | **2021/11/16**   |
|                     | OpenHarmony 3.1.1.3          | 2021/11/17   | 2021/11/17     | 2021/11/23       |
|                     | OpenHarmony 3.1.1.5          | 2021/11/24   | 2021/11/24     | 2021/11/30       |
| IT7（API Level：8） | OpenHarmony 3.1.2.1          | 2021/12/1    | 2021/12/1      | 2021/12/6        |
|                     | OpenHarmony 3.1.2.2          | 2021/12/8    | 2021/12/8      | 2021/12/13       |
|                     | OpenHarmony 3.1.2.3          | 2021/12/15   | 2021/12/15     | 2021/12/20       |
|                     | OpenHarmony 3.1.2.5(Beta)    | 2021/12/22   | 2021/12/22     | **2021/12/31**   |
| IT8（API Level：8） | OpenHarmony 3.1.3.1          | 2022/1/4     | 2022/1/5       | 2022/1/11        |
|                     | OpenHarmony 3.1.3.2          | 2022/1/11    | 2022/1/12      | 2022/1/18        |
|                     | OpenHarmony 3.1.3.3          | 2022/1/18    | 2022/1/19      | 2022/1/25        |
|                     | OpenHarmony 3.1.3.5          | 2022/1/25    | 2022/1/26      | **2022/2/11**    |
| IT9（API Level：8） | OpenHarmony 3.1.5.1          | 2022/2/15    | 2022/2/16      | 2022/2/22        |
|                     | OpenHarmony 3.1.5.2          | 2022/2/22    | 2022/2/23      | 2022/3/1         |
|                     | OpenHarmony 3.1.5.3          | 2022/3/1     | 2022/3/2       | 2022/3/8         |
|                     | OpenHarmony 3.1.5.5(Release) | 2022/3/8     | 2022/3/9       | **2022/3/15**    |

# 各版本特性交付清单：

## OpenHarmony 2.2.0.1版本特性清单：

针对OpenHarmony 2.2.0.1版本解决的缺陷ISSUE列表：

| ISSUE                                                        | 问题描述                                                     |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [I3I31W](https://e.gitee.com/open_harmony/unset_project?issue=I3I31W) | ActsNFSTest.bin会引起内核crash                               |
| [I3D49E](https://e.gitee.com/open_harmony/issues/list?issue=I3D49E) | uboot的路径不对                                              |
| [I3D71U](https://e.gitee.com/open_harmony/issues/list?issue=I3D71U) | 【驱动子系统】反复reset，启动到hmac_main_init SUCCESSULLY后，高概率出现系统挂死 |
| [I3DGZW](https://e.gitee.com/open_harmony/issues/list?issue=I3DGZW) | 【应用程序框架子系统】HI3516开源板进入屏保后 ，点击触摸屏，出现蓝屏问题 |
| [I3DHIL](https://e.gitee.com/open_harmony/issues/list?issue=I3DHIL) | 【系统问题】HI3518开源板剩余空间不足，导致ACTS用例大量失败   |
| [I3DU36](https://e.gitee.com/open_harmony/issues/list?issue=I3DU36) | 【应用程序框架子系统】ipcamera bm 查询命令失效               |
| [I3EALU](https://e.gitee.com/open_harmony/issues/list?issue=I3EALU) | 【媒体子系统】cameraActs 用例执行时，找不到相机配置文件，初始失败 |
| [I3EGUX](https://e.gitee.com/open_harmony/issues/list?issue=I3EGUX) | 【可靠性问题】反复reset，出现一次KIdle进程crash，系统挂死无法启动 |
| [I3EH4E](https://e.gitee.com/open_harmony/issues/list?issue=I3EH4E) | 【流水线问题】高概率出现：uname无响应，然后执行reset也无响应 |
| [I3EQJA](https://e.gitee.com/open_harmony/issues/list?issue=I3EQJA) | 【文件系统】cat /proc/mounts功能不可用                       |
| [I3EQRC](https://e.gitee.com/open_harmony/issues/list?issue=I3EQRC) | 磁盘文件映射延迟测试：并发3个测试进程，系统crash             |
| [I3HVL0](https://e.gitee.com/open_harmony/issues/list?issue=I3HVL0) | 3861编译失败，报错[OHOS ERROR] Fatal error: invalid -march= option:rv32imac |

## OpenHarmony 2.2.0.2版本特性清单：

状态说明：discussion(方案讨论，需求未接受)，Reject(未纳入版本)，developing(开发中)，Testing(测试中)，Accepted(已验收)

| no   | issue                                                        | feture description                           | platform     | sig            | owner                                         |
| :--- | ------------------------------------------------------------ | :------------------------------------------- | :--------- | :------------- | :-------------------------------------------- |
| 1    | [I3HX0V](https://gitee.com/openharmony/hiviewdfx_hilog_lite/issues/I3HX0V) | 【HiLog】L1系统HiLog功能增强                 | 轻量系统 | SIG_BscSoftSrv | [@shenchenkai](https://gitee.com/shenchenkai) |
| 2    | I3INEZ                                                       | 【AI子系统】AI引擎支持基于共享内存的数据传输 | 轻量系统 | SIG_AI         | [@armylee0](https://gitee.com/armylee0)       |

## OpenHarmony 2.2.0.3版本特性清单：

暂无

## OpenHarmony 2.2 Beta版本特性清单：

 状态说明：discussion(方案讨论，需求未接受)，Reject(未纳入版本)，developing(开发中)，Testing(测试中)，Accepted(已验收)

| no   | issue                                                        | feture description                                  | platform     | sig                  | owner                                   |
| :--- | ------------------------------------------------------------ | :-------------------------------------------------- | :--------- | :------------------- | :-------------------------------------- |
| 1    | [I3ICFO](https://gitee.com/openharmony/utils_native_lite/issues/I3ICFO) | 【分布式数据管理】提供数据库内容的删除能力          | 轻量系统 | SIG_DataManagement   | [@widecode](https://gitee.com/widecode) |
| 2    | [I3ICH0](https://gitee.com/openharmony/utils_native_lite/issues/I3ICH0) | 【分布式数据管理】提供统一的HAL文件系统操作函数实现 | 轻量系统 | SIG_DataManagement   | [@widecode](https://gitee.com/widecode) |
| 3    | [I3ICG4](https://gitee.com/openharmony/utils_native_lite/issues/I3ICG4) | 【分布式数据管理】提供相关数据存储的原子操作能力    | 轻量系统 | SIG_DataManagement   | [@widecode](https://gitee.com/widecode) |
| 4    | [I3ICGH](https://gitee.com/openharmony/utils_native_lite/issues/I3ICGH) | 【分布式数据管理】提供二进制Value的写入读取能力     | 轻量系统 | SIG_DataManagement   | [@widecode](https://gitee.com/widecode) |
| 5    | [I3NSPB](https://gitee.com/openharmony/graphic_ui/issues/I3NSPB) | 【轻量级图形】UIKit组件支持margin/padding           | 轻量系统 | SIG_GraphicsandMedia | [@niulihua](https://gitee.com/niulihua) |
| 7    | [I3NSZH](https://gitee.com/openharmony/graphic_ui/issues/I3NSZH) | 【轻量级图形】圆形/胶囊按钮支持缩放和白色蒙层动效   | 轻量系统 | SIG_GraphicsandMedia | [@niulihua](https://gitee.com/niulihua) |

## OpenHarmony 2.3.0.1版本特性清单：

状态说明：discussion(方案讨论，需求未接受)，Reject(未纳入版本)，developing(开发中)，Testing(测试中)，Accepted(已验收)

| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                               |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :-------------------------------------------------- |
| 1    | [I3NCKH](https://gitee.com/openharmony/kernel_liteos_a/issues/I3NCKH) | 【轻内核子系统】L0上支持基于NOR Flash的littlefs文件系统      | 轻量系统 | SIG_Kernel           | [@kkup180](https://gitee.com/kkup180)               |
| 2    | [I3NCTE](https://gitee.com/openharmony/kernel_liteos_a/issues/I3NCTE) | 【轻内核子系统】L0上对外提供统一的文件系统操作接口           | 轻量系统 | SIG_Kernel           | [@kkup180](https://gitee.com/kkup180)               |
| 3    | [I3NCX2](https://gitee.com/openharmony/kernel_liteos_a/issues/I3NCX2) | 【轻内核子系统】L0 补充120个POSIX接口                        | 轻量系统 | SIG_Kernel           | [@kkup180](https://gitee.com/kkup180)               |
| 4    | [I3NT2C](https://gitee.com/openharmony/kernel_liteos_a/issues/I3NT2C) | 【轻内核子系统】移植mksh命令解析器                           | 轻量系统 | SIG_Kernel           | [@kkup180](https://gitee.com/kkup180)               |
| 5    | [I3NT2K](https://gitee.com/openharmony/kernel_liteos_a/issues/I3NT2K) | 【轻内核子系统】shell交互友好性提升                          | 轻量系统 | SIG_Kernel           | [@kkup180](https://gitee.com/kkup180)               |
| 6    | [I3NT2V](https://gitee.com/openharmony/kernel_liteos_a/issues/I3NT2V) | 【轻内核子系统】移植toybox命令集                             | 轻量系统 | SIG_Kernel           | [@kkup180](https://gitee.com/kkup180)               |
| 7    | [I3NT4N](https://gitee.com/openharmony/kernel_liteos_a/issues/I3NT4N) | 【轻内核子系统】Namecache模块                                | 轻量系统 | SIG_Kernel           | [@kkup180](https://gitee.com/kkup180)               |
| 8    | [I3NT58](https://gitee.com/openharmony/kernel_liteos_a/issues/I3NT58) | 【轻内核子系统】Vnode管理                                    | 轻量系统 | SIG_Kernel           | [@kkup180](https://gitee.com/kkup180)               |
| 9    | [I3NT5Q](https://gitee.com/openharmony/kernel_liteos_a/issues/I3NT5Q) | 【轻内核子系统】Lookup模块                                   | 轻量系统 | SIG_Kernel           | [@kkup180](https://gitee.com/kkup180)               |
| 10   | [I3NT6H](https://gitee.com/openharmony/kernel_liteos_a/issues/I3NT6H) | 【轻内核子系统】文件系统维测增强                             | 轻量系统 | SIG_Kernel           | [@kkup180](https://gitee.com/kkup180)               |
| 11   | [I3NT6U](https://gitee.com/openharmony/kernel_liteos_a/issues/I3NT6U) | 【轻内核子系统】liteos-a內核模块可配置                       | 轻量系统 | SIG_Kernel           | [@kkup180](https://gitee.com/kkup180)               |
| 12   | [I3NT78](https://gitee.com/openharmony/kernel_liteos_a/issues/I3NT78) | 【轻内核子系统】liteos-a小系统三方芯片适配                   | 轻量系统 | SIG_Kernel           | [@kkup180](https://gitee.com/kkup180)               |
| 13   | [I3SNIP](https://gitee.com/openharmony/kernel_liteos_m/issues/I3SNIP) | 【轻内核子系统】L0支持三方组件Mbedtls编译                    | 轻量系统 | SIG_Kernel           | [@kkup180](https://gitee.com/kkup180)               |
| 14   | [I3SNKK](https://gitee.com/openharmony/kernel_liteos_m/issues/I3SNKK) | 【轻内核子系统】L0支持三方组件curl编译                       | 轻量系统 | SIG_Kernel           | [@kkup180](https://gitee.com/kkup180)               |
| 15  | [I3NIME](https://gitee.com/openharmony/startup_appspawn_lite/issues/I3NIME) | 【启动恢复子系统】支持恢复出厂设置                           | 轻量系统 | SIG_BscSoftSrv       | [@handyohos](https://gitee.com/handyohos)           |
| 16   | [I3NTBC](https://gitee.com/openharmony/startup_appspawn_lite/issues/I3NTBC) | 【启动恢复子系统】L0/L1/L2接口优化                           | 轻量系统 | SIG_BscSoftSrv       | [@handyohos](https://gitee.com/handyohos)           |
| 17   | [I3NN4H](https://gitee.com/openharmony/hiviewdfx_hiview_lite/issues/I3NN4H) | 【DFX子系统】【HiLog】L0系统HiLog功能增强                    | 轻量系统 | SIG_BscSoftSrv       | [@shenchenkai](https://gitee.com/shenchenkai)       |
| 18  | [I3NN53](https://gitee.com/openharmony/hiviewdfx_hiview_lite/issues/I3NN53) | 【DFX子系统】【HiEvent】L0系统HiEvent功能增强                | 轻量系统 | SIG_BscSoftSrv       | [@shenchenkai](https://gitee.com/shenchenkai)       |
| 19   | [I3ID9Q](https://gitee.com/openharmony/distributedschedule_dms_fwk_lite/issues/I3ID9Q) | 【分布式调度】建立轻量设备DMS与富设备DMS通信通道             | 轻量系统 | SIG_AppFramework     | [@lijiarun](https://gitee.com/lijiarun)             |
| 20  | [I3ID9V](https://gitee.com/openharmony/distributedschedule_dms_fwk_lite/issues/I3ID9V) | 【分布式调度】轻量设备启动富设备上的Ability                  | 轻量系统 | SIG_AppFramework     | [@lijiarun](https://gitee.com/lijiarun)             |
| 21  | [I3I1V8](https://gitee.com/openharmony/global_resmgr_lite/issues/I3I1V8)         | 【全球化子系统】构建应用资源解析和加载机制                   | 轻量系统 | SIG_AppFramework     | [@zhengbin5](https://gitee.com/zhengbin5)           |
| 22  | [I3I1VJ](https://gitee.com/openharmony/global_resmgr_lite/issues/I3I1VJ) | 【全球化子系统】构建资源回溯机制                             | 轻量系统 | SIG_AppFramework     | [@zhengbin5](https://gitee.com/zhengbin5)           |
| 23   | [I3QE85](https://gitee.com/openharmony/drivers_framework/issues/I3QE85) | 【驱动子系统】L0支持HDF框架                                  | 轻量系统 | SIG_DriverFramework  | [@zianed](https://gitee.com/zianed)                 |
| 24   | [I3NSVQ](https://gitee.com/openharmony/graphic_ui/issues/I3NSVQ) | 【轻量级图形】DFX维测能力：UIKit支持显示控件轮廓             | 轻量系统 | SIG_GraphicsandMedia | [@niulihua](https://gitee.com/niulihua)             |
| 25   | [I3NSWY](https://gitee.com/openharmony/graphic_ui/issues/I3NSWY) | 【轻量级图形】ScrollView/List支持通过弧形进度条展示滑动进度  | 轻量系统 | SIG_GraphicsandMedia | [@niulihua](https://gitee.com/niulihua)             |
| 26   | [I3NSZZ](https://gitee.com/openharmony/graphic_ui/issues/I3NSZZ) | 【轻量级图形】支持开关按钮/复选框/单选按钮动效               | 轻量系统 | SIG_GraphicsandMedia | [@niulihua](https://gitee.com/niulihua)             |
| 27   | [I3NSQ6](https://gitee.com/openharmony/graphic_ui/issues/I3NSQ6) | 【轻量级图形】UIKit支持点阵字体产品化解耦                    | 轻量系统 | SIG_GraphicsandMedia | [@niulihua](https://gitee.com/niulihua)             |
| 28   | [I3NSZ1](https://gitee.com/openharmony/graphic_ui/issues/I3NSZ1) | 【轻量级图形】UI框架提供统一多后端框架支持多芯片平台         | 轻量系统 | SIG_GraphicsandMedia | [@niulihua](https://gitee.com/niulihua)             |           |

## OpenHarmony 2.3.0.2 版本特性清单：	

状态说明：discussion(方案讨论，需求未接受)，Reject(未纳入版本)，developing(开发中)，Testing(测试中)，Accepted(已验收)

L0/L1需求列表:
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1    | [I3NCB9](https://gitee.com/openharmony/third_party_Linux_Kernel/issues/I3NCB9) | 【L1 Linux开源】编译器替换后的内核代码适配                   | 轻量系统 | SIG_Kernel           | [@zzzuo](https://gitee.com/zzzuo)                 |
| 2   | [I3NBVI](https://gitee.com/openharmony/build_lite/issues/I3NBVI) | 【L1 Linux开源】clang替换                                    | 轻量系统 | SIG_CompileRuntime   | [@zhuoli72](https://gitee.com/zhuoli72)           |
| 3   | [I3NC8H](https://gitee.com/openharmony/build_lite/issues/I3NC8H) | 【L1 Linux开源】musl库替换                                   | 轻量系统 | SIG_CompileRuntime   | [@zhuoli72](https://gitee.com/zhuoli72)           |
| 4    | [I3NCEF](https://gitee.com/openharmony/build_lite/issues/I3NCEF) | 【L1 Linux开源】工具集替换                                   | 轻量系统 | SIG_CompileRuntime   | [@taiyipei](https://gitee.com/taiyipei)           |
| 5   | [I3NCF7](https://gitee.com/openharmony/build_lite/issues/I3NCF7) | 【L1 Linux开源】rootfs替换                                   | 轻量系统 | SIG_CompileRuntime   | [@taiyipei](https://gitee.com/taiyipei)           |
| 6   | [I3NCG0](https://gitee.com/openharmony/build_lite/issues/I3NCG0) | 【L1 Linux开源】镜像制作工具替换                             | 轻量系统 | SIG_CompileRuntime   | [@taiyipei](https://gitee.com/taiyipei)           |
| 7   | [I3NCGM](https://gitee.com/openharmony/build_lite/issues/I3NCGM) | 【L1 Linux开源】开源编译构建                                 | 轻量系统 | SIG_CompileRuntime   | [@taiyipei](https://gitee.com/taiyipei)           |
| 8   | [I3NCCT](https://gitee.com/openharmony/drivers_adapter/issues/I3NCCT) | 【L1 Linux开源】编译器替换后的HDF适配                        | 轻量系统 | SIG_DriverFramework  | [@zianed](https://gitee.com/zianed)               |
| 9   | [I3N0LP](https://gitee.com/openharmony/global_i18n_lite/issues/I3N0LP?from=project-issue) | 【全球化子系统】构建自定义数据编译能力                       | 轻量系统 | SIG_AppFramework     | [@zhengbin5](https://gitee.com/zhengbin5)         |
| 10   | [I3N0OP](https://gitee.com/openharmony/global_i18n_lite/issues/I3N0OP?from=project-issue) | 【全球化子系统】构建星期、单复数、数字开关国际化能力         | 轻量系统 | SIG_AppFramework     | [@zhengbin5](https://gitee.com/zhengbin5)         |
| 11   | [I3NSY0](https://gitee.com/openharmony/graphic_ui/issues/I3NSY0) | 【轻量级图形】支持A4\A8、LUT8、TSC图片格式作为输入           | 轻量系统 | SIG_GraphicsandMedia | [@niulihua](https://gitee.com/niulihua)           |
| 12   | [I3NT0R](https://gitee.com/openharmony/graphic_ui/issues/I3NT0R) | 【轻量级图形】支持多语言字体对齐                             | 轻量系统 | SIG_GraphicsandMedia | [@niulihua](https://gitee.com/niulihua)           |
| 13   | [I3SNGO](https://gitee.com/openharmony/build_lite/issues/I3SNGO) | 【编译子系统】build_lite支持开源软件的通用patch框架          | 轻量系统 | SIG_CompileRuntime   | [@taiyipei](https://gitee.com/taiyipei)           |

L2需求列表：
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1 | [I3ZDIF](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDIF) | RQ-[Demo&应用子系统][JSUI]【通用】JS动画（动画样式、渐变样式、转场样式、自定义字体样式） | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 2 | [I3ZDIG](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDIG) | RQ-[Demo&应用子系统][JSUI]【通用】原子布局 | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 3 | [I3ZDIH](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDIH) | RQ-[Demo&应用子系统][JSUI]【容器组件】新事件提醒（badge） | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 4 | [I3ZDII](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDII) | RQ-[Demo&应用子系统][JAVA UI]【HarmonyOS】自定义组件（CustomComponent） | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 5 | [I3ZDIJ](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDIJ) | RQ-[Demo&应用子系统][JAVA UI]【HarmonyOS】自定义布局（CustomLayout） | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 6 | [I3ZDIK](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDIK) | RQ-[Demo&应用子系统][JS UI]【HarmonyOS】FA卡片（JsFACard） | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 7 | [I3ZDIL](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDIL) | RQ-[Demo&应用子系统][多模]【HarmonyOS】多模输入（MultiModeInput） | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 8 | [I3ZDIM](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDIM) | RQ-[Demo&应用子系统][多模]【HarmonyOS】多模输入事件标准化（MultimodalEvent） | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 9 | [I3ZDIN](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDIN) | RQ-[Demo&应用子系统][JAVA UI]【HarmonyOS】Fraction | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 10 | [I3ZDIO](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDIO) | RQ-[Demo&应用子系统][ServiceAbility]【HarmonyOS】前台服务（ForegroundService） | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 11 | [I3ZDIP](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDIP) | RQ-[Demo&应用子系统][Ability测试]【HarmonyOS】Ability的自动化测试（Delegator） | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 12 | [I3ZDIQ](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDIQ) | RQ-[Demo&应用子系统][无障碍]【HarmonyOS】accessibility | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 13 | [I3ZDIR](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDIR) | RQ-[Demo&应用子系统][JAVA UI]【HarmonyOS】AbilityForm | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 14 | [I3ZDIS](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDIS) | RQ-[Demo&应用子系统][AI]【HarmonyOS】AI | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 15 | [I3ZDIT](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDIT) | RQ-[Demo&应用子系统][媒体]【HarmonyOS】Audio | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 16 | [I3ZDIU](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDIU) | RQ-[Demo&应用子系统][媒体]【HarmonyOS】AudioPlayer | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 17 | [I3ZDIW](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDIW) | RQ-[Demo&应用子系统][设备]【HarmonyOS】BatteryInfo | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 18 | [I3ZDIX](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDIX) | RQ-[Demo&应用子系统][设备]【HarmonyOS】指南针Compass | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 19 | [I3ZDIY](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDIY) | RQ-[Demo&应用子系统][安全]【HarmonyOS】DataSecurity | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 20 | [I3ZDIZ](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDIZ) | RQ-[Demo&应用子系统][网络]【HarmonyOS】DistributedAbility | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 21 | [I3ZDJ0](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDJ0) | RQ-[Demo&应用子系统][通用]【HarmonyOS】DistributedCommonEvent | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 22 | [I3ZDJ1](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDJ1) | RQ-[Demo&应用子系统][数据]【HarmonyOS】DistributedPictures | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 23 | [I3ZDJ3](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDJ3) | RQ-[Demo&应用子系统][安全]【HarmonyOS】FaceRecognition | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 24 | [I3ZDJ4](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDJ4) | RQ-[Demo&应用子系统][设备]【HarmonyOS】NFC | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 25 | [I3ZDJ5](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDJ5) | RQ-[Demo&应用子系统][数据]【HarmonyOS】关系型数据库 | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 26 | [I3ZDJ6](https://gitee.com/open_harmony/dashboard?issue_id=I3ZDJ6) | RQ-[Demo&应用子系统][数据]【HarmonyOS】对象关系映射数据库 | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 27 | [I3ZRBA](https://gitee.com/open_harmony/dashboard?issue_id=I3ZRBA) | 提供系统电源状态机管理能力 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 28 | [I3ZRBV](https://gitee.com/open_harmony/dashboard?issue_id=I3ZRBV) | 提供休眠运行锁（RunningLock）管理能力 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 29 | [I3ZRCV](https://gitee.com/open_harmony/dashboard?issue_id=I3ZRCV) | 提供休眠、唤醒流程管理及实现 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 30 | [I3ZMUM](https://gitee.com/open_harmony/dashboard?issue_id=I3ZMUM) | 【分布式数据管理子系统】【本地数据库】 轻量级数据库JS API交付，对标开源+小程序 | 标准系统 | SIG_DataManagement | [@widecode](https://gitee.com/widecode) |
| 31 | [I3ZMW4](https://gitee.com/open_harmony/dashboard?issue_id=I3ZMW4) | 【分布式数据管理子系统】【本地数据库】提供RDB和PREFERENCES的能力 | 标准系统 | SIG_DataManagement | [@widecode](https://gitee.com/widecode) |
| 32 | [I3ZMX5](https://gitee.com/open_harmony/dashboard?issue_id=I3ZMX5) | 【分布式数据管理子系统】【本地数据库】 支持ResultSet滑动窗口能力 | 标准系统 | SIG_DataManagement | [@widecode](https://gitee.com/widecode) |
| 33 | [I3YC8O](https://gitee.com/open_harmony/dashboard?issue_id=I3YC8O) | 【分布式数据管理】【分布式数据库】支持JS接口创建分布式数据库 | 标准系统 | SIG_DataManagement | [@widecode](https://gitee.com/widecode) |
| 34 | [I3WHJS](https://gitee.com/open_harmony/dashboard?issue_id=I3WHJS) | 基于HDF驱动框架提供马达驱动程序适配 | 标准系统 | SIG_DriverFramework | [@chenfeng469](https://gitee.com/chenfeng469) |
| 35 | [I3ZRA7](https://gitee.com/open_harmony/dashboard?issue_id=I3ZRA7) | 基于HDF驱动框架提供加速度传感器驱动程序适配 | 标准系统 | SIG_DriverFramework | [@chenfeng469](https://gitee.com/chenfeng469) |


## OpenHarmony 2.3.0.3版本特性清单：
L0/L1需求列表:
| no   | issue                                                        | feture description                                | platform     | sig            | owner                                 |
| :--- | ------------------------------------------------------------ | :------------------------------------------------ | :--------- | :------------- | :------------------------------------ |
| 1    | [I3NN88](https://gitee.com/openharmony/hiviewdfx_hiview_lite/issues/I3NN88) | 【DFX子系统】【HiDumper】LiteOS_M系统信息dump工具 | 轻量系统 | SIG_BscSoftSrv | [@kkup180](https://gitee.com/kkup180) |
| 2    | [I3NN7D](https://gitee.com/openharmony/hiviewdfx_hiview_lite/issues/I3NN7D) | 【DFX子系统】【HiDumper】LiteOS_A系统信息dump工具 | 轻量系统 | SIG_BscSoftSrv | [@kkup180](https://gitee.com/kkup180) |
| 3    | [I3NT48](https://gitee.com/openharmony/kernel_liteos_a/issues/I3NT48) | 【轻内核子系统】proc文件系统增强                             | 轻量系统 | SIG_Kernel           | [@kkup180](https://gitee.com/kkup180)             |
| 4    | [I3WLDI](https://gitee.com/openharmony/kernel_liteos_m/issues/I3WLDI) | 【轻内核子系统】L0支持轻量级shell框架和常用调测命令           | 轻量系统 | SIG_Kernel           | [@kkup180](https://gitee.com/kkup180)             |
| 5    | [I3WLCN](https://gitee.com/openharmony/kernel_liteos_m/issues/I3WLCN) | 【轻内核子系统】L0 LiteOS-M支持ARM9架构                     | 轻量系统 | SIG_Kernel           | [@kkup180](https://gitee.com/kkup180)             |
| 6    | [I3NE8P](https://gitee.com/openharmony/powermgr_powermgr_lite/issues/I3NE8P) | 【电源管理】充放电状态查询接口                               | 轻量系统 | SIG_HardwareMng      | [@zianed](https://gitee.com/zianed)               |
| 7    | [I3NIEJ)](https://gitee.com/openharmony/powermgr_powermgr_lite/issues/I3NIEJ) | 【电源管理】电量查询接口                                     | 轻量系统 | SIG_HardwareMng      | [@zianed](https://gitee.com/zianed)               |
| 8    | [I3NIFG](https://gitee.com/openharmony/powermgr_powermgr_lite/issues/I3NIFG) | 【电源管理】实现并提供低功耗模式                             | 轻量系统 | SIG_HardwareMng      | [@zianed](https://gitee.com/zianed)               |
| 9   | [I3NIFR](https://gitee.com/openharmony/powermgr_powermgr_lite/issues/I3NIFR) | 【电源管理】提供低功耗模式统一API                            | 轻量系统 | SIG_HardwareMng      | [@zianed](https://gitee.com/zianed)               |
| 10   | [I3NN7V](https://gitee.com/openharmony/hiviewdfx_hiview_lite/issues/I3NN7V) | 【DFX子系统】【BBoxDetector】LiteOS_A死机重启维测框架        | 轻量系统 | SIG_BscSoftSrv       | [@kkup180](https://gitee.com/kkup180)             |
| 11   | [I3NN9B](https://gitee.com/openharmony/hiviewdfx_hiview_lite/issues/I3NN9B) | 【DFX子系统】【BBoxDetector】LiteOS_M死机重启维测框架        | 轻量系统 | SIG_BscSoftSrv       | [@kkup180](https://gitee.com/kkup180)             |
L2需求列表:
| 1 | [I3ZN3M](https://gitee.com/open_harmony/dashboard?issue_id=I3ZN3M) | 【分布式数据管理】鸿蒙单框架L2分布式数据管理开源 | 标准系统 | SIG_DataManagement | [@widecode](https://gitee.com/widecode) |
| 2 | [I40K12](https://gitee.com/open_harmony/dashboard?issue_id=I40K12) | 【Samples】【JSUI】【容器组件】JS气泡（popup） | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 3 | [I40K1P](https://gitee.com/open_harmony/dashboard?issue_id=I40K1P) | 【Samples】【JSUI】【容器组件】下拉刷新容器（refresh） | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 4 | [I40K2A](https://gitee.com/open_harmony/dashboard?issue_id=I40K2A) | 【Samples】【数据】【HarmonyOS】轻量级偏好数据库 | 标准系统 | SIG_Samples | [@illybyy](https://gitee.com/illybyy) |
| 5 | [I40NBW](https://gitee.com/open_harmony/dashboard?issue_id=I40NBW) | 【应用子系统】鸿蒙单框架L2系统应用-设置开源-WLAN | 标准系统 | SIG_SysApplication | [@nicolaswang](https://gitee.com/nicolaswang) |

## OpenHarmony 2.3.0.3版本特性清单：

L2需求列表：
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1 | [I41LYF](https://gitee.com/open_harmony/dashboard?issue_id=I41LYF) | 导航栏支持显示BACK、HOME、RECENT菜单 | 标准系统 | SIG_SysApplication | [@liuzhenyu2021](https://gitee.com/liuzhenyu2021) |
| 2 | [I41LSC](https://gitee.com/open_harmony/dashboard?issue_id=I41LSC) | 【SystemUI】SystemUI权限系统弹窗能力 | 标准系统 | SIG_SysApplication | [@liuzhenyu2021](https://gitee.com/liuzhenyu2021) |
| 3 | [I3XY72](https://gitee.com/open_harmony/dashboard?issue_id=I3XY72) | c++与 js时间\日期和数字国际化能力构建 | 标准系统 | SIG_ApplicationFramework | [@meaty-bag-and-wangwang-meat](https://gitee.com/meaty-bag-and-wangwang-meat) |

## OpenHarmony 2.2 Beta2 版本特性清单：

L2需求列表：
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
|1|[I3XLZR](https://gitee.com/open_harmony/dashboard?issue_id=I3XLZR)|AudioService音频服务|标准系统|SIG_GraphicsandMedia|[@magekkkk](https://gitee.com/magekkkk)|
|2|[I3XM04](https://gitee.com/open_harmony/dashboard?issue_id=I3XM04)|Audio音频管理模块及API|标准系统|SIG_GraphicsandMedia|[@magekkkk](https://gitee.com/magekkkk)|
|3|[I3XM2C](https://gitee.com/open_harmony/dashboard?issue_id=I3XM2C)|CameraService 相机服务|标准系统|SIG_GraphicsandMedia|[@magekkkk](https://gitee.com/magekkkk)|
|4|[I3XM1U](https://gitee.com/open_harmony/dashboard?issue_id=I3XM1U)|Camera相机处理模块及API|标准系统|SIG_GraphicsandMedia|[@magekkkk](https://gitee.com/magekkkk)|
|5|[I3XM34](https://gitee.com/open_harmony/dashboard?issue_id=I3XM34)|MediaService播放录制服务|标准系统|SIG_GraphicsandMedia|[@magekkkk](https://gitee.com/magekkkk)|
|6|[I3XM2V](https://gitee.com/open_harmony/dashboard?issue_id=I3XM2V)|Media媒体处理模块及API|标准系统|SIG_GraphicsandMedia|[@magekkkk](https://gitee.com/magekkkk)|
|7|[I41GOS ](https://gitee.com/open_harmony/dashboard?issue_id=I41GOS)|【分布式文件子系统】（需求）基于JS语言实现system.file接口|标准系统|SIG_DataManagement|[@panqinxu](https://gitee.com/panqinxu)|
|8|[I426IN](https://gitee.com/open_harmony/dashboard?issue_id=I426IN)|【组网】自组网管理|标准系统|SIG_SoftBus|[@maerlii](https://gitee.com/maerlii)|
|9|[I40NBW](https://gitee.com/open_harmony/dashboard?issue_id=I40NBW)|WLAN设置项|标准系统|SIG_SystemApplication|[@xiongshiyi](https://gitee.com/xiongshiyi)|
|10|[I400ZM](https://gitee.com/open_harmony/dashboard?issue_id=I400ZM)|【应用子系统】【Launcher】桌面设置界面UX优化，Grid布局桌面支持图标拖动|标准系统|SIG_SystemApplication|[@xiongshiyi](https://gitee.com/xiongshiyi)|
|11|[I41J7Q](https://gitee.com/open_harmony/dashboard?issue_id=I41J7Q)|支持Video组件|标准系统|SIG_ApplicationFramework|[@zhanghaibo0](https://gitee.com/zhanghaibo0)|
|12|[I41JBF](https://gitee.com/open_harmony/dashboard?issue_id=I41JBF)|支持Camera组件|标准系统|SIG_ApplicationFramework|[@zhanghaibo0](https://gitee.com/zhanghaibo0)|
|13|[I41JG4](https://gitee.com/open_harmony/dashboard?issue_id=I41JG4)|支持JS与NAPI混合开发接口|标准系统|SIG_ApplicationFramework|[@zhanghaibo0](https://gitee.com/zhanghaibo0)|
|14|[I3ZRBA](https://gitee.com/open_harmony/dashboard?issue_id=I3ZRBA)|提供系统电源状态机管理能力|标准系统|SIG_DistributedHardwareManagement|[@zianed](https://gitee.com/zianed)|
|15|[I3ZRBV](https://gitee.com/open_harmony/dashboard?issue_id=I3ZRBV)|提供休眠运行锁（RunningLock）管理能力|标准系统|SIG_DistributedHardwareManagement|[@zianed](https://gitee.com/zianed)|
|16|[I3ZRCV](https://gitee.com/open_harmony/dashboard?issue_id=I3ZRCV)|提供休眠、唤醒流程管理及实现|标准系统|SIG_DistributedHardwareManagement|[@zianed](https://gitee.com/zianed)|
|17|[I3WHJS](https://gitee.com/open_harmony/dashboard?issue_id=I3WHJS)|基于HDF驱动框架提供马达驱动程序适配|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|18|[I3ZRA7](https://gitee.com/open_harmony/dashboard?issue_id=I3ZRA7)|基于HDF驱动框架提供加速度传感器驱动程序适配|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|19|[I41HBJ](https://gitee.com/open_harmony/dashboard?issue_id=I41HBJ)|【驱动子系统】提供Audio HDI接口实现|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|20|[I41HBK](https://gitee.com/open_harmony/dashboard?issue_id=I41HBK)|【驱动子系统】Audio 驱动框架用户态接口库|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|21|[I41HBM](https://gitee.com/open_harmony/dashboard?issue_id=I41HBM)|【驱动子系统】提供支持MPI接口的用户态接口库|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|22|[I41HBN](https://gitee.com/open_harmony/dashboard?issue_id=I41HBN)|【驱动子系统】HDI接口支持跨进程通信|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|23|[I41HBH](https://gitee.com/open_harmony/dashboard?issue_id=I41HBH)|【驱动子系统】基于HDF驱动框架提供WIFI支持HDI接口能力|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|24|[I41HBG](https://gitee.com/open_harmony/dashboard?issue_id=I41HBG)|【驱动子系统】基于HDF驱动框架提供WIFI支持P2P驱动能力|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|25|[I41HBI](https://gitee.com/open_harmony/dashboard?issue_id=I41HBI)|【驱动子系统】提供Audio Driver Model驱动模型框架|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|26|[I41HBA](https://gitee.com/open_harmony/dashboard?issue_id=I41HBA)|【驱动子系统】基于HDF驱动框架提供keyboard驱动能力|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|27|[I41HBZ](https://gitee.com/open_harmony/dashboard?issue_id=I41HBZ)|【驱动子系统】基于HDF框架提供USB device DDK|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|28|[I41HBU](https://gitee.com/open_harmony/dashboard?issue_id=I41HBU)|【驱动子系统】基于HDF驱动框架提供Camera设备驱动模型框架层BufferManager|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|29|[I41HBV](https://gitee.com/open_harmony/dashboard?issue_id=I41HBV)|【驱动子系统】基于HDF驱动框架提供Camera设备驱动模型框架层utils通用组件(thread、event、watchdog)r|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|30|[I41HBX](https://gitee.com/open_harmony/dashboard?issue_id=I41HBX)|【驱动子系统】基于HDF驱动框架提供Camera驱动多平台扩展平台适配|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|31|[I41HBY](https://gitee.com/open_harmony/dashboard?issue_id=I41HBY)|【驱动子系统】基于HDF框架提供USB host DDK|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|32|[I41HBP](https://gitee.com/open_harmony/dashboard?issue_id=I41HBP)|【驱动子系统】基于HDF驱动框架提供相机标准南向接口Camera设备控制（CameraDevice）|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|33|[I41HBQ](https://gitee.com/open_harmony/dashboard?issue_id=I41HBQ)|【驱动子系统】基于HDF驱动框架提供相机标准南向接口Image流控制（StreamOperator）|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|34|[I41HBR](https://gitee.com/open_harmony/dashboard?issue_id=I41HBR)|【驱动子系统】基于HDF驱动框架提供相机标准南向接口Camera HDI接口|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|35|[I41HBS](https://gitee.com/open_harmony/dashboard?issue_id=I41HBS)|【驱动子系统】基于HDF驱动框架提供Pipeline管理 |标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|36|[I41HBT](https://gitee.com/open_harmony/dashboard?issue_id=I41HBT)|【驱动子系统】基于HDF驱动框架提供Camera设备驱动模型框架层设备管理DeviceManager|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|37|[I41HBO](https://gitee.com/open_harmony/dashboard?issue_id=I41HBO)|【驱动子系统】基于HDF驱动框架提供相机标准南向接口Camera设备管理（CameraHost）|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|38|[I3ZVIO](https://gitee.com/open_harmony/dashboard?issue_id=I3ZVIO)|Hi3516DV300开发板上，提供账号无关的设备认证能力|标准系统|SIG_DistributedHardwareManagement|[@locheng7](https://gitee.com/locheng7)|
|39|[I40PB8](https://gitee.com/open_harmony/dashboard?issue_id=I40PB8)|应用侧取消本地所有通知|标准系统|SIG_SystemApplication|[@autumn330](https://gitee.com/autumn330)|
|40|[I40PB0](https://gitee.com/open_harmony/dashboard?issue_id=I40PB0)|应用侧发布本地多行类型通知|标准系统|SIG_SystemApplication|[@autumn330](https://gitee.com/autumn330)|
|41|[I40PB1](https://gitee.com/open_harmony/dashboard?issue_id=I40PB1)|应用侧发布本地长文本通知|标准系统|SIG_SystemApplication|[@autumn330](https://gitee.com/autumn330)|
|42|[I40PB2](https://gitee.com/open_harmony/dashboard?issue_id=I40PB2)|应用侧发布本地内容资讯类型的普通文本通知|标准系统|SIG_SystemApplication|[@autumn330](https://gitee.com/autumn330)|
|43|[I40PB3](https://gitee.com/open_harmony/dashboard?issue_id=I40PB3)|应用侧发布本地其他类型的普通文本通知|标准系统|SIG_SystemApplication|[@autumn330](https://gitee.com/autumn330)|
|44|[I40PB4](https://gitee.com/open_harmony/dashboard?issue_id=I40PB4)|应用侧发布本地服务提醒类型的普通文本通知|标准系统|SIG_SystemApplication|[@autumn330](https://gitee.com/autumn330)|
|45|[I40PB5](https://gitee.com/open_harmony/dashboard?issue_id=I40PB5)|应用侧发布本地社交通讯类型的普通文本通知|标准系统|SIG_SystemApplication|[@autumn330](https://gitee.com/autumn330)|
|46|[I3XY72](https://gitee.com/open_harmony/dashboard?issue_id=I3XY72)|c++与 js时间\日期和数字国际化能力构建|标准系统|SIG_ApplicationFramework|[@meaty-bag-and-wangwang-meat](https://gitee.com/meaty-bag-and-wangwang-meat)|
|47|[I42IMT](https://gitee.com/open_harmony/dashboard?issue_id=I42IMT)|RQ-[Demo&应用子系统][分布式]OpenHarmony版本分布式计算器|标准系统|SIG_SystemApplication|[@purple-ding-gags](https://gitee.com/purple-ding-gags)|
|48|[I42IKN](https://gitee.com/open_harmony/dashboard?issue_id=I42IKN)|RQ-[Demo&应用子系统][分布式]OpenHarmony版本分布式音乐播放器|标准系统|SIG_SystemApplication|[@purple-ding-gags](https://gitee.com/purple-ding-gags)|
|49|[I41JB1](https://gitee.com/openharmony/applications_photos/issues/I41JB1)|【应用子系统】【图库】图库基础功能-大图浏览|标准系统|SIG_SystemApplication|[@sunjunxiong](https://gitee.com/sunjunxiong)|
|50|[I41J2Y](https://gitee.com/openharmony/applications_photos/issues/I41J2Y)|【应用子系统】【图库】图库基础功能-相册管理|标准系统|SIG_SystemApplication|[@sunjunxiong](https://gitee.com/sunjunxiong)|
|51|[I41JBB](https://gitee.com/openharmony/applications_photos/issues/I41JBB)|【应用子系统】【图库】图库基础功能-Toolbar操作|标准系统|SIG_SystemApplication|[@sunjunxiong](https://gitee.com/sunjunxiong)|


## OpenHarmony 3.0.0.1版本特性清单：

L2需求列表：
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1 | [I40PAV](https://gitee.com/open_harmony/dashboard?issue_id=I40PAV) | 发布开启一个有页面的Ability的WantAgent通知 | 标准系统 | SIG_ApplicationFramework | [@autumn330](https://gitee.com/autumn330) |
| 2 | [I40PAW](https://gitee.com/open_harmony/dashboard?issue_id=I40PAW) | 通知删除接口 | 标准系统 | SIG_ApplicationFramework | [@autumn330](https://gitee.com/autumn330) |
| 3 | [I40PAX](https://gitee.com/open_harmony/dashboard?issue_id=I40PAX) | 查看Active通知内容和Active通知个数的接口 | 标准系统 | SIG_ApplicationFramework | [@autumn330](https://gitee.com/autumn330) |
| 4 | [I40PAY](https://gitee.com/open_harmony/dashboard?issue_id=I40PAY) | 通知取消订阅 | 标准系统 | SIG_ApplicationFramework | [@autumn330](https://gitee.com/autumn330) |
| 5 | [I40PAZ](https://gitee.com/open_harmony/dashboard?issue_id=I40PAZ) | 通知订阅 | 标准系统 | SIG_ApplicationFramework | [@autumn330](https://gitee.com/autumn330) |
| 6 | [I426IT](https://gitee.com/open_harmony/dashboard?issue_id=I426IT) | 【组网】组网单框架FWK | 标准系统 | SIG_SoftBus | [@maerlii](https://gitee.com/maerlii) |
| 7 | [I426IM](https://gitee.com/open_harmony/dashboard?issue_id=I426IM) | 【组网】组网框架构建：安全 | 标准系统 | SIG_SoftBus | [@maerlii](https://gitee.com/maerlii) |
| 8 | [I426IQ](https://gitee.com/open_harmony/dashboard?issue_id=I426IQ) | 【组网】节点信息管理 | 标准系统 | SIG_SoftBus | [@maerlii](https://gitee.com/maerlii) |
| 9 | [I3NIMY](https://gitee.com/open_harmony/dashboard?issue_id=I3NIMY) | 【连接】BR/EDR模块管理 | 标准系统 | SIG_SoftBus | [@tianmeimimi](https://gitee.com/tianmeimimi) |
| 10 | [I426J0](https://gitee.com/open_harmony/dashboard?issue_id=I426J0) | 【传输】通道管理 | 标准系统 | SIG_SoftBus | [@maerlii](https://gitee.com/maerlii) |
| 11 | [I3NIZD](https://gitee.com/open_harmony/dashboard?issue_id=I3NIZD) | 【连接】连接策略的定义与管理 | 标准系统 | SIG_SoftBus | [@tianmeimimi](https://gitee.com/tianmeimimi) |
| 12 | [I426IW](https://gitee.com/open_harmony/dashboard?issue_id=I426IW) | 【连接】连接状态机功能实现 | 标准系统 | SIG_SoftBus | [@maerlii](https://gitee.com/maerlii) |
| 13 | [I42UO1](https://gitee.com/open_harmony/dashboard?issue_id=I42UO1) | RQ-[Demo&应用子系统][JSUI][容器组件]JS堆叠容器（stack） | 标准系统 | SIG_SystemApplication | [@adslk](https://gitee.com/adslk) |
| 14 | [I42X0C](https://gitee.com/open_harmony/dashboard?issue_id=I42X0C) | RQ-[Demo&应用子系统][JSUI][容器组件]JS步骤导航器（stepper） | 标准系统 | SIG_SystemApplication | [@gaohui100](https://gitee.com/gaohui100) |
| 15 | [I42WY6](https://gitee.com/open_harmony/dashboard?issue_id=I42WY6) | RQ-[Demo&应用子系统][JSUI][容器组件]JS滑动容器（swiper） | 标准系统 | SIG_SystemApplication | [@gaohui100](https://gitee.com/gaohui100) |
| 16 | [I42UNK](https://gitee.com/open_harmony/dashboard?issue_id=I42UNK) | RQ-[Demo&应用子系统][JSUI][容器组件]JS页签容器（tabs） | 标准系统 | SIG_SystemApplication | [@adslk](https://gitee.com/adslk) |
| 17 | [I42S96](https://gitee.com/open_harmony/dashboard?issue_id=I42S96) | RQ-[Demo&应用子系统][数据][HarmonyOS]融合搜索 | 标准系统 | SIG_SystemApplication | [@guojin26](https://gitee.com/guojin26) |
| 18 | [I40PB6](https://gitee.com/open_harmony/dashboard?issue_id=I40PB6) | 应用侧增加slot | 标准系统 | SIG_ApplicationFramework | [@autumn330](https://gitee.com/autumn330) |
| 19 | [I40PB7](https://gitee.com/open_harmony/dashboard?issue_id=I40PB7) | 应用侧删除slot | 标准系统 | SIG_ApplicationFramework | [@autumn330](https://gitee.com/autumn330) |
| 20 | [I42WA2](https://gitee.com/open_harmony/dashboard?issue_id=I42WA2) | L2 提供 OHOS LLVM编译 | 标准系统 | SIG_CompileRuntime | [@zhuoli72](https://gitee.com/zhuoli72) |
| 21 | [I3U4DP](https://gitee.com/open_harmony/dashboard?issue_id=I3U4DP) | musl侵入式修改分离 | 标准系统 | SIG_CompileRuntime | [@caoruihong](https://gitee.com/caoruihong) |
| 22 | [I42WDD](https://gitee.com/open_harmony/dashboard?issue_id=I42WDD) | L2 musl c库支持 | 标准系统 | SIG_CompileRuntime | [@zhuoli72](https://gitee.com/zhuoli72) |
| 23 | [I4014F](https://gitee.com/open_harmony/dashboard?issue_id=I4014F) | 【帐号子系统】JS API交付，开源+小程序 | 标准系统 | SIG_BasicSoftwareService | [@verystone](https://gitee.com/verystone) |
| 24 | [I436VH](https://gitee.com/open_harmony/dashboard?issue_id=I436VH) | 创建串行任务分发器，使用串行任务分发器执行任务 | 标准系统 | SIG_ApplicationFramework | [@autumn330](https://gitee.com/autumn330) |
| 25 | [I436VI](https://gitee.com/open_harmony/dashboard?issue_id=I436VI) | 创建专有任务分发器，使用专有任务分发器执行任务 | 标准系统 | SIG_ApplicationFramework | [@autumn330](https://gitee.com/autumn330) |
| 26 | [I436VJ](https://gitee.com/open_harmony/dashboard?issue_id=I436VJ) | 创建全局并发任务分发器，使用全局并发任务分发器执行任务 | 标准系统 | SIG_ApplicationFramework | [@autumn330](https://gitee.com/autumn330) |
| 27 | [I436VK](https://gitee.com/open_harmony/dashboard?issue_id=I436VK) | 创建并发任务分发器，使用并发任务分发器执行任务 | 标准系统 | SIG_ApplicationFramework | [@autumn330](https://gitee.com/autumn330) |
| 28 | [I436VL](https://gitee.com/open_harmony/dashboard?issue_id=I436VL) | ces部件化改造 | 标准系统 | SIG_ApplicationFramework | [@autumn330](https://gitee.com/autumn330) |
| 29 | [I436VM](https://gitee.com/open_harmony/dashboard?issue_id=I436VM) | ans部件化改造 | 标准系统 | SIG_ApplicationFramework | [@autumn330](https://gitee.com/autumn330) |
| 30 | [I436VX](https://gitee.com/open_harmony/dashboard?issue_id=I436VX) | 提供分布式的回调Native接口 | 标准系统 | SIG_ApplicationFramework | [@autumn330](https://gitee.com/autumn330) |
| 31 | [I436N0](https://gitee.com/open_harmony/dashboard?issue_id=I436N0) | 支持应用包信息分布式存储能力 | 标准系统 | SIG_ApplicationFramework | [@fuzhangHW](https://gitee.com/fuzhangHW) |
| 32 | [I4312I](https://gitee.com/open_harmony/dashboard?issue_id=I4312I) | 支持全新开发范式 | 标准系统 | SIG_ApplicationFramework | [@zhanghaibo0](https://gitee.com/zhanghaibo0) |
| 33 | [I4313W](https://gitee.com/open_harmony/dashboard?issue_id=I4313W) | 【分布式文件子系统】（需求）ext4&f2fs拍包工具 | 标准系统 | SIG_DataManagement | [@panqinxu](https://gitee.com/panqinxu) |
| 34 | [I41H57](https://gitee.com/open_harmony/dashboard?issue_id=I41H57) | 【驱动子系统】基于HDF驱动框架提供ADC平台总线驱动 | 标准系统 | SIG_DriverFramework | [@zianed](https://gitee.com/zianed) |


## OpenHarmony 3.0.0.2版本特性清单：
状态说明：discussion(方案讨论，需求未接受)，Reject(未纳入版本)，developing(开发中)，Testing(测试中)，Accepted(已验收)
| no   | issue                                                        | feture description                                | platform     | sig            | owner                                 |
| :--- | ------------------------------------------------------------ | :------------------------------------------------ | :--------- | :------------- | :------------------------------------ |
| 1    | [I3QEVG](https://gitee.com/openharmony/distributedschedule_dms_fwk_lite/issues/I3QEVG) | 【分布式调度】轻设备获取调用者APP的APPID并传输到富设备      | 轻量系统 | SIG_AppFramework     | [@lijiarun](https://gitee.com/lijiarun)            |

L2需求列表：
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
|1|[I43W4L](https://gitee.com/open_harmony/dashboard?issue_id=I43W4L)|RQ-[Demo&应用子系统][JSUI][基础组件]JS基础组件|标准系统|SIG_SystemApplication|[@guojin26](https://gitee.com/guojin26)|
|2|[I43XA7](https://gitee.com/open_harmony/dashboard?issue_id=I43XA7)|RQ-[Demo&应用子系统][JSUI][图表组件]chart|标准系统|SIG_SystemApplication|[@adslk](https://gitee.com/adslk)|
|3|[I43W8L](https://gitee.com/open_harmony/dashboard?issue_id=I43W8L)|RQ-[Demo&应用子系统][JSUI][滑动选择器组件] picker|标准系统|SIG_SystemApplication|[@gaohui100](https://gitee.com/gaohui100)|
|4|[I41HBB](https://gitee.com/open_harmony/dashboard?issue_id=I41HBB)|【驱动子系统】基于HDF驱动框架提供mouse驱动能力|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|

## OpenHarmony 3.0.0.3版本特性清单：

状态说明：discussion(方案讨论，需求未接受)，Reject(未纳入版本)，developing(开发中)，Testing(测试中)，Accepted(已验收)
| no   | issue                                                        | feture description                                | platform     | sig            | owner                                 |
| :--- | ------------------------------------------------------------ | :------------------------------------------------ | :--------- | :------------- | :------------------------------------ |
| 1   | [I3XCB5](https://gitee.com/openharmony/appexecfwk_appexecfwk_lite/issues/I3XCB5) | 【应用程序框架】按照应用粒度的存储资源使用统计       | 轻量系统 | SIG_AppFramework     | [@autumn](https://gitee.com/autumn330)            |
| 2   | [I3NK7D](https://gitee.com/openharmony/multimedia_utils_lite/issues/I3NK7D) | 【多媒体子系统】适配新南向接口                               | 轻量系统 | SIG_GraphicsAndMedia | [@zhu-mingliang](https://gitee.com/zhu-mingliang)   |
| 3   | [I3NM60](https://gitee.com/openharmony/multimedia_utils_lite/issues/I3NM60) | 【多媒体子系统】相机metadata管理及相机静态能力查询           | 轻量系统 | SIG_GraphicsAndMedia | [@zhu-mingliang](https://gitee.com/zhu-mingliang)   |
| 4   | [I3NM6F](https://gitee.com/openharmony/multimedia_utils_lite/issues/I3NM6F) | 【多媒体子系统】相机设备管理                                 | 轻量系统 | SIG_GraphicsAndMedia | [@zhu-mingliang](https://gitee.com/zhu-mingliang)   |
| 5   | [I3NM73](https://gitee.com/openharmony/multimedia_utils_lite/issues/I3NM73) | 【多媒体子系统】相机图像帧管理                               | 轻量系统 | SIG_GraphicsAndMedia | [@zhu-mingliang](https://gitee.com/zhu-mingliang)   |
| 6   | [I3NM8J](https://gitee.com/openharmony/multimedia_utils_lite/issues/I3NM8J) | 【多媒体子系统】本地mp3播放支持                              | 轻量系统 | SIG_GraphicsAndMedia | [@zhu-mingliang](https://gitee.com/zhu-mingliang)   |
| 7   | [I3NMMU](https://gitee.com/openharmony/multimedia_utils_lite/issues/I3NMMU) | 【多媒体子系统】编码视频流和音频流处理支持                   | 轻量系统 | SIG_GraphicsAndMedia | [@zhu-mingliang](https://gitee.com/zhu-mingliang)   |

L2 需求列表：
| no   | issue                                                        | feture description                                | platform     | sig            | owner                                 |
| :--- | ------------------------------------------------------------ | :------------------------------------------------ | :--------- | :------------- | :------------------------------------ |
|1|[I43G6T](https://gitee.com/open_harmony/dashboard?issue_id=I43G6T)|WM Client架构演进|标准系统|SIG_GraphicsandMedia|[@lz-230](https://gitee.com/lz-230)|
|2|[I43HMM](https://gitee.com/open_harmony/dashboard?issue_id=I43HMM)|WM Server架构演进|标准系统|SIG_GraphicsandMedia|[@lz-230](https://gitee.com/lz-230)|
|3|[I43I3O](https://gitee.com/open_harmony/dashboard?issue_id=I43I3O)|RQ-[图形子系统][vsync模块][vsync]vsync架构演进|标准系统|SIG_GraphicsandMedia|[@lz-230](https://gitee.com/lz-230)|
|4|[I44TNC](https://gitee.com/open_harmony/dashboard?issue_id=I44TNC)|RQ-[Demo&应用子系统][JSUI][Menu/Option]JSMenu|标准系统|SIG_SystemApplication|[@guojin26](https://gitee.com/guojin26)|
|5|[I452UD](https://gitee.com/open_harmony/dashboard?issue_id=I452UD)|RQ-[Demo&应用子系统][JSUI]【OpenHarmony】工具栏|标准系统|SIG_SystemApplication|[@adslk](https://gitee.com/adslk)|
|6|[I44TN0](https://gitee.com/open_harmony/dashboard?issue_id=I44TN0)|RQ-[Demo&应用子系统][JSUI][滑动条] slider|标准系统|SIG_SystemApplication|[@gaohui100](https://gitee.com/gaohui100)|
|7|[I41HC0](https://gitee.com/open_harmony/dashboard?issue_id=I41HC0)|【驱动子系统】基于HDF驱动框架提供Display HDI的服务化|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|8|[I41LWL](https://gitee.com/open_harmony/dashboard?issue_id=I41LWL)|【SystemUI】状态栏最大最小化|标准系统|SIG_SystemApplication|[@liuzhenyu2021](https://gitee.com/liuzhenyu2021)|

## OpenHarmony 3.0 Beta1版本特性清单：
L1需求列表

| no   | issue                                                        | feture description                                | platform     | sig            | owner                                 |
| :--- | ------------------------------------------------------------ | :------------------------------------------------ | :--------- | :------------- | :------------------------------------ |
|1|[I3NIOF](https://gitee.com/openharmony/communication_dsoftbus/issues/I3NIOF)|【传输】传输SDK|轻量系统|SIG_SoftBus|[@jiangkuaixue](https://gitee.com/jiangkuaixue)|

L2 需求列表：
| no   | issue                                                        | feture description                                | platform     | sig            | owner                                 |
| :--- | ------------------------------------------------------------ | :------------------------------------------------ | :--------- | :------------- | :------------------------------------ |
|1|[I3XHVJ](https://gitee.com/open_harmony/dashboard?issue_id=I3XHVJ)|支持全量升级包升级|标准系统|SIG_BasicSoftwareService|[@ailorna](https://gitee.com/ailorna)|
|2|[I3XHVP](https://gitee.com/open_harmony/dashboard?issue_id=I3XHVP)|支持差分升级包升级|标准系统|SIG_BasicSoftwareService|[@ailorna](https://gitee.com/ailorna)|
|3|[I436VT](https://gitee.com/open_harmony/dashboard?issue_id=I436VT)|安装读取shortcut信息|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|

## OpenHarmony 3.0.0.6版本特性清单：
状态说明：discussion(方案讨论，需求未接受)，Reject(未纳入版本)，developing(开发中)，Testing(测试中)，Accepted(已验收)
| no   | issue                                                        | feture description                                | platform     | sig            | owner                                 |
| :--- | ------------------------------------------------------------ | :------------------------------------------------ | :--------- | :------------- | :------------------------------------ |
| 1    | [I3NN1Z](https://gitee.com/openharmony/aafwk_aafwk_lite/issues/I3NN1Z) | 【应用程序框架】轻量级实现弹窗授权动态授权机制               | 轻量系统 | SIG_AppFramework     | [@autumn](https://gitee.com/autumn330)              |
| 2    | [I3NTAZ](https://gitee.com/openharmony/security_huks/issues/I3NTAZ) | 【安全】轻量级实现弹窗授权动态授权机制                       | 轻量系统 | SIG_Security         | [@scuteehuangjun](https://gitee.com/scuteehuangjun) |

L2 需求列表：
| no   | issue                                                        | feture description                                | platform     | sig            | owner                                 |
| :--- | ------------------------------------------------------------ | :------------------------------------------------ | :--------- | :------------- | :------------------------------------ |
|1|[I45ZKP](https://gitee.com/open_harmony/dashboard?issue_id=I45ZKP)|RQ-[Demo&应用子系统][JSUI][画布组件] JsCanvas|标准系统|SIG_SystemApplication|[@gaohui100](https://gitee.com/gaohui100)|
|2|[I466KX](https://gitee.com/open_harmony/dashboard?issue_id=I466KX)|RQ-[Demo&应用子系统][JSUI]【OpenHarmony】 栅格布局|标准系统|SIG_SystemApplication|[@adslk](https://gitee.com/adslk)|
|3|[I45YM2](https://gitee.com/open_harmony/dashboard?issue_id=I45YM2)|RQ-[Demo&应用子系统][JS UI]【OpenHarmony】JS自定义组件（JSUICustomComponent）|标准系统|SIG_SystemApplication|[@guojin26](https://gitee.com/guojin26)|
|4|[I44Y4J](https://gitee.com/open_harmony/dashboard?issue_id=I44Y4J)|RQ-[Demo&应用子系统][JS UI]【HarmonyOS】自适应卡片（AdaptiveServiceWidget）|标准系统|SIG_SystemApplication|[@caopan_com](https://gitee.com/caopan_com)|
|5|[I44Y0N](https://gitee.com/open_harmony/dashboard?issue_id=I44Y0N)|RQ-[Demo&应用子系统][JS UI]【HarmonyOS】自适应页面（AdaptivePortalPage）|标准系统|SIG_SystemApplication|[@wangli325](https://gitee.com/wangli325)|
|6|[I44Y15](https://gitee.com/open_harmony/dashboard?issue_id=I44Y15)|RQ-[Demo&应用子系统][JS UI]【HarmonyOS】自适应效率型首页（AdaptivePortalList）|标准系统|SIG_SystemApplication|[@wangli325](https://gitee.com/wangli325)|
|7|[I46ZCN](https://gitee.com/open_harmony/dashboard?issue_id=I46ZCN)|RQ-[Demo&应用子系统][JAVA UI]【HarmonyOS】添加NativeLayer示例|标准系统|SIG_SystemApplication|[@guojin26](https://gitee.com/guojin26)|
|8|[I3ZVTJ](https://gitee.com/open_harmony/dashboard?issue_id=I3ZVTJ)|【分布式任务调度子系统】接收远端拉起FA请求，跨设备拉起远端FA|标准系统|SIG_DataManagement|[@zjucx](https://gitee.com/zjucx)|
|9|[I3ZVTT](https://gitee.com/open_harmony/dashboard?issue_id=I3ZVTT)|【分布式任务调度子系统】鸿蒙单框架L2分布式能力建设-SAMGR模块构建|标准系统|SIG_DataManagement|[@zjucx](https://gitee.com/zjucx)|
|10|[I3ZMY9](https://gitee.com/open_harmony/dashboard?issue_id=I3ZMY9)|【分布式数据管理子系统】【本地数据库】 XTS测试用例|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
|11|[I41H55](https://gitee.com/open_harmony/dashboard?issue_id=I41H55)|【驱动子系统】基于HDF驱动框架提供I2S/PCM平台总线驱动|标准系统|SIG_DistributedHardwareManagement|[@zianed](https://gitee.com/zianed)|
|12|[I41HBF](https://gitee.com/open_harmony/dashboard?issue_id=I41HBF)|【驱动子系统】基于HDF驱动框架提供Display驱动模型兼容DRM显示框架|标准系统|SIG_DistributedHardwareManagement|[@zianed](https://gitee.com/zianed)|
|13|[I46J19](https://gitee.com/open_harmony/dashboard?issue_id=I46J19)|add jsapi in compileruntime|标准系统|SIG_CompileRuntime|[@xliu-huanwei](https://gitee.com/xliu-huanwei)|
|14|[I46N37](https://gitee.com/open_harmony/dashboard?issue_id=I46N37)|升级quickjs使用worker|标准系统|SIG_CompileRuntime|[@wpyhuawei](https://gitee.com/wpyhuawei)|
|15|[I40PBC](https://gitee.com/open_harmony/dashboard?issue_id=I40PBC)|应用侧发布本地分组的普通通知|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|
|16|[I40PBF](https://gitee.com/open_harmony/dashboard?issue_id=I40PBF)|在免打扰模式下发布通知|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|
|17|[I40PBG](https://gitee.com/open_harmony/dashboard?issue_id=I40PBG)|发布开启一个无页面的Ability的wantAgent|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|
|18|[I40PBH](https://gitee.com/open_harmony/dashboard?issue_id=I40PBH)|取消WantAgent的实例|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|
|19|[I40PBI](https://gitee.com/open_harmony/dashboard?issue_id=I40PBI)|发布公共事件的WantAgent通知|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|
|20|[I40PBM](https://gitee.com/open_harmony/dashboard?issue_id=I40PBM)|应用侧取消本地通知|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|
|21|[I40PBN](https://gitee.com/open_harmony/dashboard?issue_id=I40PBN)|应用侧发布声音通知|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|
|22|[I40PBO](https://gitee.com/open_harmony/dashboard?issue_id=I40PBO)|应用侧发布振动通知|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|
|23|[I40PBP](https://gitee.com/open_harmony/dashboard?issue_id=I40PBP)|应用侧发布本地有输入框的通知（NotificationUserInput）|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|

## OpenHarmony 3.0.0.7版本特性清单:
状态说明：discussion(方案讨论，需求未接受)，Reject(未纳入版本)，developing(开发中)，Testing(测试中)，Accepted(已验收)
| no   | issue                                                        | feture description                                | platform     | sig            | owner                                 |
| :--- | ------------------------------------------------------------ | :------------------------------------------------ | :--------- | :------------- | :------------------------------------ |
| 1    | [I46W6Q](https://gitee.com/openharmony/global_i18n_lite/issues/I46W6Q) | 【全球化】新增31种语言支持              | 轻量系统 | SIG_AppFramework     | [@zhiweilai](https://gitee.com/zhiweilai)              |

L2需求列表:
| no   | issue                                                        | feture description                                | platform     | sig            | owner                                 |
| :--- | ------------------------------------------------------------ | :------------------------------------------------ | :--------- | :------------- | :------------------------------------ |
|1|[I47B4N](https://gitee.com/open_harmony/dashboard?issue_id=I47B4N)|RQ-[Demo&应用子系统][JSUI]【OpenHarmony】页面路由|标准系统|SIG_SystemApplication|[@gaohui100](https://gitee.com/gaohui100)|
|2|[I479L2](https://gitee.com/open_harmony/dashboard?issue_id=I479L2)|RQ-[Demo&应用子系统][JSUI]【OpenHarmony】 系统时间与定时器|标准系统|SIG_SystemApplication|[@adslk](https://gitee.com/adslk)|
|3|[I48CJ3](https://gitee.com/open_harmony/dashboard?issue_id=I48CJ3)|RQ-[Demo&应用子系统][JSUI]【OpenHarmony】设备信息|标准系统|SIG_SystemApplication|[@caopan_com](https://gitee.com/caopan_com)|
|4|[I48VL4](https://gitee.com/open_harmony/dashboard?issue_id=I48VL4)|RQ-[Demo&应用子系统][JSUI]【OpenHarmony】设置屏幕亮度|标准系统|SIG_SystemApplication|[@caopan_com](https://gitee.com/caopan_com)|
|5|[I49B4X](https://gitee.com/open_harmony/dashboard?issue_id=I49B4X)|RQ-[Demo&应用子系统][JSUI]【OpenHarmony】国际化|标准系统|SIG_SystemApplication|[@gaohui100](https://gitee.com/gaohui100)|
|6|[I3ZXZF](https://gitee.com/open_harmony/dashboard?issue_id=I3ZXZF)|【Kernel升级适配】OpenHarmony L2内核版本升级至5.10|标准系统|SIG_Kernel|[@z-jax](https://gitee.com/z-jax)|
|7|[I41HC1](https://gitee.com/open_harmony/dashboard?issue_id=I41HC1)|【驱动子系统】基于HDF驱动框架提供Codec HDI的服务化|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|8|[I41HB1](https://gitee.com/open_harmony/dashboard?issue_id=I41HB1)|【驱动子系统】基于HDF驱动框架提供陀螺仪Sensor驱动能力|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|9|[I41HBE](https://gitee.com/open_harmony/dashboard?issue_id=I41HBE)|【驱动子系统】基于HDF驱动框架提供backlight驱动能力|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|10|[I41HB5](https://gitee.com/open_harmony/dashboard?issue_id=I41HB5)|【驱动子系统】基于HDF驱动框架提供压力Sensor驱动能力|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|11|[I41HB4](https://gitee.com/open_harmony/dashboard?issue_id=I41HB4)|【驱动子系统】基于HDF驱动框架提供霍尔Sensor驱动能力|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|12|[I41H50](https://gitee.com/open_harmony/dashboard?issue_id=I41H50)|【驱动子系统】驱动hdi-gen开发工具|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|13|[I41HBD](https://gitee.com/open_harmony/dashboard?issue_id=I41HBD)|【驱动子系统】基于HDF驱动框架提供Encoder驱动能力|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|14|[I3NJ0R](https://gitee.com/open_harmony/dashboard?issue_id=I3NJ0R)|【发现】发现调度模块功能实现|标准系统|SIG_SoftBus|[@tianmeimimi](https://gitee.com/tianmeimimi)|
|15|[I3NIPH](https://gitee.com/open_harmony/dashboard?issue_id=I3NIPH)|【传输】消息传输|标准系统|SIG_SoftBus|[@tianmeimimi](https://gitee.com/tianmeimimi)|
|16|[I3NIPO](https://gitee.com/open_harmony/dashboard?issue_id=I3NIPO)|【传输】Byte传输|标准系统|SIG_SoftBus|[@tianmeimimi](https://gitee.com/tianmeimimi)|
|17|[I3NIOF](https://gitee.com/open_harmony/dashboard?issue_id=I3NIOF)|【传输】传输SDK|标准系统|SIG_SoftBus|[@tianmeimimi](https://gitee.com/tianmeimimi)|
|18|[I3NINO](https://gitee.com/open_harmony/dashboard?issue_id=I3NINO)|【传输】会话协商|标准系统|SIG_SoftBus|[@tianmeimimi](https://gitee.com/tianmeimimi)|
|19|[I40PBD](https://gitee.com/open_harmony/dashboard?issue_id=I40PBD)|2个不同的slot，加入到同一个Slot组里面|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|
|20|[I40PBJ](https://gitee.com/open_harmony/dashboard?issue_id=I40PBJ)|提供管理角标显示/隐藏的接口|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|
|21|[I40PBK](https://gitee.com/open_harmony/dashboard?issue_id=I40PBK)|提供管理通知许可的接口（设置和查询）|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|
|22|[I40PBL](https://gitee.com/open_harmony/dashboard?issue_id=I40PBL)|应用删除SlotGroup|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|
|23|[I40PBQ](https://gitee.com/open_harmony/dashboard?issue_id=I40PBQ)|发布带ActionButton的本地通知|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|
|24|[I40PBR](https://gitee.com/open_harmony/dashboard?issue_id=I40PBR)|通知流控处理|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|
|25|[I40PBT](https://gitee.com/open_harmony/dashboard?issue_id=I40PBT)|死亡监听|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|
|26|[I40PBU](https://gitee.com/open_harmony/dashboard?issue_id=I40PBU)|通知shell命令|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|
|27|[I49HOX](https://gitee.com/open_harmony/dashboard?issue_id=I49HOX)|工具链提供维测能力|标准系统|SIG_CompileRuntime|[@dhy308](https://gitee.com/dhy308)|
|28|[I40OPG](https://gitee.com/open_harmony/dashboard?issue_id=I40OPG)|【定时服务】非功能性需求|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|
|29|[I40OPH](https://gitee.com/open_harmony/dashboard?issue_id=I40OPH)|【定时服务】时间时区同步|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|
|30|[I40OPI](https://gitee.com/open_harmony/dashboard?issue_id=I40OPI)|【定时服务】定时器功能|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|
|31|[I40OPJ](https://gitee.com/open_harmony/dashboard?issue_id=I40OPJ)|【定时服务】时间时区管理|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|
|32|[I3ZQED](https://gitee.com/open_harmony/dashboard?issue_id=I3ZQED)|【帐号子系统】添加分布式帐号登录状态IPC消息权限检查|标准系统|SIG_BasicSoftwareService|[@verystone](https://gitee.com/verystone)|
|33|[I40MWK](https://gitee.com/open_harmony/dashboard?issue_id=I40MWK)|构建语言和地区配置和区域显示能力|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|34|[I40OWT](https://gitee.com/open_harmony/dashboard?issue_id=I40OWT)|【搜网】Radio状态管理|标准系统|SIG_SoftBus|[@zhang-hai-feng](https://gitee.com/zhang-hai-feng)|
|35|[I40OWU](https://gitee.com/open_harmony/dashboard?issue_id=I40OWU)|【搜网】搜网注册|标准系统|SIG_SoftBus|[@zhang-hai-feng](https://gitee.com/zhang-hai-feng)|
|36|[I40OWN](https://gitee.com/open_harmony/dashboard?issue_id=I40OWN)|【SIM卡】卡状态处理|标准系统|SIG_SoftBus|[@zhang-hai-feng](https://gitee.com/zhang-hai-feng)|
|37|[I40OWO](https://gitee.com/open_harmony/dashboard?issue_id=I40OWO)|【SIM卡】解锁卡pin、puk|标准系统|SIG_SoftBus|[@zhang-hai-feng](https://gitee.com/zhang-hai-feng)|
|38|[I40OWM](https://gitee.com/open_harmony/dashboard?issue_id=I40OWM)|【SIM卡】卡账户处理|标准系统|SIG_SoftBus|[@zhang-hai-feng](https://gitee.com/zhang-hai-feng)|
|39|[I40OX3](https://gitee.com/open_harmony/dashboard?issue_id=I40OX3)|【短彩信】提供小区广播的能力|标准系统|SIG_SoftBus|[@zhang-hai-feng](https://gitee.com/zhang-hai-feng)|
|40|[I40OX6](https://gitee.com/open_harmony/dashboard?issue_id=I40OX6)|【短彩信】提供短信基本配置功能|标准系统|SIG_SoftBus|[@zhang-hai-feng](https://gitee.com/zhang-hai-feng)|
|41|[I40OSY](https://gitee.com/open_harmony/dashboard?issue_id=I40OSY)|【SIM卡】提供拷贝、删除、更新、获取卡短信的能力供电话子系统内部使用|标准系统|SIG_SoftBus|[@zhang-hai-feng](https://gitee.com/zhang-hai-feng)|
|42|[I40OTN](https://gitee.com/open_harmony/dashboard?issue_id=I40OTN)|【通话管理】CS域发送MMI命令|标准系统|SIG_SoftBus|[@zhang-hai-feng](https://gitee.com/zhang-hai-feng)|
|43|[I40OTS](https://gitee.com/open_harmony/dashboard?issue_id=I40OTS)|【通话管理】CS域DTMF功能|标准系统|SIG_SoftBus|[@zhang-hai-feng](https://gitee.com/zhang-hai-feng)|
|44|[I40OTW](https://gitee.com/open_harmony/dashboard?issue_id=I40OTW)|【通话管理】CS通话基础功能|标准系统|SIG_SoftBus|[@zhang-hai-feng](https://gitee.com/zhang-hai-feng)|
|45|[I40OTX](https://gitee.com/open_harmony/dashboard?issue_id=I40OTX)|【通话管理】CS通话扩展功能|标准系统|SIG_SoftBus|[@zhang-hai-feng](https://gitee.com/zhang-hai-feng)|
|46|[I40OTY](https://gitee.com/open_harmony/dashboard?issue_id=I40OTY)|【SIM卡】G/U卡文件信息获取、保存|标准系统|SIG_SoftBus|[@zhang-hai-feng](https://gitee.com/zhang-hai-feng)|
|47|[I40OU3](https://gitee.com/open_harmony/dashboard?issue_id=I40OU3)|【搜网】驻网信息(运营商信息及SPN广播GSM）|标准系统|SIG_SoftBus|[@zhang-hai-feng](https://gitee.com/zhang-hai-feng)|
|48|[I40OU5](https://gitee.com/open_harmony/dashboard?issue_id=I40OU5)|【搜网】网络状态(漫游、GSM/LTE/WCDMA接入技术）|标准系统|SIG_SoftBus|[@zhang-hai-feng](https://gitee.com/zhang-hai-feng)|
|49|[I40OU7](https://gitee.com/open_harmony/dashboard?issue_id=I40OU7)|【搜网】信号强度(WCDMA/GSM/LTE)|标准系统|SIG_SoftBus|[@zhang-hai-feng](https://gitee.com/zhang-hai-feng)|
|50|[I40OUC](https://gitee.com/open_harmony/dashboard?issue_id=I40OUC)|【短彩信】提供发送短信功能|标准系统|SIG_SoftBus|[@zhang-hai-feng](https://gitee.com/zhang-hai-feng)|
|51|[I40OUF](https://gitee.com/open_harmony/dashboard?issue_id=I40OUF)|【短彩信】提供接收短信功能|标准系统|SIG_SoftBus|[@zhang-hai-feng](https://gitee.com/zhang-hai-feng)|
|52|[I40OTT](https://gitee.com/open_harmony/dashboard?issue_id=I40OTT)|【通话管理】CS域呼叫等待功能|标准系统|SIG_SoftBus|[@zhang-hai-feng](https://gitee.com/zhang-hai-feng)|
|53|[I49HPE](https://gitee.com/open_harmony/dashboard?issue_id=I49HPE)|独立发布SDK能力支持|标准系统|SIG_ApplicationFramework|[@zhanghaibo0](https://gitee.com/zhanghaibo0)|
|54|[I49CTN](https://gitee.com/open_harmony/dashboard?issue_id=I49CTN)|【hdc_std】支持openharmony sdk独立发布|标准系统|SIG_R&DToolChain|[@chuxuezhe111](https://gitee.com/chuxuezhe111)|
|55|[I49HLR](https://gitee.com/open_harmony/dashboard?issue_id=I49HLR)|【Build】OpenHarmony SDK的打包和编译|标准系统|SIG_CompileRuntime|[@weichaox](https://gitee.com/weichaox)|
|56|[I40OWF](https://gitee.com/open_harmony/dashboard?issue_id=I40OWF)|【SIM卡】随卡功能实现，包括获取当前运营商配置等|标准系统|SIG_SoftBus|[@zhang-hai-feng](https://gitee.com/zhang-hai-feng)|

## OpenHarmony 3.0.0.8 版本特性清单：

| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1    | [I3NT3F](https://gitee.com/openharmony/kernel_liteos_a/issues/I3NT3F) | 【轻内核子系统】内核支持trace功能                            | 轻量系统 | SIG_Kernel           | [@kkup180](https://gitee.com/kkup180)               |
| 2    | [I3NT63](https://gitee.com/openharmony/kernel_liteos_a/issues/I3NT63) | 【轻内核子系统】pagecache功能完善                            | 轻量系统 | SIG_Kernel           | [@kkup180](https://gitee.com/kkup180)               |

L2需求列表:
| no   | issue                                                        | feture description                                | platform     | sig            | owner                                 |
| :--- | ------------------------------------------------------------ | :------------------------------------------------ | :--------- | :------------- | :------------------------------------ |
|1|[I49V7K](https://gitee.com/open_harmony/dashboard?issue_id=I49V7K)|test code static check|标准系统|SIG_BasicSoftwareService|[@skyblackleon](https://gitee.com/skyblackleon)|
|2|[I49XVK](https://gitee.com/open_harmony/dashboard?issue_id=I49XVK)|AA/APPEXE/IPC合入master|标准系统|SIG_ApplicationFramework|[@fuzhangHW](https://gitee.com/fuzhangHW)|
|3|[I43UU4](https://gitee.com/open_harmony/dashboard?issue_id=I43UU4)|JS应用生命周期补充支持|标准系统|SIG_ApplicationFramework|[@zhanghaibo0](https://gitee.com/zhanghaibo0)|
|4|[I43V4W](https://gitee.com/open_harmony/dashboard?issue_id=I43V4W)|支持使用JS开发service ability|标准系统|SIG_ApplicationFramework|[@zhanghaibo0](https://gitee.com/zhanghaibo0)|
|5|[I43V73](https://gitee.com/open_harmony/dashboard?issue_id=I43V73)|支持使用JS开发data ability|标准系统|SIG_ApplicationFramework|[@zhanghaibo0](https://gitee.com/zhanghaibo0)|
|6|[I43UZ0](https://gitee.com/open_harmony/dashboard?issue_id=I43UZ0)|支持系统服务弹窗|标准系统|SIG_ApplicationFramework|[@zhanghaibo0](https://gitee.com/zhanghaibo0)|
|7|[I3ZVIO](https://gitee.com/open_harmony/dashboard?issue_id=I3ZVIO)|Hi3516DV300开发板上，提供账号无关的设备认证能力|标准系统|SIG_DistributedHardwareManagement|[@locheng7](https://gitee.com/locheng7)|
|8|[I3ZVUD](https://gitee.com/open_harmony/dashboard?issue_id=I3ZVUD)|L2系统上为系统服务（应用）提供kit接口（JS）|标准系统|SIG_DistributedHardwareManagement|[@locheng7](https://gitee.com/locheng7)|
|9|[I4A3YY](https://gitee.com/open_harmony/dashboard?issue_id=I4A3YY)|ACE2.0方舟对接|标准系统|SIG_CompileRuntime|[@guobingbing3](https://gitee.com/guobingbing3)|
|10|[I40SQU](https://gitee.com/open_harmony/dashboard?issue_id=I40SQU)|【工具链】提供hdc java lib|标准系统|SIG_R&DToolChain|[@chuxuezhe111](https://gitee.com/chuxuezhe111)|
|11|[I474ZZ](https://gitee.com/open_harmony/dashboard?issue_id=I474ZZ)|【分布式数据管理】【JS API接口】增加关系型数据库JS API|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
|12|[I40PBE](https://gitee.com/open_harmony/dashboard?issue_id=I40PBE)|应用侧发布本地带应用ICON的普通通知|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|
|13|[I43UU4](https://gitee.com/open_harmony/dashboard?issue_id=I43UU4)|JS应用生命周期补充支持|标准系统|SIG_ApplicationFramework|[@zhanghaibo0](https://gitee.com/zhanghaibo0)|
|14|[I43UU4](https://gitee.com/open_harmony/dashboard?issue_id=I43UU4)|JS应用生命周期补充支持|标准系统|SIG_ApplicationFramework|[@zhanghaibo0](https://gitee.com/zhanghaibo0)|
|15|[I43V4W](https://gitee.com/open_harmony/dashboard?issue_id=I43V4W)|支持使用JS开发service ability|标准系统|SIG_ApplicationFramework|[@zhanghaibo0](https://gitee.com/zhanghaibo0)|
|16|[I43V73](https://gitee.com/open_harmony/dashboard?issue_id=I43V73)|支持使用JS开发data ability|标准系统|SIG_ApplicationFramework|[@zhanghaibo0](https://gitee.com/zhanghaibo0)|
|17|[I43UZ0](https://gitee.com/open_harmony/dashboard?issue_id=I43UZ0)|支持系统服务弹窗|标准系统|SIG_ApplicationFramework|[@zhanghaibo0](https://gitee.com/zhanghaibo0)|
|18|[I42TIB](https://gitee.com/open_harmony/dashboard?issue_id=I42TIB)|RQ-[媒体子系统][Media][媒体录制器]音视频录制及API|标准系统|SIG_GraphicsandMedia|[@magekkkk](https://gitee.com/magekkkk)|
|19|[I42TMC](https://gitee.com/open_harmony/dashboard?issue_id=I42TMC)|RQ-[媒体子系统][Camera][相机框架管理]相机录像功能|标准系统|SIG_GraphicsandMedia|[@magekkkk](https://gitee.com/magekkkk)|
|20|[I3ZVT4](https://gitee.com/open_harmony/dashboard?issue_id=I3ZVT4)|【分布式任务调度子系统】鸿蒙单框架L2分布式能力建设-跨设备绑定元能力|标准系统|SIG_DataManagement|[@zjucx](https://gitee.com/zjucx)|
|21|[I49HB0](https://gitee.com/open_harmony/dashboard?issue_id=I49HB0)|【分布式任务调度子系统】启动、绑定能力添加组件visible权限校验|标准系统|SIG_DataManagement|[@zmxlye](https://gitee.com/zmxlye)|
|22|[I40121](https://gitee.com/open_harmony/dashboard?issue_id=I40121)|【Kernel升级适配】HDF适配OpenHarmony 5.10内核|标准系统|SIG_DriverFramework|[@yuanbogit](https://gitee.com/yuanbogit)|
|23|[I3ZY55](https://gitee.com/open_harmony/dashboard?issue_id=I3ZY55)|【Kernel升级适配】Hi3516DV300开发板适配OpenHarmony 5.10内核|标准系统|SIG_Kernel|[@z-jax](https://gitee.com/z-jax)|

## OpenHarmony 3.0 LTS版本特性清单：
L2需求列表:
| no   | issue                                                        | feture description                                | platform     | sig            | owner                                 |
| :--- | ------------------------------------------------------------ | :------------------------------------------------ | :--------- | :------------- | :------------------------------------ |
|1|[I441K6](https://gitee.com/open_harmony/dashboard?issue_id=I441K6)|L2构建分布式权限管理主体标识转换能力|标准系统|SIG_Security|[@xuwenfang](https://gitee.com/xuwenfang)|
|2|[I4446U](https://gitee.com/open_harmony/dashboard?issue_id=I4446U)|L2构建分布式权限同步能力|标准系统|SIG_Security|[@xuwenfang](https://gitee.com/xuwenfang)|
|3|[I4447R](https://gitee.com/open_harmony/dashboard?issue_id=I4447R)|L2构建分布式权限管理能力|标准系统|SIG_Security|[@xuwenfang](https://gitee.com/xuwenfang)|
|4|[I3NIQX](https://gitee.com/open_harmony/dashboard?issue_id=I3NIQX)|【连接】BLE模块管理|标准系统|SIG_SoftBus|[@tianmeimimi](https://gitee.com/tianmeimimi)|
|5|[I426J6](https://gitee.com/open_harmony/dashboard?issue_id=I426J6)|【传输】文件传输|标准系统|SIG_SoftBus|[@maerlii](https://gitee.com/maerlii)|
|6|[I426J3](https://gitee.com/open_harmony/dashboard?issue_id=I426J3)|【传输】TCP通道复用打开和关闭|标准系统|SIG_SoftBus|[@maerlii](https://gitee.com/maerlii)|
|7|[I40PBB](https://gitee.com/open_harmony/dashboard?issue_id=I40PBB)|应用侧发布本地图片类型通知|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|
|8|[I40PBS](https://gitee.com/open_harmony/dashboard?issue_id=I40PBS)|通知图片大小限制|标准系统|SIG_ApplicationFramework|[@autumn330](https://gitee.com/autumn330)|
|9|[I4BE9N](https://gitee.com/open_harmony/dashboard?issue_id=I4BE9N)|modify dataabilityDB for LTS branch|标准系统|SIG_BasicSoftwareService|[@skyblackleon](https://gitee.com/skyblackleon)|
|10|[I48DFB](https://gitee.com/open_harmony/dashboard?issue_id=I48DFB)|【分布式任务调度子系统】分布式调度支持元能力迁移管理能力|标准系统|SIG_DataManagement|[@wangdd__zju](https://gitee.com/wangdd__zju)|
|11|[I48BOB](https://gitee.com/open_harmony/dashboard?issue_id=I48BOB)|【分布式文件子系统】（需求）JS 存储框架开发|标准系统|SIG_DataManagement|[@gudehe](https://gitee.com/gudehe)|
|12|[I4A3LY](https://gitee.com/open_harmony/dashboard?issue_id=I4A3LY)|【分布式文件子系统】（需求）JS 存储框架挂载能力 - FS manager|标准系统|SIG_DataManagement|[@zhangzhiwi](https://gitee.com/zhangzhiwi)|
|13|[I4BXPQ](https://gitee.com/open_harmony/dashboard?issue_id=I4BXPQ)|【DFX】libhicollie支持OpenHarmony standard|标准系统|SIG_BasicSoftwareService|[@stesen](https://gitee.com/stesen)|
|14|[I4BXMM](https://gitee.com/open_harmony/dashboard?issue_id=I4BXMM)|【DFX】libhitrace支持OpenHarmony standard|标准系统|SIG_BasicSoftwareService|[@stesen](https://gitee.com/stesen)|
|15|[I4BY0R](https://gitee.com/open_harmony/dashboard?issue_id=I4BY0R)|【DFX】鸿蒙应用事件打点接口与鸿蒙应用事件管理（JS）|标准系统|SIG_BasicSoftwareService|[@lyj](https://gitee.com/lyj_love_code)|

## OpenHarmony 3.1.0.1版本特性清单：

暂无



## OpenHarmony 3.1.0.2版本特性清单：
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1   | [I3XGJH](https://gitee.com/openharmony/startup_init_lite/issues/I3XGJH) | init基础环境构建                             | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 2   | [I3XGKV](https://gitee.com/openharmony/startup_init_lite/issues/I3XGKV) | sytemparameter管理                            | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 3   | [I3XGLN](https://gitee.com/openharmony/startup_init_lite/issues/I3XGLN) | init 脚本管理                            | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 4   | [I3XGM3](https://gitee.com/openharmony/startup_init_lite/issues/I3XGM3) | init 服务管理                            | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 5   | [I3XGMQ](https://gitee.com/openharmony/startup_init_lite/issues/I3XGMQ) | 基础权限管理                             | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 6   | [I3XGN8](https://gitee.com/openharmony/startup_init_lite/issues/I3XGN8) | bootimage构建和加载                             | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 7   | [I3XGO7](https://gitee.com/openharmony/startup_init_lite/issues/I3XGO7) | uevent 管理                            | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 8   | [I4BX5Z](https://gitee.com/openharmony/multimedia_histreamer/issues/I4BX5Z) | 【需求】HiStreamer支持音频播放和控制             | 轻量系统 | SIG_GraphicsandMedia         | [@guodongchen](https://gitee.com/guodongchen) |
| 9   | [I4BX8A](https://gitee.com/openharmony/multimedia_histreamer/issues/I4BX8A) | 【需求】HiStreamer支持常见音频格式mp3/wav的播放   | 轻量系统 | SIG_GraphicsandMedia         | [@guodongchen](https://gitee.com/guodongchen) |
| 10   | [I4BX9E](https://gitee.com/openharmony/multimedia_histreamer/issues/I4BX9E) | 【需求】HiStreamer播放引擎框架需求               | 轻量系统 | SIG_GraphicsandMedia         | [@guodongchen](https://gitee.com/guodongchen) |


## OpenHarmony 3.1.1.1版本特性清单：
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1    | [I3XGNM](https://gitee.com/openharmony/startup_init_lite/issues/I3XGNM) | 烧写模式支持                           | 轻量系统 | SIG_BscSoftSrv           | [@xionglei6](https://gitee.com/xionglei6) |
| 2    | [I4FL3F](https://e.gitee.com/open_harmony/dashboard?issue=I4FL3F) | 3516开发板多模输入触屏和Back适配验证                          | 标准系统 | SIG_HardwareMgr           | [@hhh2](https://gitee.com/hhh2) |
| 3   | [I4DK89](https://gitee.com/openharmony/multimedia_histreamer/issues/I4DK89) | 【需求】HiStreamer插件框架需求                              | 轻量系统 | SIG_GraphicsandMedia | [@guodongchen](https://gitee.com/guodongchen) |
| 4   | [I4DK8D](https://gitee.com/openharmony/multimedia_histreamer/issues/I4DK8D) | 【需求】HiStreamer性能和DFX需求                             | 轻量系统 | SIG_GraphicsandMedia | [@guodongchen](https://gitee.com/guodongchen) |


## OpenHarmony 3.1.1.2版本特性清单：

| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1    | [I3ND6Y](https://gitee.com/openharmony/kernel_liteos_a/issues/I3ND6Y) | 【性能】OS内核&驱动启动优化                           | 轻量系统 | SIG_Kernel           | [@kkup180](https://gitee.com/kkup180)             |
| 2    | [I3NTCT](https://gitee.com/openharmony/startup_appspawn_lite/issues/I3NTCT) | 【启动恢复子系统】Linux版本init支持热插拔                    | 轻量系统 | SIG_BscSoftSrv       | [@handyohos](https://gitee.com/handyohos)         |

## OpenHarmony 3.1.1.3版本特性清单：
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1    | [I3O2G8](https://gitee.com/openharmony/aafwk_aafwk_lite/issues/I3O2G8?from=project-issue) | 【应用程序框架】轻量级应用实现entity标签                     | 轻量系统 | SIG_AppFramework     | [@autumn](https://gitee.com/autumn330)|
| 2   | [I3NTDP](https://gitee.com/openharmony/communication_wifi_aware/issues/I3NTDP) | 【电话服务】支持轻量级mbed TLS协议栈                      | 轻量系统 | SIG_SoftBus          | [@rain_myf](https://gitee.com/rain_myf) |

## OpenHarmony 3.1.0.1版本特性清单：

暂无



## OpenHarmony 3.1.0.2版本特性清单：

| no   | issue                                                        | feture description                              | platform | sig                  | owner                                         |
| :--- | ------------------------------------------------------------ | :---------------------------------------------- | :------- | :------------------- | :-------------------------------------------- |
| 1    | [I3XGJH](https://gitee.com/openharmony/startup_init_lite/issues/I3XGJH) | init基础环境构建                                | 轻量系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 2    | [I3XGKV](https://gitee.com/openharmony/startup_init_lite/issues/I3XGKV) | sytemparameter管理                              | 轻量系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 3    | [I3XGLN](https://gitee.com/openharmony/startup_init_lite/issues/I3XGLN) | init 脚本管理                                   | 轻量系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 4    | [I3XGM3](https://gitee.com/openharmony/startup_init_lite/issues/I3XGM3) | init 服务管理                                   | 轻量系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 5    | [I3XGMQ](https://gitee.com/openharmony/startup_init_lite/issues/I3XGMQ) | 基础权限管理                                    | 轻量系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 6    | [I3XGN8](https://gitee.com/openharmony/startup_init_lite/issues/I3XGN8) | bootimage构建和加载                             | 轻量系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 7    | [I3XGO7](https://gitee.com/openharmony/startup_init_lite/issues/I3XGO7) | uevent 管理                                     | 轻量系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 8    | [I4BX5Z](https://gitee.com/openharmony/multimedia_histreamer/issues/I4BX5Z) | 【需求】HiStreamer支持音频播放和控制            | 轻量系统 | SIG_GraphicsandMedia | [@guodongchen](https://gitee.com/guodongchen) |
| 9    | [I4BX8A](https://gitee.com/openharmony/multimedia_histreamer/issues/I4BX8A) | 【需求】HiStreamer支持常见音频格式mp3/wav的播放 | 轻量系统 | SIG_GraphicsandMedia | [@guodongchen](https://gitee.com/guodongchen) |
| 10   | [I4BX9E](https://gitee.com/openharmony/multimedia_histreamer/issues/I4BX9E) | 【需求】HiStreamer播放引擎框架需求              | 轻量系统 | SIG_GraphicsandMedia | [@guodongchen](https://gitee.com/guodongchen) |


## OpenHarmony 3.1.1.1版本特性清单：

| no   | issue                                                        | feture description                   | platform | sig                  | owner                                         |
| :--- | ------------------------------------------------------------ | :----------------------------------- | :------- | :------------------- | :-------------------------------------------- |
| 1    | [I3XGNM](https://gitee.com/openharmony/startup_init_lite/issues/I3XGNM) | 烧写模式支持                         | 轻量系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 2    | [I4FL3F](https://e.gitee.com/open_harmony/dashboard?issue=I4FL3F) | 3516开发板多模输入触屏和Back适配验证 | 标准系统 | SIG_HardwareMgr      | [@hhh2](https://gitee.com/hhh2)               |
| 3    | [I4DK89](https://gitee.com/openharmony/multimedia_histreamer/issues/I4DK89) | 【需求】HiStreamer插件框架需求       | 轻量系统 | SIG_GraphicsandMedia | [@guodongchen](https://gitee.com/guodongchen) |
| 4    | [I4DK8D](https://gitee.com/openharmony/multimedia_histreamer/issues/I4DK8D) | 【需求】HiStreamer性能和DFX需求      | 轻量系统 | SIG_GraphicsandMedia | [@guodongchen](https://gitee.com/guodongchen) |


## OpenHarmony 3.1.1.2版本特性清单：

| no   | issue                                                        | feture description                        | platform | sig            | owner                                     |
| :--- | ------------------------------------------------------------ | :---------------------------------------- | :------- | :------------- | :---------------------------------------- |
| 1    | [I3ND6Y](https://gitee.com/openharmony/kernel_liteos_a/issues/I3ND6Y) | 【性能】OS内核&驱动启动优化               | 轻量系统 | SIG_Kernel     | [@kkup180](https://gitee.com/kkup180)     |
| 2    | [I3NTCT](https://gitee.com/openharmony/startup_appspawn_lite/issues/I3NTCT) | 【启动恢复子系统】Linux版本init支持热插拔 | 轻量系统 | SIG_BscSoftSrv | [@handyohos](https://gitee.com/handyohos) |

## OpenHarmony 3.1.1.3版本特性清单：

| no   | issue                                                        | feture description                         | platform | sig            | owner                                       |
| :--- | ------------------------------------------------------------ | :----------------------------------------- | :------- | :------------- | :------------------------------------------ |
| 1    | [I4FZ29](https://gitee.com/openharmony/communication_dsoftbus/issues/I4FZ29) | 【软总线】【传输】软总线提供传输ExtAPI接口     | 标准系统 | SIG_SoftBus    | [@laosan_ted](https://gitee.com/laosan_ted) |
| 2    | [I4FZ25](https://gitee.com/openharmony/communication_dsoftbus/issues/I4FZ25) | 【软总线】【组网】软总线支持网络切换组网       | 标准系统 | SIG_SoftBus    | [@heyingjiao](https://gitee.com/heyingjiao) |
| 3    | [I4HPR7](https://https://gitee.com/openharmony/drivers_framework/issues/I4HPR7) | [驱动子系统]提供hcs宏式解析接口            | 标准系统 | SIG_Driver     | [@fx_zhang](https://gitee.com/fx_zhang)     |
| 4    | [I4IT3U](https://gitee.com/openharmony/account_os_account/issues/I4IT3U) | [帐号子系统]支持应用帐号基础信息管理       | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 5    | [I4IT4G](https://gitee.com/openharmony/account_os_account/issues/I4IT4G) | [帐号子系统]支持应用帐号信息查询           | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 6    | [I4IT4N](https://gitee.com/openharmony/account_os_account/issues/I4IT4N) | [帐号子系统]支持应用帐号功能设置与内容修改 | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 7    | [I4IT4X](https://gitee.com/openharmony/account_os_account/issues/I4IT4X) | [帐号子系统]支持应用帐号订阅及取消订阅     | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 8    | [I4IT54](https://gitee.com/openharmony/account_os_account/issues/I4IT54) | [帐号子系统]支持应用帐号的新增和删除       | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 9    | [I4WTQT](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTQT) | 【资料】ace_engine_standard部件IT2版本资料录入需求                          | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |


## OpenHarmony 3.1.1.5(Beta)版本特性清单：

暂无

## OpenHarmony 3.1.2.1版本特性清单：

| no   | issue                                                        | feture description                                 | platform | sig                | owner                                   |
| :--- | ------------------------------------------------------------ | :------------------------------------------------- | :------- | :----------------- | :-------------------------------------- |
| 1    | [I4H3JJ](https://gitee.com/openharmony/distributeddatamgr_objectstore/issues/I4H3JJ) | L1设备分布式对象支持Listeneer复读机                | 轻量系统 | SIG_DataManagement | [@widecode](https://gitee.com/widecode) |
| 2    | [I4IBPH](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4IBPH) | 【distributed_kv_store】分布式数据服务缺失功能补齐 | 标准系统 | SIG_DataManagement | [@widecode](https://gitee.com/widecode) |
| 3    | [I4K7E3](https://gitee.com/openharmony/build/issues/I4K7E3)  | [编译构建子系统]支持使用统一的编译命令作为编译入口 | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox) |
| 4 | [I4WWRZ](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWRZ) | 【多模】鼠标按键功能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 5 | [I4WWS0](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWS0) | 【多模】鼠标滚轮功能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 6 | [I4WWSP](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWSP) | 【多模】按键输入事件拦截功能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 7 | [I4WWSQ](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWSQ) | 【多模】按键输入事件注入功能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 8 | [I4WWS1](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWS1) | 【多模】鼠标移动功能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 9 | [I4WWSR](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWSR) | 【多模】按键基本功能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |


## OpenHarmony 3.1.2.2版本特性清单：

| no   | issue                                                        | feture description                                 | platform | sig                | owner                                         |
| :--- | ------------------------------------------------------------ | :------------------------------------------------- | :------- | :----------------- | :-------------------------------------------- |
| 1    | [I4KCMM](https://gitee.com/openharmony/build/issues/I4KCMM)  | [编译构建子系统]轻量级和标准系统使用统一的编译流程 | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)       |
| 2    | [I4KCNB](https://gitee.com/openharmony/build/issues/I4KCNB)  | [编译构建子系统]支持使用统一的gn模板               | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)       |
| 3    | [I4KVJQ](https://gitee.com/openharmony/drivers_framework/issues/I4KVJQ) | [驱动子系统]支持Linux/Liteos-a内核系统级休眠唤醒   | 标准系统 | SIG_Driver         | [@fx_zhang](https://gitee.com/fx_zhang)       |
| 4    | [I4L3KK](https://gitee.com/openharmony/drivers_peripheral/issues/I4L3KK) | [驱动子系统]传感器器件驱动能力增强                 | 标准系统 | SIG_Driver         | [@Kevin-Lau](https://gitee.com/Kevin-Lau)     |
| 5    | [I410OZ](https://gitee.com/openharmony/usb_manager/issues/I410OZ) | [USB服务子系统]轻量级和标准系统使用统一的编译流程  | 标准系统 | SIG_Driver         | [@wu-chengwen](https://gitee.com/wu-chengwen) |
| 6 | [I4WWT0](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWT0) | 【多模】触摸板双指轻击手势 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 7 | [I4WWT1](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWT1) | 【多模】触摸板双指滑动手势 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 8 | [I4WWT2](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWT2) | 【多模】触摸板双指捏合手势 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 9 | [I4WWST](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWST) | 【多模】触摸屏输入事件拦截功能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 10 | [I4WWSV](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWSV) | 【多模】触摸屏输入事件注入功能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 11 | [I4WWT8](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWT8) | 【多模】触摸板基本功能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 12 | [I4WWT9](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWT9) | 【多模】触摸板单指移动手势 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 13 | [I4WWTA](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWTA) | 【多模】触摸板单指轻击手势 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 14 | [I4MBTZ](https://gitee.com/openharmony/graphic_standard/issues/I4MBTZ) | 【render_service部件】新增ACE2.0控件接入RenderService渲染后端规格 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 15 | [I4MBTY](https://gitee.com/openharmony/graphic_standard/issues/I4MBTY) | 【render_service部件】【新增特性】新增UI框架渲染后端特性 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 16 | [I4MBTX](https://gitee.com/openharmony/graphic_standard/issues/I4MBTX) | 【animation部件】提供动画增强算法 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 17 | [I4MBTW](https://gitee.com/openharmony/graphic_standard/issues/I4MBTW) | 【animation部件】提供基础动画框架 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |

## OpenHarmony 3.1.2.3版本特性清单：

| no   | issue                                                        | feture description                                           | platform | sig                | owner                                               |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :------- | :----------------- | :-------------------------------------------------- |
| 1    | [I4KCO7](https://gitee.com/openharmony/build/issues/I4KCO7)  | [编译构建子系统]轻量级和标准系统支持使用统一的产品配置       | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)             |
| 2    | [I4JQ2N](https://gitee.com/openharmony/communication_netstack/issues/I4JQ2N) | [电话服务子系统]提供Http JS API                              | 轻量系统 | SIG_Telephony      | [@zhang-hai-feng](https://gitee.com/zhang-hai-feng) |
| 3    | [I4JQ3G](https://gitee.com/openharmony/third_party_nghttp2/issues/I4JQ3G) | [电话服务子系统]提供Http 2.0协议                             | 轻量系统 | SIG_Telephony      | [@zhang-hai-feng](https://gitee.com/zhang-hai-feng) |
| 4    | [I4LZZF](https://gitee.com/openharmony/drivers_framework/issues/I4LZZF) | [驱动子系统]支持同步/异步电源管理调用                        | 标准系统 | SIG_Driver         | [@fx_zhang](https://gitee.com/fx_zhang)             |
| 5    | [I4L3LF](https://gitee.com/openharmony/drivers_peripheral/issues/I4L3LF) | [驱动子系统]传感器驱动模型能力增强                           | 标准系统 | SIG_Driver         | [@Kevin-Lau](https://gitee.com/Kevin-Lau)           |
| 6    | [I4MBTR](https://gitee.com/openharmony/drivers_peripheral/issues/I4MBTR) | [驱动子系统]SR000GGUSG:【新增特性】Display-Layer HDI接口针对L2的参考实现； Display-Gralloc HDI接口针对L2的参考实现； Display-Device HDI接口针对L2的参考实现； | 标准系统 | SIG_Driver         | [@YUJIA](https://gitee.com/JasonYuJia)              |
| 7    | [I4MBTS](https://gitee.com/openharmony/drivers_framework/issues/I4MBTS) | [驱动子系统]SR000GH16G:【增强特性】 HDF-Input设备能力丰富    | 标准系统 | SIG_Driver         | [@huangkai71](https://gitee.com/huangkai71)         |
| 8 | [I4WWSD](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWSD) | 【多模】基于配置的双系统按键按下分发功能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 9 | [I4WWSF](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWSF) | 【多模】基于配置的双系统按键按下加延时的分发功能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 10 | [I4WWSG](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWSG) | 【多模】基于配置的三系统按键按下分发功能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 11 | [I4WWSH](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWSH) | 【多模】基于配置的单系统按键按下加延时的分发功能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 12 | [I4WWSI](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWSI) | 【多模】基于配置的单系统按键抬起分发功能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 13 | [I4WWSK](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWSK) | 【多模】基于配置的单系统按键按下分发功能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 14 | [I4WWSB](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWSB) | 【多模】获取设备id列表API | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 15 | [I4WWSC](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWSC) | 【多模】获取指定id的输入设备信息 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 16 | [I4WWSZ](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWSZ) | 【多模】触摸板单指按下手势 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 17 | [I4WWSO](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWSO) | 【多模】按键输入事件监听功能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 18 | [I4WWSS](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWSS) | 【多模】触摸屏输入事件监听功能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 19 | [I4WWS2](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWS2) | 【多模】鼠标输入事件注入功能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 20 | [I4WWS3](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWS3) | 【多模】鼠标输入事件拦截功能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |

## OpenHarmony 3.1.2.5版本特性清单：

| no   | issue                                                        | feture description                                           | platform | sig                               | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :------- | :-------------------------------- | :------------------------------------------------ |
| 1    | [I4M8FW](https://gitee.com/openharmony/account_os_account/issues/I4M8FF?from=project-issue) | 【新增特性】支持应用账号基础信息管理                         | 标准系统 | SIG_BasicSoftwareService          | [@verystone](https://gitee.com/verystone)         |
| 2    | [I4MBQE](https://gitee.com/openharmony/miscservices_time/issues/I4MBQE) | 新增特性：时间时区管理                                       | 标准系统 | SIG_BasicSoftwareService          | [@autumn330](https://gitee.com/autumn330)         |
| 3    | [I4MBQF](https://gitee.com/openharmony/notification_ces_standard/issues/I4MBQF) | 【增强特性】【事件通知子系统】线程间EventHandler支持HiTrace能力 | 标准系统 | SIG_BasicSoftwareService          | [@autumn330](https://gitee.com/autumn330)         |
| 4    | [I4MBQG](https://gitee.com/openharmony/hiviewdfx_hisysevent/issues/I4MBQG?from=project-issue) | 【资料】hisysevent部件资料需求                               | 标准系统 | SIG_BasicSoftwareService          | [@yaomanhai](https://gitee.com/yaomanhai)         |
| 5    | [I4MBQH](https://gitee.com/openharmony/hiviewdfx_hisysevent/issues/I4MBQH?from=project-issue) | 【新增特性】支持鸿蒙HiSysEvent部件提供查询接口               | 标准系统 | SIG_BasicSoftwareService          | [@yaomanhai](https://gitee.com/yaomanhai)         |
| 6    | [I4MBQI](https://gitee.com/openharmony/hiviewdfx_hisysevent/issues/I4MBQI?from=project-issue) | 【新增特性】提供工具查询或者订阅系统事件                     | 标准系统 | SIG_BasicSoftwareService          | [@yaomanhai](https://gitee.com/yaomanhai)         |
| 7    | [I4MBQJ](https://gitee.com/openharmony/hiviewdfx_hisysevent/issues/I4MBQJ?from=project-issue) | 【新增特性】支持鸿蒙HiSysEvent部件提供订阅接口               | 标准系统 | SIG_BasicSoftwareService          | [@yaomanhai](https://gitee.com/yaomanhai)         |
| 8    | [I4MBQK](https://gitee.com/openharmony/hiviewdfx_hiappevent/issues/I4MBQK?from=project-issue) | 【资料】hiappevent部件资料需求                               | 标准系统 | SIG_BasicSoftwareService          | [@yaomanhai](https://gitee.com/yaomanhai)         |
| 9    | [I4MBQL](https://gitee.com/openharmony/hiviewdfx_hiappevent/issues/I4MBQL?from=project-issue) | 【新增特性】支持鸿蒙hiappevent部件的C接口                    | 标准系统 | SIG_BasicSoftwareService          | [@yaomanhai](https://gitee.com/yaomanhai)         |
| 10   | [I4MBQM](https://gitee.com/openharmony/build/issues/I4MBQM?from=project-issue) | 【新增规格】L0-L2支持统一的部件配置                          | 标准系统 | SIG_CompileRuntime                | [@wangxing-hw](https://gitee.com/wangxing-hw)     |
| 11   | [I4MBQN](https://gitee.com/openharmony/build/issues/I4MBQN?from=project-issue) | 【新增规格】L0-L2支持使用统一的编译命令进行编译              | 标准系统 | SIG_CompileRuntime                | [@wangxing-hw](https://gitee.com/wangxing-hw)     |
| 12   | [I4MBQO](https://gitee.com/openharmony/build/issues/I4MBQO?from=project-issue) | 【新增规格】L0-L2支持使用统一的编译流程                      | 标准系统 | SIG_CompileRuntime                | [@wangxing-hw](https://gitee.com/wangxing-hw)     |
| 13   | [I4MBQP](https://gitee.com/openharmony/build/issues/I4MBQP?from=project-issue) | 【新增规格】L0-L2支持使用统一的gn模板                        | 标准系统 | SIG_CompileRuntime                | [@wangxing-hw](https://gitee.com/wangxing-hw)     |
| 14   | [I4MBQQ](https://gitee.com/openharmony/build/issues/I4MBQQ?from=project-issue) | 【新增规格】L0-L2支持统一的产品配置                          | 标准系统 | SIG_CompileRuntime                | [@wangxing-hw](https://gitee.com/wangxing-hw)     |
| 15   | [I4MBQR](https://gitee.com/openharmony/build/issues/I4MBQR?from=project-issue) | 【资料】制定gn编码规范和最佳实践指导                         | 标准系统 | SIG_CompileRuntime                | [@wangxing-hw](https://gitee.com/wangxing-hw)     |
| 16   | [I4MBQS](https://gitee.com/openharmony/account_os_account/issues/I4MBQS?from=project-issue) | 【新增特性】支持应用账号信息查询                             | 标准系统 | SIG_BasicSoftwareService          | [@verystone](https://gitee.com/verystone)         |
| 17   | [I4MBQT](https://gitee.com/openharmony/account_os_account/issues/I4MBQT?from=project-issue) | 【新增特性】支持应用账号功能设置与内容修改                   | 标准系统 | SIG_BasicSoftwareService          | [@verystone](https://gitee.com/verystone)         |
| 18   | [I4MBQU](https://gitee.com/openharmony/account_os_account/issues/I4MBQU?from=project-issue) | 【新增特性】支持应用账号订阅及取消订阅                       | 标准系统 | SIG_BasicSoftwareService          | [@verystone](https://gitee.com/verystone)         |
| 19   | [I4MBQV](https://gitee.com/openharmony/account_os_account/issues/I4MBQV?from=project-issue) | 【DFX】应用账号基础信息约束                                  | 标准系统 | SIG_BasicSoftwareService          | [@verystone](https://gitee.com/verystone)         |
| 20   | [I4MBQW](https://gitee.com/openharmony/account_os_account/issues/I4MBQW?from=project-issue) | 【新增特性】支持应用账号的新增和删除                         | 标准系统 | SIG_BasicSoftwareService          | [@verystone](https://gitee.com/verystone)         |
| 21   | [I4MBQX](https://gitee.com/openharmony/global_cust_lite/issues/I4MBQX?from=project-issue) | 【增强特性】定制框架基础能力                                 | 标准系统 | SIG_ApplicationFramework          | [@zhengbin5](https://gitee.com/zhengbin5)         |
| 22   | [I4MBQY](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBQY?from=project-issue) | 【新增特性】资源编译工具支持增量编译                         | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 23   | [I4MBQZ](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBQZ?from=project-issue) | 【增强特性】时间段格式化                                     | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 24   | [I4MBR0](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR0?from=project-issue) | 【增强特性】区域表示和属性                                   | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 25   | [I4MBR1](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR1?from=project-issue) | 【增强特性】单复数支持                                       | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 26   | [I4MBR2](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR2?from=project-issue) | 【增强特性】字符串排序                                       | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 27   | [I4MBR3](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR3?from=project-issue) | 【增强特性】电话号码处理                                     | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 28   | [I4MBR4](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR4?from=project-issue) | 【新增特性】字母表检索                                       | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 29   | [I4MBR5](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR5?from=project-issue) | 【新增特性】度量衡体系和格式化                               | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 30   | [I4MBR7](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR7?from=project-issue) | 【新增特性】日历&本地历法                                    | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 31   | [I4MBR8](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR8?from=project-issue) | 【增强特性】unicode字符属性                                  | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 32   | [I4MBR9](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR9?from=project-issue) | 【增强特性】断词断行                                         | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 33   | [I4MBRA](https://gitee.com/openharmony/global_resmgr_standard/issues/I4MBRA?from=project-issue) | 【新增特性】系统资源管理                                     | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)             |
| 34   | [I4MBRB](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBRB?from=project-issue) | 【新增特性】rawfile资源管理                                  | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 35   | [I4MBRC](https://gitee.com/openharmony/developtools_profiler/issues/I4MBRC?from=project-issue) | 【hiperf部件】采样数据展示                                   | 标准系统 | SIG_R&DToolChain                  | [@wangzaishang](https://gitee.com/wangzaishang)   |
| 36   | [I4MBRD](https://gitee.com/openharmony/developtools_profiler/issues/I4MBRD?from=project-issue) | 【hiperf部件】性能数据采样记录                               | 标准系统 | SIG_R&DToolChain                  | [@wangzaishang](https://gitee.com/wangzaishang)   |
| 37   | [I4MBRE](https://gitee.com/openharmony/developtools_profiler/issues/I4MBRE?from=project-issue) | 【hiperf部件】性能数据计数统计                               | 标准系统 | SIG_R&DToolChain                  | [@wangzaishang](https://gitee.com/wangzaishang)   |
| 38   | [I4MBRF](https://gitee.com/openharmony/communication_wifi/issues/I4MBRF?from=project-issue) | 【新增特性】支持STA基础特性的JS API接口                      | 标准系统 | SIG_SoftBus                       | [@cheng_guohong](https://gitee.com/cheng_guohong) |
| 39   | [I4MBRG](https://gitee.com/openharmony/communication_wifi/issues/I4MBRG?from=project-issue) | 【新增特性】支持STA基础特性JS API资料文档                    | 标准系统 | SIG_SoftBus                       | [@cheng_guohong](https://gitee.com/cheng_guohong) |
| 40   | [I4MBRH](https://gitee.com/openharmony/communication_wifi/issues/I4MBRH?from=project-issue) | 【新增特性】支持STA基础特性                                  | 标准系统 | SIG_SoftBus                       | [@cheng_guohong](https://gitee.com/cheng_guohong) |
| 41   | [I4MBRI](https://gitee.com/openharmony/communication_wifi/issues/I4MBRI?from=project-issue) | 【新增特性】支持SoftAP基础特性                               | 标准系统 | SIG_SoftBus                       | [@cheng_guohong](https://gitee.com/cheng_guohong) |
| 42   | [I4MBRJ](https://gitee.com/openharmony/communication_wifi/issues/I4MBRJ?from=project-issue) | 【新增特性】提供WiFi模块的维测能力                           | 标准系统 | SIG_SoftBus                       | [@cheng_guohong](https://gitee.com/cheng_guohong) |
| 43   | [I4MBRK](https://gitee.com/openharmony/usb_manager/issues/I4MBRK?from=project-issue) | 【新增特性】USB服务JS接口实现                                | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2)                   |
| 44   | [I4MBRP](https://gitee.com/openharmony/sensors_sensor/issues/I4MBRP?from=project-issue) | 【泛Sensor】地磁场偏角和倾角                                 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2)                   |
| 45   | [I4MBRQ](https://gitee.com/openharmony/sensors_sensor/issues/I4MBRQ?from=project-issue) | 【泛Sensor】地磁场偏角和倾角                                 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2)                   |
| 45   | [I4MBRR](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4MBRR?from=project-issue) | 【资料】distributed_kv_store分布式数据库支持按谓词查询条件进行数据库记录的跨设备同步和订阅 | 标准系统 | SIG_DataManagement                | [@widecode](https://gitee.com/widecode)           |
| 46   | [I4MBRS](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4MBRS?from=project-issue) | 【distributed_kv_store】分布式数据库支持按谓词查询条件进行数据库记录的跨设备同步和订阅 | 标准系统 | SIG_DataManagement                | [@widecode](https://gitee.com/widecode)           |
| 47   | [I4MBRT](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4MBRT?from=project-issue) | 【资料】RDB提供数据库级安全加密                              | 标准系统 | SIG_DataManagement                | [@mangtsang](https://gitee.com/mangtsang)         |
| 48   | [I4MBRU](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4MBRU?from=project-issue) | 【RDB】支持数据库加密                                        | 标准系统 | SIG_DataManagement                | [@mangtsang](https://gitee.com/mangtsang)         |
| 49   | [I4MBRV](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4MBRV?from=project-issue) | 【samgr】系统服务状态监控                                    | 标准系统 | SIG_BasicSoftwareService          | [@lijiarun](https://gitee.com/lijiarun)           |
| 50   | [I4MBRW](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4MBRW?from=project-issue) | 【samgr】服务进程内的SA名单管控                              | 标准系统 | SIG_BasicSoftwareService          | [@lijiarun](https://gitee.com/lijiarun)           |
| 51   | [I4MBRX](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4MBRX?from=project-issue) | 【samgr】加载指定系统服务                                    | 标准系统 | SIG_BasicSoftwareService          | [@lijiarun](https://gitee.com/lijiarun)           |
| 52   | [I4MBRY](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4MBRY?from=project-issue) | 【samgr】系统服务进程管理                                    | 标准系统 | SIG_BasicSoftwareService          | [@lijiarun](https://gitee.com/lijiarun)           |
| 53   | [I4MBRZ](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4MBRZ?from=project-issue) | 【samgr】全量服务列表初始化                                  | 标准系统 | SIG_BasicSoftwareService          | [@lijiarun](https://gitee.com/lijiarun)           |
| 54   | [I4MBS0](https://gitee.com/openharmony/communication_dsoftbus/issues/I4MBS0?from=project-issue) | 【新增特性】【组网】软总线支持网络切换组网                   | 标准系统 | SIG_SoftBus                       | [@bigpumpkin](https://gitee.com/bigpumpkin)       |
| 55   | [I4MBS1](https://gitee.com/openharmony/communication_dsoftbus/issues/I4MBS1?from=project-issue) | 【新增特性】【传输】软总线提供传输ExtAPI接口                 | 标准系统 | SIG_SoftBus                       | [@bigpumpkin](https://gitee.com/bigpumpkin)       |
| 56   | [I4MBS2](https://gitee.com/openharmony/distributeddatamgr_file/issues/I4MBS2?from=project-issue) | 【新增特性】支持statfs API能力需求                           | 标准系统 | SIG_DataManagement                | [@zhangzhiwi](https://gitee.com/zhangzhiwi)       |
| 57   | [I4MBS3](https://gitee.com/openharmony/notification_ans_standard/issues/I4MBS3?from=project-issue) | 【新增特性】支持长时任务通知                                 | 标准系统 | SIG_BasicSoftwareService          | [@autumn330](https://gitee.com/autumn330)         |
| 58   | [I4MBS4](https://gitee.com/openharmony/notification_ans_standard/issues/I4MBS4?from=project-issue) | 【新增特性】通知系统API支持权限管理                          | 标准系统 | SIG_BasicSoftwareService          | [@autumn330](https://gitee.com/autumn330)         |
| 59   | [I4MBS5](https://gitee.com/openharmony/notification_ans_standard/issues/I4MBS5?from=project-issue) | 【新增特性】支持设置通知振动                                 | 标准系统 | SIG_BasicSoftwareService          | [@autumn330](https://gitee.com/autumn330)         |
| 60   | [I4MBS6](https://gitee.com/openharmony/notification_ans_standard/issues/I4MBS6?from=project-issue) | 【新增特性】支持通知声音设置和查询                           | 标准系统 | SIG_BasicSoftwareService          | [@autumn330](https://gitee.com/autumn330)         |
| 61   | [I4MBS7](https://gitee.com/openharmony/notification_ans_standard/issues/I4MBS7?from=project-issue) | 【新增特性】通知支持免打扰                                   | 标准系统 | SIG_BasicSoftwareService          | [@autumn330](https://gitee.com/autumn330)         |
| 62   | [I4MBS8](https://gitee.com/openharmony/notification_ans_standard/issues/I4MBS8?from=project-issue) | 【新增特性】支持会话类通知                                   | 标准系统 | SIG_BasicSoftwareService          | [@autumn330](https://gitee.com/autumn330)         |
| 63   | [I4MBS9](https://gitee.com/openharmony/notification_ces_standard/issues/I4MBS9?from=project-issue) | 【新增特性】EventHandler支持hitrace                          | 标准系统 | SIG_BasicSoftwareService          | [@autumn330](https://gitee.com/autumn330)         |
| 64   | [I4MBSA](https://gitee.com/openharmony/notification_ces_standard/issues/I4MBSA?from=project-issue) | 【新增特性】支持系统公共事件管理特性                         | 标准系统 | SIG_BasicSoftwareService          | [@autumn330](https://gitee.com/autumn330)         |
| 65   | [I4MBSB](https://gitee.com/openharmony/notification_ces_standard/issues/I4MBSB?from=project-issue) | 【新增特性】支持eventEmitter                                 | 标准系统 | SIG_BasicSoftwareService          | [@autumn330](https://gitee.com/autumn330)         |
| 66   | [I4MBSC](https://gitee.com/openharmony/appexecfwk_standard/issues/I4MBSC?from=project-issue) | 【增强特性】支持Module和Ability的srcPath字段                 | 标准系统 | SIG_ApplicationFramework          | [@gwang2008](https://gitee.com/gwang2008)         |
| 67   | [I4MBSD](https://gitee.com/openharmony/appexecfwk_standard/issues/I4MBSD?from=project-issue) | 【新增特性】支持多hap包安装                                  | 标准系统 | SIG_ApplicationFramework          | [@gwang2008](https://gitee.com/gwang2008)         |
| 68   | [I4MBSE](https://gitee.com/openharmony/appexecfwk_standard/issues/I4MBSE?from=project-issue) | 【新增特性】提供桌面包管理客户端                             | 标准系统 | SIG_ApplicationFramework          | [@gwang2008](https://gitee.com/gwang2008)         |
| 69   | [I4MBSF](https://gitee.com/openharmony/appexecfwk_standard/issues/I4MBSF?from=project-issue) | 【新增特性】提供清除缓存数据js api                           | 标准系统 | SIG_ApplicationFramework          | [@gwang2008](https://gitee.com/gwang2008)         |
| 70   | [I4MBSG](https://gitee.com/openharmony/appexecfwk_standard/issues/I4MBSG?from=project-issue) | 【增强特性】安装包信息查询                                   | 标准系统 | SIG_ApplicationFramework          | [@gwang2008](https://gitee.com/gwang2008)         |
| 71   | [I4MBSH](https://gitee.com/openharmony/appexecfwk_standard/issues/I4MBSH?from=project-issue) | 【新增特性】多hap安装时的签名校验                            | 标准系统 | SIG_ApplicationFramework          | [@gwang2008](https://gitee.com/gwang2008)         |
| 72   | [I4MBSI](https://gitee.com/openharmony/aafwk_standard/issues/I4MBSI?from=project-issue) | 【新增特性】ZIDL工具自动生成Extension C++服务端及客户端接口文件 | 标准系统 | SIG_ApplicationFramework          | [@gwang2008](https://gitee.com/gwang2008)         |
| 73   | [I4MBT4](https://gitee.com/openharmony/aafwk_standard/issues/I4MBT4?from=project-issue) | 【增强特性】支持常驻进程开机启动                             | 标准系统 | SIG_ApplicationFramework          | [@gwang2008](https://gitee.com/gwang2008)         |
| 74   | [I4MBTN](https://gitee.com/openharmony/kernel_linux_5.10/issues/I4MBTN?from=project-issue) | 【新增特性】支持CMA复用特性                                  | 标准系统 | SIG_Kernel                        | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 75   | [I4MBTO](https://gitee.com/openharmony/third_party_musl/issues/I4MBTO?from=project-issue) | 【新增特性】支持内存占用分类查询                             | 标准系统 | SIG_Kernel                        | [@liuyoufang](https://gitee.com/liuyoufang)     |
| 76   | [I4MBTP](https://gitee.com/openharmony/drivers_peripheral/issues/I4MBTP?from=project-issue) | 【增强特性】传感器驱动模型能力增强                           | 标准系统 | SIG_DriverFramework               | [@zianed](https://gitee.com/zianed)               |
| 77   | [I4MBTQ](https://gitee.com/openharmony/drivers_peripheral/issues/I4MBTQ?from=project-issue) | 【增强特性】传感器器件驱动能力增强                           | 标准系统 | SIG_DriverFramework               | [@zianed](https://gitee.com/zianed)               |
| 78   | [I4MBTR](https://gitee.com/openharmony/drivers_peripheral/issues/I4MBTR?from=project-issue) | 【新增特性】Display-Layer HDI接口针对L2的参考实现；  Display-Gralloc HDI接口针对L2的参考实现；  Display-Device  HDI接口针对L2的参考实现； | 标准系统 | SIG_DriverFramework               | [@zianed](https://gitee.com/zianed)               |
| 79   | [I4MBTS](https://gitee.com/openharmony/drivers_framework/issues/I4MBTS?from=project-issue) | 【增强特性】 HDF-Input设备能力丰富                           | 标准系统 | SIG_DriverFramework               | [@zianed](https://gitee.com/zianed)               |
| 80   | [I4MBTT](https://gitee.com/openharmony/drivers_framework/issues/I4MBTT?from=project-issue) | 【新增特性】支持Linux/Liteos-a内核系统级休眠唤醒             | 标准系统 | SIG_DriverFramework               | [@zianed](https://gitee.com/zianed)               |
| 81   | [I4MBTU](https://gitee.com/openharmony/drivers_framework/issues/I4MBTU?from=project-issue) | 【新增特性】支持同步/异步电源管理调用                        | 标准系统 | SIG_DriverFramework               | [@zianed](https://gitee.com/zianed)               |
| 82   | [I4MBTV](https://gitee.com/openharmony/drivers_framework/issues/I4MBTV?from=project-issue) | 【新增特性】提供hcs宏式解析接口                              | 标准系统 | SIG_DriverFramework               | [@zianed](https://gitee.com/zianed)               |
| 83   | [I4MBU1](https://gitee.com/openharmony/applications_settings/issues/I4MBU1?from=project-issue) | 【设置公共数据存储】Settings数据管理API                      | 标准系统 | SIG_SystemApplication             | [@lv-zhongwei](https://gitee.com/lv-zhongwei)     |
| 84   | [I4MBU3](https://gitee.com/openharmony/applications_settings/issues/I4MBU3?from=project-issue) | 【设置】系统-时间设置                                        | 标准系统 | SIG_SystemApplication             | [@lv-zhongwei](https://gitee.com/lv-zhongwei)     |
| 85   | [I4MBU5](https://gitee.com/openharmony/applications_settings/issues/I4MBU5?from=project-issue) | 【设置】声音管理                                             | 标准系统 | SIG_SystemApplication             | [@lv-zhongwei](https://gitee.com/lv-zhongwei)     |
| 86   | [I4MBU6](https://gitee.com/openharmony/applications_settings/issues/I4MBU6?from=project-issue) | 【设置】基础能力-数据管理                                    | 标准系统 | SIG_SystemApplication             | [@lv-zhongwei](https://gitee.com/lv-zhongwei)     |
| 87   | [I4MBU7](https://gitee.com/openharmony/applications_settings/issues/I4MBU7?from=project-issue) | 【设置】基础能力-默认值管理                                  | 标准系统 | SIG_SystemApplication             | [@lv-zhongwei](https://gitee.com/lv-zhongwei)     |
| 88   | [I4MBU8](https://gitee.com/openharmony/applications_settings/issues/I4MBU8?from=project-issue) | 【设置】基础能力-多设备形态差异化构建                        | 标准系统 | SIG_SystemApplication             | [@lv-zhongwei](https://gitee.com/lv-zhongwei)     |
| 89   | [I4MBU9](https://gitee.com/openharmony/applications_systemui/issues/I4MBU9?from=project-issue) | 【SystemUI】【通知】通知组件化                               | 标准系统 | SIG_SystemApplication             | [@lv-zhongwei](https://gitee.com/lv-zhongwei)     |
| 90   | [I4MBUB](https://gitee.com/openharmony/ark_ts2abc/issues/I4MBUB?from=project-issue) | 【新增特性】提供windows/MacOS/Linux的前端编译工具链          | 标准系统 | SIG_CompileRuntime                | [@godmiaozi](https://gitee.com/godmiaozi)         |
| 91   | [I4MBUC](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUC?from=project-issue) | 【新增特性】Openharmony jsapi替换为ark版本                   | 标准系统 | SIG_CompileRuntime                | [@wuzhefengh](https://gitee.com/wuzhefengh)       |
| 92   | [I4MBUD](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUD?from=project-issue) | 【新增规格】内存管理分配回收功能/Concurrent mark算法以及Concurrent Sweep实现 | 标准系统 | SIG_CompileRuntime                | [@wuzhefengh](https://gitee.com/wuzhefengh)       |
| 93   | [I4MBUE](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUE?from=project-issue) | 【新增特性】OpenharmonyOS上默认内置应用替换为ark版本         | 标准系统 | SIG_CompileRuntime                | [@wuzhefengh](https://gitee.com/wuzhefengh)       |
| 94   | [I4MBUF](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUF?from=project-issue) | 【增强特性】Inline Cache功能                                 | 标准系统 | SIG_CompileRuntime                | [@wuzhefengh](https://gitee.com/wuzhefengh)       |
| 95   | [I4MBUG](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUG?from=project-issue) | 【增强特性】支持解释器call指令优化                           | 标准系统 | SIG_CompileRuntime                | [@wuzhefengh](https://gitee.com/wuzhefengh)       |
| 96   | [I4MBUH](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUH?from=project-issue) | 【新增规格】CPU Profiler运行时实现                           | 标准系统 | SIG_CompileRuntime                | [@wuzhefengh](https://gitee.com/wuzhefengh)       |
| 97   | [I4MBUI](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUI?from=project-issue) | 【新增规格】内存管理分配回收功能/ 支持Old Space的Partial GC  | 标准系统 | SIG_CompileRuntime                | [@wuzhefengh](https://gitee.com/wuzhefengh)       |
| 98   | [I4MBUJ](https://gitee.com/openharmony/ark_ts2abc/issues/I4MBUJ?from=project-issue) | 【新增特性】OpenHarmony应用工程编译构建能力  【描述】  1、在正常构建场景下，能够将开发者程序代码编译成方舟字节码  2、在编译出现错误时，输出准确编译错误提示信息 | 标准系统 | SIG_CompileRuntime                | [@godmiaozi](https://gitee.com/godmiaozi)         |
| 99   | [I4MBUK](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUK?from=project-issue) | 【新增规格】JS运行时支持预览器                               | 标准系统 | SIG_CompileRuntime                | [@wuzhefengh](https://gitee.com/wuzhefengh)       |
| 100  | [I4MBUL](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUL?from=project-issue) | 【新增规格】方舟支持调试并且支持attach模式                   | 标准系统 | SIG_CompileRuntime                | [@wuzhefengh](https://gitee.com/wuzhefengh)       |
| 101  | [I4MBUM](https://gitee.com/openharmony/js_util_module/issues/I4MBUM?from=project-issue) | 【新增规格】提供libc，c++，clang基础测试框架                 | 标准系统 | SIG_CompileRuntime                | [@wuzhefengh](https://gitee.com/wuzhefengh)       |
| 102  | [I4MBUN](https://gitee.com/openharmony/js_sys_module/issues/I4MBUN?from=project-issue) | 【新增规格】支持utils特性  /提供process接口规格              | 标准系统 | SIG_CompileRuntime                | [@wuzhefengh](https://gitee.com/wuzhefengh)       |
| 103  | [I4MBUO](https://gitee.com/openharmony/js_util_module/issues/I4MBUO?from=project-issue) | 【新增规格】支持utils特性  /提供提供Scope接口规格            | 标准系统 | SIG_CompileRuntime                | [@wuzhefengh](https://gitee.com/wuzhefengh)       |
| 104  | [I4MBUP](https://gitee.com/openharmony/js_util_module/issues/I4MBUP?from=project-issue) | 【新增规格】支持utils特性  /提供提供Base64接口规格           | 标准系统 | SIG_CompileRuntime                | [@wuzhefengh](https://gitee.com/wuzhefengh)       |
| 105  | [I4MBUQ](https://gitee.com/openharmony/js_util_module/issues/I4MBUQ?from=project-issue) | 【新增规格】支持utils特性  /提供提供RationalNumber接口规格   | 标准系统 | SIG_CompileRuntime                | [@wuzhefengh](https://gitee.com/wuzhefengh)       |
| 106  | [I4MBUR](https://gitee.com/openharmony/js_util_module/issues/I4MBUR?from=project-issue) | 【新增规格】支持语言增强特性/提供JS Typeof 接口规格          | 标准系统 | SIG_CompileRuntime                | [@wuzhefengh](https://gitee.com/wuzhefengh)       |
| 107  | [I4MBUS](https://gitee.com/openharmony/js_api_module/issues/I4MBUS?from=project-issue) | 【新增规格】支持URI特性 /提供 URI解析接口规格                | 标准系统 | SIG_CompileRuntime                | [@wuzhefengh](https://gitee.com/wuzhefengh)       |
| 108  | [I4MBUT](https://gitee.com/openharmony/js_util_module/issues/I4MBUT?from=project-issue) | 【新增规格】支持utils特性提供LRUBuffer接口规格               | 标准系统 | SIG_CompileRuntime                | [@wuzhefengh](https://gitee.com/wuzhefengh)       |
| 109  | [I4MBUU](https://gitee.com/openharmony/js_api_module/issues/I4MBUU?from=project-issue) | 【新增规格】支持XML特性/提供XmlPullParser 接口规格           | 标准系统 | SIG_CompileRuntime                | [@wuzhefengh](https://gitee.com/wuzhefengh)       |
| 110  | [I4MBUV](https://gitee.com/openharmony/js_api_module/issues/I4MBUV?from=project-issue) | 【新增规格】支持XML特性/提供xmlSerializer 接口规格           | 标准系统 | SIG_CompileRuntime                | [@wuzhefengh](https://gitee.com/wuzhefengh)       |
| 111  | [I4MBUW](https://gitee.com/openharmony/js_api_module/issues/I4MBUW?from=project-issue) | 【新增规格】支持XML特性/提供xml2JSObject 接口规格            | 标准系统 | SIG_CompileRuntime                | [@wuzhefengh](https://gitee.com/wuzhefengh)       |
| 112  | [I4MBUX](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBUX?from=project-issue) | 【新增规格】资源管理特性对接全球化规格                       | 标准系统 | SIG_ApplicationFramework          | [@davidwulanxi](https://gitee.com/davidwulanxi)   |
| 113  | [I4MBUY](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBUY?from=project-issue) | 【新增规格】事件中增加Target获取尺寸                         | 标准系统 | SIG_ApplicationFramework          | [@davidwulanxi](https://gitee.com/davidwulanxi)   |
| 114  | [I4MBUZ](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBUZ?from=project-issue) | 【新增规格】Swiper组件支持设置缓存cache                      | 标准系统 | SIG_ApplicationFramework          | [@davidwulanxi](https://gitee.com/davidwulanxi)   |
| 115  | [I4MBV1](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBV1?from=project-issue) | 【新增规格】Image组件支持同步、异步渲染设置                  | 标准系统 | SIG_ApplicationFramework          | [@davidwulanxi](https://gitee.com/davidwulanxi)   |
| 116  | [I4MBV3](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBV3?from=project-issue) | 【新增规格】样式设置特性增加组件多态样式设置规格             | 标准系统 | SIG_ApplicationFramework          | [@davidwulanxi](https://gitee.com/davidwulanxi)   |
| 117  | [I4MBV5](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBV5?from=project-issue) | 【新增规格】字母索引条组件增加提示菜单内容扩展规格           | 标准系统 | SIG_ApplicationFramework          | [@davidwulanxi](https://gitee.com/davidwulanxi)   |
| 118  | [I4MBV6](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBV6?from=project-issue) | 【新增规格】组件自定义特性增加自定义容器组件规格             | 标准系统 | SIG_ApplicationFramework          | [@davidwulanxi](https://gitee.com/davidwulanxi)   |
| 119  | [I4MBV7](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBV7?from=project-issue) | 【新增规格】滚动条样式自定义能力                             | 标准系统 | SIG_ApplicationFramework          | [@davidwulanxi](https://gitee.com/davidwulanxi)   |
| 120  | [I4MBV8](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBV8?from=project-issue) | 【新增规格】Swiper组件新增切换禁用规格                       | 标准系统 | SIG_ApplicationFramework          | [@davidwulanxi](https://gitee.com/davidwulanxi)   |
| 121  | [I4MBV9](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBV9?from=project-issue) | 【新增规格】Tabs组件新增TabBar内容自定义规格                 | 标准系统 | SIG_ApplicationFramework          | [@davidwulanxi](https://gitee.com/davidwulanxi)   |
| 122  | [I4MBVA](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVA?from=project-issue) | 【新增规格】Navigation组件新增标题栏设置规格                 | 标准系统 | SIG_ApplicationFramework          | [@davidwulanxi](https://gitee.com/davidwulanxi)   |
| 123  | [I4MBVB](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVB?from=project-issue) | 【新增规格】工具栏组件增加工具栏显隐控制规格                 | 标准系统 | SIG_ApplicationFramework          | [@davidwulanxi](https://gitee.com/davidwulanxi)   |
| 124  | [I4MBVC](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVC?from=project-issue) | 【新增规格】工具栏组件增加内容自定义能力规格                 | 标准系统 | SIG_ApplicationFramework          | [@davidwulanxi](https://gitee.com/davidwulanxi)   |
| 125  | [I4MBVD](https://gitee.com/openharmony/interface_sdk-js/issues/I4MBVD?from=project-issue) | 【新增特性】新增SysCap声明编译特性                           | 标准系统 | SIG_ApplicationFramework          | [@karl-z](https://gitee.com/karl-z)               |
| 126  | [I4MBVE](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVE?from=project-issue) | 【新增特性】新增JS SDK编译特性                               | 标准系统 | SIG_ApplicationFramework          | [@lihong67](https://gitee.com/lihong67)           |
| 127  | [I4MBVF](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVF?from=project-issue) | 【新增特性】新增Config.json编译特性                          | 标准系统 | SIG_ApplicationFramework          | [@lihong67](https://gitee.com/lihong67)           |
| 128  | [I4MBVG](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVG?from=project-issue) | 【新增规格】新增断点调试特性支持单实例调试                   | 标准系统 | SIG_ApplicationFramework          | [@lihong67](https://gitee.com/lihong67)           |
| 129  | [I4MBVH](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVH?from=project-issue) | 【新增规格】新增attach调试特性支持单实例调试                 | 标准系统 | SIG_ApplicationFramework          | [@lihong67](https://gitee.com/lihong67)           |
| 130  | [I4MBVI](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVI?from=project-issue) | 【新增规格】新增声明式范式编译特性支持编译和校验规格         | 标准系统 | SIG_ApplicationFramework          | [@lihong67](https://gitee.com/lihong67)           |
| 131  | [I4MBVJ](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVJ?from=project-issue) | 【新增特性】新增JS模块共享编译特性                           | 标准系统 | SIG_ApplicationFramework          | [@lihong67](https://gitee.com/lihong67)           |
| 132  | [I4MBVK](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVK?from=project-issue) | 【新增特性】新增HAR引用和编译特性                            | 标准系统 | SIG_ApplicationFramework          | [@lihong67](https://gitee.com/lihong67)           |
| 133  | [I4MBVL](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVL?from=project-issue) | 【新增特性】新增NPM引用和编译特性                            | 标准系统 | SIG_ApplicationFramework          | [@lihong67](https://gitee.com/lihong67)           |
| 134  | [I4MBVM](https://gitee.com/openharmony/docs/issues/I4MBVM?from=project-issue) | 【资料】ace_engine_standard部件IT2版本资料录入需求           | 标准系统 | SIG_Docs                          | [@neeen](https://gitee.com/neeen)                 |
| 135  | [I4MBVN](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVN?from=project-issue) | 【新增特性】纵向显示滑动条组件特性                           | 标准系统 | SIG_ApplicationFramework          | [@davidwulanxi](https://gitee.com/davidwulanxi)   |
| 136  | [I4MBVO](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVO?from=project-issue) | 【新增特性】Popup组件增加内容自定义规格                      | 标准系统 | SIG_ApplicationFramework          | [@davidwulanxi](https://gitee.com/davidwulanxi)   |
| 137  | [I4MBVP](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVP?from=project-issue) | 【新增特性】Canvas绘制能力支持                               | 标准系统 | SIG_ApplicationFramework          | [@davidwulanxi](https://gitee.com/davidwulanxi)   |
| 138  | [I4MBVQ](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVQ?from=project-issue) | 【新增规格】Canvas能力增强                                   | 标准系统 | SIG_ApplicationFramework          | [@davidwulanxi](https://gitee.com/davidwulanxi)   |
| 139  | [I4MBVR](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVR?from=project-issue) | 【新增特性】触摸响应热区设置                                 | 标准系统 | SIG_ApplicationFramework          | [@davidwulanxi](https://gitee.com/davidwulanxi)   |
| 140  | [I4MBVS](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVS?from=project-issue) | 【新增特性】Lottie动画支持                                   | 标准系统 | SIG_ApplicationFramework          | [@davidwulanxi](https://gitee.com/davidwulanxi)   |
| 141  | [I4MBVT](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVT?from=project-issue) | 【新增特性】组件尺寸获取特性                                 | 标准系统 | SIG_ApplicationFramework          | [@davidwulanxi](https://gitee.com/davidwulanxi)   |
| 142  | [I4MBVU](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVU?from=project-issue) | 【新增特性】Menu组件增加内容自定义规格                       | 标准系统 | SIG_ApplicationFramework          | [@davidwulanxi](https://gitee.com/davidwulanxi)   |
| 143  | [I4MBVV](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVV?from=project-issue) | 【新增特性】Swipe手势特性                                    | 标准系统 | SIG_ApplicationFramework          | [@davidwulanxi](https://gitee.com/davidwulanxi)   |
| 144  | [I4MBVW](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVW?from=project-issue) | 【新增特性】UI预览支持Inspector能力                          | 标准系统 | SIG_ApplicationFramework          | [@lihong67](https://gitee.com/lihong67)           |
| 145  | [I4MBVX](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVX?from=project-issue) | 【新增特性】新增非路由文件预览特性                           | 标准系统 | SIG_ApplicationFramework          | [@lihong67](https://gitee.com/lihong67)           |
| 146  | [I4MBVY](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVY?from=project-issue) | 【新增特性】新增NAPI预览特性                                 | 标准系统 | SIG_ApplicationFramework          | [@lihong67](https://gitee.com/lihong67)           |
| 147  | [I4MBVZ](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVZ?from=project-issue) | 【新增规格】新增声明式范式预览特性支持基础预览规格           | 标准系统 | SIG_ApplicationFramework          | [@lihong67](https://gitee.com/lihong67)           |
| 148  | [I4MBW2](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBW2?from=project-issue) | 【新增规格】新增声明式范式热加载特性支持已有节点修改规格     | 标准系统 | SIG_ApplicationFramework          | [@lihong67](https://gitee.com/lihong67)           |
| 149  | [I4MBW3](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBW3?from=project-issue) | 【新增规格】新增声明式范式热加载特性支持新增节点规格         | 标准系统 | SIG_ApplicationFramework          | [@lihong67](https://gitee.com/lihong67)           |
| 150  | [I4MBW4](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBW4?from=project-issue) | 【新增规格】新增声明式范式热加载特性支持删除节点规格         | 标准系统 | SIG_ApplicationFramework          | [@lihong67](https://gitee.com/lihong67)           |
| 151  | [I4MBW5](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBW5?from=project-issue) | 【新增规格】新增组件预览特性支持页面组件预览规格             | 标准系统 | SIG_ApplicationFramework          | [@lihong67](https://gitee.com/lihong67)           |
| 152  | [I4NY1T](https://gitee.com/openharmony/device_profile_core/issues/I4NY1T?from=project-issue) | 【device_profile】订阅profile信息变化                        | 标准系统 | SIG_BasicSoftwareService          | [@lijiarun](https://gitee.com/lijiarun)           |
| 153  | [I4NY1U](https://gitee.com/openharmony/device_profile_core/issues/I4NY1U?from=project-issue) | 【device_profile】订阅同步通知                               | 标准系统 | SIG_BasicSoftwareService          | [@lijiarun](https://gitee.com/lijiarun)           |
| 154  | [I4NY1V](https://gitee.com/openharmony/device_profile_core/issues/I4NY1V?from=project-issue) | 【device_profile】CS采集OS特征信息                           | 标准系统 | SIG_BasicSoftwareService          | [@lijiarun](https://gitee.com/lijiarun)           |
| 155  | [I4NY1W](https://gitee.com/openharmony/device_profile_core/issues/I4NY1W?from=project-issue) | 【device_profile】向业务端提供同步profile能力                | 标准系统 | SIG_BasicSoftwareService          | [@lijiarun](https://gitee.com/lijiarun)           |
| 156  | [I4NY1X](https://gitee.com/openharmony/device_profile_core/issues/I4NY1X?from=project-issue) | 【device_profile】提供查询远程设备profile记录功能            | 标准系统 | SIG_BasicSoftwareService          | [@lijiarun](https://gitee.com/lijiarun)           |
| 157  | [I4NY1Z](https://gitee.com/openharmony/device_profile_core/issues/I4NY1Z?from=project-issue) | 【device_profile】profile上线同步（wifi组网下）              | 标准系统 | SIG_BasicSoftwareService          | [@lijiarun](https://gitee.com/lijiarun)           |
| 158  | [I4NY21](https://gitee.com/openharmony/device_profile_core/issues/I4NY21?from=project-issue) | 【device_profile】提供删除本地profile记录功能                | 标准系统 | SIG_BasicSoftwareService          | [@lijiarun](https://gitee.com/lijiarun)           |
| 159  | [I4NY22](https://gitee.com/openharmony/device_profile_core/issues/I4NY22?from=project-issue) | 【device_profile】提供查询本地profile记录功能                | 标准系统 | SIG_BasicSoftwareService          | [@lijiarun](https://gitee.com/lijiarun)           |
| 160  | [I4NY23](https://gitee.com/openharmony/device_profile_core/issues/I4NY23?from=project-issue) | 【device_profile】提供写入profile记录功能                    | 标准系统 | SIG_BasicSoftwareService          | [@lijiarun](https://gitee.com/lijiarun)           |
| 161  | [I4MBRC](https://gitee.com/openharmony/developtools_profiler/issues/I4MBRC?from=project-issue) | 【hiperf部件】采样数据展示                                   | 标准系统 | SIG_R&DToolChain                  | [@wangzaishang](https://gitee.com/wangzaishang)   |
| 162  | [I4MBRD](https://gitee.com/openharmony/developtools_profiler/issues/I4MBRD?from=project-issue) | 【hiperf部件】性能数据采样记录                               | 标准系统 | SIG_R&DToolChain                  | [@wangzaishang](https://gitee.com/wangzaishang)   |
| 163  | [I4MBRE](https://gitee.com/openharmony/developtools_profiler/issues/I4MBRE?from=project-issue) | 【hiperf部件】性能数据计数统计                               | 标准系统 | SIG_R&DToolChain                  | [@wangzaishang](https://gitee.com/wangzaishang)   |
| 164  | [I4MBU1](https://gitee.com/openharmony/applications_settings/issues/I4MBU1?from=project-issue) | 【设置公共数据存储】Settings数据管理API                      | 标准系统 | SIG_SystemApplication             | [@lv-zhongwei](https://gitee.com/lv-zhongwei)     |
| 165  | [I4MBU8](https://gitee.com/openharmony/applications_settings/issues/I4MBU8?from=project-issue) | 【设置】基础能力-多设备形态差异化构建                        | 标准系统 | SIG_SystemApplication             | [@lv-zhongwei](https://gitee.com/lv-zhongwei)     |
| 166  | [I4MBU9](https://gitee.com/openharmony/applications_systemui/issues/I4MBU9?from=project-issue) | 【SystemUI】【通知】通知组件化                               | 标准系统 | SIG_SystemApplication             | [@lv-zhongwei](https://gitee.com/lv-zhongwei)     |
| 167  | [I4MBU3](https://gitee.com/openharmony/applications_settings/issues/I4MBU3?from=project-issue) | 【设置】系统-时间设置                                        | 标准系统 | SIG_SystemApplication             | [@lv-zhongwei](https://gitee.com/lv-zhongwei)     |
| 168  | [I4MBU5](https://gitee.com/openharmony/applications_settings/issues/I4MBU5?from=project-issue) | 【设置】声音管理                                             | 标准系统 | SIG_SystemApplication             | [@lv-zhongwei](https://gitee.com/lv-zhongwei)     |
| 169  | [I4MBU6](https://gitee.com/openharmony/applications_settings/issues/I4MBU6?from=project-issue) | 【设置】基础能力-数据管理                                    | 标准系统 | SIG_SystemApplication             | [@lv-zhongwei](https://gitee.com/lv-zhongwei)     |
| 170  | [I4MBU7](https://gitee.com/openharmony/applications_settings/issues/I4MBU7?from=project-issue) | 【设置】基础能力-默认值管理                                  | 标准系统 | SIG_SystemApplication             | [@lv-zhongwei](https://gitee.com/lv-zhongwei)     |
| 171 | [I4WTQI](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTQY) | 【新增特性】线性占比显示控件特性支持                          | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 172 | [I4WTQI](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTRD) | 【新增特性】PluginComponent组件                          | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 173 | [I4WTQI](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTRE) | 【新增特性】PluginComponentManager接口                         | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 174 | [I4WTQI](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTQ3) | 【新增规格】List组件增加内容拖拽能力规格                          | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 175 | [I4WTQI](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTQ4) | 【新增规格】Grid组件增加内容拖拽能力规格                          | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 176 | [I4WTQI](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTQ6) | 【新增规格】新增断点调试特性支持单实例调试                          | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 177 | [I4XJ4F](https://gitee.com/openharmony/ark_js_runtime/issues/I4XJ4F) | 【增强特性】Builtin Stub功能汇编化                           | 标准系统 | SIG_CompileRuntime                | [@wuzhefengh](https://gitee.com/wuzhefengh)  

## OpenHarmony 3.1.3.1版本特性清单：

| no   | issue                                                        | feture description                                           | platform       | sig                      | owner                                                 |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :------------- | :----------------------- | :---------------------------------------------------- |
| 1    | [I410YD](https://gitee.com/openharmony/powermgr_battery_manager/issues/I410YD) | 【充放电&Battery服务】 支持关机充电特性                      | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 2    | [I410Y1](https://gitee.com/openharmony/powermgr_battery_manager/issues/I410Y1) | 【充放电&Battery服务】 电池温度异常关机保护                  | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 3    | [I4OGD2](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGD2?from=project-issue) | 【资料】跨设备组件调用新增/增强特性资料说明                  | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 4    | [I4PDB8](https://gitee.com/openharmony/drivers_framework/issues/I4PDB8) | 【新增特性】提供设备PnP事件监听接口                          | 标准系统       | SIG_HardwareMgr          | [@yuanbogit](https://gitee.com/yuanbogit)             |
| 5    | [I4PDAV](https://gitee.com/openharmony/applications_contacts/issues/I4PDAV) | 【联系人】通话记录 - 基本内容（名字、号码、时间、来电次数等） | 标准系统       | SIG_SystemApplication    | [@lv-zhongwei](https://gitee.com/lv-zhongwei)         |
| 6    | [I4PDAW](https://gitee.com/openharmony/applications_contacts/issues/I4PDAW) | 【联系人】通话记录 - 通话记录列表显示（TAB/列表展示）        | 标准系统       | SIG_SystemApplication    | [@lv-zhongwei](https://gitee.com/lv-zhongwei)         |
| 7    | [I4PDAX](https://gitee.com/openharmony/applications_settings/issues/I4PDAX) | 【设置】显示管理                                             | 标准系统       | SIG_SystemApplication    | [@lv-zhongwei](https://gitee.com/lv-zhongwei)         |
| 8    | [I4PDAY](https://gitee.com/openharmony/applications_systemui/issues/I4PDAY) | 【SystemUI】【状态栏】提示胶囊                               | 标准系统       | SIG_SystemApplication    | [@lv-zhongwei](https://gitee.com/lv-zhongwei)         |
| 9    | [I4PCX8](https://gitee.com/openharmony/communication_ipc/issues/I4PCX8?from=project-issue) | 【RPC】进程间IPC、设备间RPC支持HiTrace能力                   | 标准系统       | SIG_SoftBus              | [@Xi_Yuhao](https://gitee.com/Xi_Yuhao)               |
| 10   | [I4PD3K](https://gitee.com/openharmony/startup_init_lite/issues/I4PD3K) | 进程退出后的回收处理策略配置能力增强                         | 标准系统       | SIG_BscSoftSrv           | [@xionglei16](https://gitee.com/xionglei16)           |
| 11   | [I4PD3C](https://gitee.com/openharmony/startup_init_lite/issues/I4PD3C) | 支持SA类进程按需启动                                         | 标准系统       | SIG_BscSoftSrv           | [@xionglei16](https://gitee.com/xionglei16)           |
| 12   | [I4NZVP](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4NZVP) | 【distributed_kv_store】提供分布式数据库JS API               | 标准系统       | SIG_DataManagement       | [@widecode](https://gitee.com/widecode)               |
| 13   | [I4WVMH](https://gitee.com/openharmony/security_access_token/issues/I4WVMH)  | 【新增规格】系统的应用权限初始化预置定义    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 14   | [I4WVO9](https://gitee.com/openharmony/security_access_token/issues/I4WVO9)  | 【新增规格】应用权限申请列表查询    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 15   | [I4WVPV](https://gitee.com/openharmony/security_access_token/issues/I4WVPV)  | 【新增规格】本地权限校验接口和机制    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 16   | [I4WVR3](https://gitee.com/openharmony/security_access_token/issues/I4WVR3)  | 【新增规格】应用权限的设置接口和机制    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 17   | [I4WVRG](https://gitee.com/openharmony/security_access_token/issues/I4WVRG)  | 【新增规格】AT管理服务基本框架    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 18   | [I4WVRR](https://gitee.com/openharmony/security_access_token/issues/I4WVRR)  | 【新增规格】Hap应用token查询接口     | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 19   | [I4WVS6](https://gitee.com/openharmony/security_access_token/issues/I4WVS6)  | 【新增规格】Hap应用的Token信息删除机制    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 20   | [I4WVSI](https://gitee.com/openharmony/security_access_token/issues/I4WVSI)  | 【新增规格】Hap应用的Token创建和更新机制   | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 21   | [I4WVTM](https://gitee.com/openharmony/security_selinux/issues/I4WVTM)  | 【新增特性】支持SELinux策略文件编译生成conf文件的编译框架    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 22   | [I4WVYE](https://gitee.com/openharmony/security_selinux/issues/I4WVYE)  | 【新增特性】支持SELinux conf文件策略生成二进制策略编译的编译框架    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 23 | [I4WTQI](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTQX) | 【新增特性】图案密码组件特性支持                          | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 24 | [I4WTQI](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTQH) | 【新增规格】FormComponent组件支持直接读取config文件获取配置信息                         | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 25 | [I4WTQI](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTQ0) | 【新增规格】输入组件支持设置光标位置                          | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 26 | [I4WTQI](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTQI) | 【新增规格】拖拽能力增加鼠标拖拽规格                          | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 27 | [I4WTQL](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTQL) | 【新增规格】支持系统应用弹窗功能                          | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 28 | [I4WWTB](https://gitee.com/openharmony/sensors_sensor/issues/I4WWTB) | 【泛sensor】支持通用的算法接口 | 标准系统 | SIG_DistributedHardwareManagement | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 29 | [I4WWT6](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWT6) | 【多模】触摸板输入事件注入功能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 30 | [I4WWSN](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWSN) | 【多模】订阅单系统按键输入事件 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 31 | [I4RKSM](https://gitee.com/openharmony/graphic_standard/issues/I4RKSM) | 【backstore部件】提供Buffer Queue机制 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 32 | [I4R9PC](https://gitee.com/openharmony/graphic_standard/issues/I4R9PC) | 【backstore部件】surface管理 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 33 | [I4R9PD](https://gitee.com/openharmony/graphic_standard/issues/I4R9PD) | 【backstore部件】平台窗口管理 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 34 | [I4R9P4](https://gitee.com/openharmony/windowmanager/issues/I4R9P4) | 【window_manager】窗口能力重构 增强特性：提供应用窗口创建管理能力 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 35 | [I4R301](https://gitee.com/openharmony/windowmanager/issues/I4R301) | 【window_manager】【新增特性】提供接口指导输入事件分发 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |

## OpenHarmony 3.1.3.2版本特性清单：

| no   | issue                                                        | feture description                                           | platform       | sig                      | owner                                                 |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :------------- | :----------------------- | :---------------------------------------------------- |
| 1    | [I4QGKM](https://gitee.com/openharmony/powermgr_battery_manager/issues/I4QGKM) | 【部件化专项】battery_manage部件标准化 | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 2    | [I412F4](https://gitee.com/openharmony/powermgr_power_manager/issues/I412F4) | 【省电模式】 支持省电模式                                    | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 3    | [I4GYBV](https://gitee.com/openharmony/powermgr_thermal_manager/issues/I4GYBV) | 【新增特性】提供温升监控接口                                 | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 4    | [I4P7FC](https://gitee.com/openharmony/third_party_musl/issues/I4P7FC) | [语言编译运行时子系统]提供NDK所需的musl版本的libc基础库以及相应头文件 | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 5    | [I4P7F9](https://gitee.com/openharmony/third_party_musl/issues/I4P7F9) | [语言编译运行时子系统]提供NDK所需的libc++库以及相应头文件    | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 6    | [I4OGCN](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCN?from=project-issue) | 【增强特性】【DMS】根据指定设备发起迁移能力，接收迁移结果    | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 7    | [I4OGCM](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCM?from=project-issue) | 【新增特性】【任务管理】提供获取实时任务接口  | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 8    | [I4PBOK](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBOK) | 【新增特性】通知支持多用户                                   | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 9    | [I4PCM4](https://gitee.com/openharmony/aafwk_standard/issues/I4PCM4) | 【新增特性】上下文提供应用/Hap包/组件信息查询能力            | 元能力子系统   | SIG_ApplicationFramework | [@silent-dye](https://gitee.com/silent-dye)           |
| 10   | [I4PCM8](https://gitee.com/openharmony/aafwk_standard/issues/I4PCM8) | 【新增特性】应用启动/组件切换流程trace                       | 元能力子系统   | SIG_ApplicationFramework | [@silent-dye](https://gitee.com/silent-dye)           |
| 11   | [I4PCNZ ](https://gitee.com/openharmony/aafwk_standard/issues/I4PCP6) | 【新增特性】服务组件泄漏检测                                 | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 12   | [I4PCPP](https://gitee.com/openharmony/aafwk_standard/issues/I4PCPP) | 【新增特性】上下文适配多用户                                 | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 13   | [I4PCPR](https://gitee.com/openharmony/aafwk_standard/issues/I4PCPR) | 【新增特性】非并发模式下，禁止非当前用户的应用通过其他方式自启 | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 14   | [I4PCPV](https://gitee.com/openharmony/aafwk_standard/issues/I4PCPV) | 【新增特性】提供指定用户启动组件的系统接口                   | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 15   | [I4PCQ1](https://gitee.com/openharmony/aafwk_standard/issues/I4PCQ1) | 【新增特性】提供指定用户管理应用的系统接口                   | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 16   | [I4PCQJ](https://gitee.com/openharmony/aafwk_standard/issues/I4PCQJ) | 【新增特性】对外接口适配多用户                               | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 17   | [I4PCQP](https://gitee.com/openharmony/aafwk_standard/issues/I4PCQP) | 【新增特性】支持singleuser的运行模式                         | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 18   | [I4PCQU](https://gitee.com/openharmony/aafwk_standard/issues/I4PCQU) | 【新增特性】启动初始化默认用户                               | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 19   | [I4PCQW](https://gitee.com/openharmony/aafwk_standard/issues/I4PCQW) | 【新增特性】启动用户                                         | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 20   | [I4PCQY](https://gitee.com/openharmony/aafwk_standard/issues/I4PCQY) | 【新增特性】切换用户                                         | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 21   | [I4PCR2](https://gitee.com/openharmony/aafwk_standard/issues/I4PCR2) | 【新增特性】停止用户                                         | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 22   | [I4PBP7](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBP7)|【新增特性】支持应用发送模板通知（调试能力）|事件通知子系统|SIG_ApplicationFramework|[@xzz_0810](https://gitee.com/xzz_0810)|
| 23   | [I4PBPE](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBPE)|【新增特性】支持进度条通知|事件通知子系统|SIG_ApplicationFramework|[@xzz_0810](https://gitee.com/xzz_0810)|
| 24   | [I4PCGY](https://gitee.com/openharmony/aafwk_standard/issues/I4PCGY)|【增强特性】新增卡片开发基类|元能力子系统|SIG_ApplicationFramework|[@lsq1474521181](https://gitee.com/lsq1474521181)|
| 25   | [I4PCH9](https://gitee.com/openharmony/aafwk_standard/issues/I4PCH9)|【增强特性】通过配置文件配置服务卡片|元能力子系统|SIG_ApplicationFramework|[@lsq1474521181](https://gitee.com/lsq1474521181)|
| 26   | [I4PCLL](https://gitee.com/openharmony/aafwk_standard/issues/I4PCLL)|【新增特性】JS提供的应用级别上下文|元能力子系统|SIG_ApplicationFramework|[@silent-dye](https://gitee.com/silent-dye)|
| 27   | [I4PCLN](https://gitee.com/openharmony/aafwk_standard/issues/I4PCLN)|【新增特性】Abilty的状态恢复|元能力子系统|SIG_ApplicationFramework|[@silent-dye](https://gitee.com/silent-dye)|
| 28   | [I4PCM6](https://gitee.com/openharmony/aafwk_standard/issues/I4PCM6)|【新增特性】提供应用或组件状态监听/查询能力|元能力子系统|SIG_ApplicationFramework|[@silent-dye](https://gitee.com/silent-dye)|
| 29   | [I4PCP1](https://gitee.com/openharmony/aafwk_standard/issues/I4PCP1)|【新增特性】应用运行信息查询|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu](https://gitee.com/xuezhongzhu)|
| 30   | [I4PCPG](https://gitee.com/openharmony/aafwk_standard/issues/I4PCPG)|【增强特性】支持系统环境变化通知|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu](https://gitee.com/xuezhongzhu)|
| 31   | [I4PCR8](https://gitee.com/openharmony/aafwk_standard/issues/I4PCR8)|【增强特性】支持常驻进程开机启动|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 32   | [I4PCSB](https://gitee.com/openharmony/aafwk_standard/issues/I4PCSB)|【新增特性】强制停止进程|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 33   | [I4PCV4](https://gitee.com/openharmony/aafwk_standard/issues/I4PCV4)|【新增特性】支持任务切换|元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 34   | [I4PCV9](https://gitee.com/openharmony/aafwk_standard/issues/I4PCV9)|【新增特性】多任务管理|元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 35   | [I4PCVA](https://gitee.com/openharmony/aafwk_standard/issues/I4PCVA)|【新增特性】支持任务锁|元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 36   | [I4PCVF](https://gitee.com/openharmony/aafwk_standard/issues/I4PCVF)|【新增特性】支持任务清除|元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 37   | [I4PCVZ](https://gitee.com/openharmony/aafwk_standard/issues/I4PCVZ)|【新增特性】支持指定displayId启动Ability|元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 38   | [I4PCW3](https://gitee.com/openharmony/aafwk_standard/issues/I4PCW3)|【增强特性】pendingwant机制支持跨设备启动通用组件|元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 39  | [I4PKYL](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYL) | 【新增特性】支持查询指定用户下的应用信息                     | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 40  | [I4PKYM](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYM) | 【新增特性】支持多用户创建                                   | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 41  | [I4PKYN](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYN) | 【新增特性】支持多用户删除                                   | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 42  | [I4PKYO](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYO) | 【新增特性】支持安装应用到指定用户                           | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 43  | [I4PKYP](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYP) | 【新增特性】支持卸载指定用户下的应用                         | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 44  | [I4PKY8](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PO7Z) | 【新增特性】installd 提供应用空间统计、cache统计             | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 45  | [I4PC12](https://gitee.com/openharmony/distributeddatamgr_file/issues/I4PC12) | 支持基本文件异步操作API需求                                  | 标准系统       | SIG_Kernel               | [@panqinxu](https://gitee.com/panqinxu)               |
| 46  | [I4PXMP](https://gitee.com/openharmony/build/issues/I4PXMP)  | [编译构建子系统]【增强特性】统一编译入口，轻量级和标准系统使用hb可编译 | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)       |
| 47  | [I4PXNS](https://gitee.com/openharmony/build/issues/I4PXNS)  | [编译构建子系统]【新增特性】提供NDK的编译模板               | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)       |
| 48  | [I4PXNZ](https://gitee.com/openharmony/build/issues/I4PXNZ)  | [编译构建子系统]【新增特性】 Native-SDK中提供cmake toolchain文件               | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)       |
| 49  | [I4PXRD](https://gitee.com/openharmony/build/issues/I4PXRD)  | [编译构建子系统]【新增特性】归一的部件定义和编译               | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)       |
| 50  | [I4JBFF](https://gitee.com/openharmony/account_os_account/issues/I4JBFF)  | [账号子系统]【新增特性】支持本地多用户信息查询               | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 51  | [I4IU2T](https://gitee.com/openharmony/account_os_account/issues/I4IU2T)  | [账号子系统]【新增特性】支持本地多用户订阅及取消订阅          | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 52  | [I4IU3B](https://gitee.com/openharmony/account_os_account/issues/I4IU3B)  | [账号子系统]【新增特性】支持本地多用户启动、停止、切换动作     | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 53  | [I4Q9QZ](https://gitee.com/openharmony/kernel_liteos_m/issues/I4Q9QZ)  | [内核子系统]【增强特性】支持南向接口融合    | 内核子系统 | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5)       |
| 54  | [I4Q8F8](https://gitee.com/openharmony/useriam_faceauth/issues/I4Q8F8)  | [用户IAM子系统]【新增特性】提供Standard人脸识别框架     | 用户IAM子系统 | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 55  | [I4Q8F5](https://gitee.com/openharmony/useriam_faceauth/issues/I4Q8F5)  | [用户IAM子系统]【新增特性】提供Standard人脸录入控件     | 用户IAM子系统 | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 56  | [I4Q8F3](https://gitee.com/openharmony/useriam_faceauth/issues/I4Q8F3)  | [用户IAM子系统]【新增特性】提供Standard统一身份认证JS接口     | 用户IAM子系统 | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 57  | [I4LKQ0](https://gitee.com/openharmony/kernel_linux_5.10/issues/I4LKQ0)  | [内核子系统]【新增特性】cpuset与cpu热插拔解耦     | 内核子系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 58  | [I4HAMI](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4HAMI)  | 【data_share_ability】支持跨应用订阅数据库的变化    | 标准系统 | SIG_DataManagement | [@verystone](https://gitee.com/verystone)      |
| 59   | [I4Q6AS](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AS)|【新增特性】FreezeDetector|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 60   | [I4Q6AV](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AV)|【新增特性】在Openharmony上hiview插件管理平台代理加载特性|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 61   | [I4Q6AT](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AT)|【新增特性】在Openharmony上FaultLogger添加js api|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 62   | [I4Q6B3](https://gitee.com/openharmony/hiviewdfx_hilog/issues/I4Q6B3)|【新增特性】支持内核日志的读取和落盘|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 63   | [I4Q6B1](https://gitee.com/openharmony/hiviewdfx_hilog/issues/I4Q6B1)|【增强特性】将hilogd NDK库加入到OpenHarmony NDK包|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 64   | [I4Q8ZL](https://gitee.com/openharmony/hiviewdfx_faultloggerd/issues/I4Q8ZL)|【增强特性】日志管理|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 65   | [I4Q8ZK](https://gitee.com/openharmony/hiviewdfx_faultloggerd/issues/I4Q8ZK)|【增强特性】进程异常信号处理|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 66   | [I4Q8ZJ](https://gitee.com/openharmony/hiviewdfx_faultloggerd/issues/I4Q8ZJ)|【增强特性】抓取调用栈工具|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 67   | [I4Q8ZI](https://gitee.com/openharmony/hiviewdfx_faultloggerd/issues/I4Q8ZI)|【新增特性】抓取调用栈基础库|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 68    | [I4QEKH](https://gitee.com/openharmony/drivers_framework/issues/I4QEKH) | 【新增特性】提供共享内存相关HDI能力                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 69    | [I4QEKI](https://gitee.com/openharmony/drivers_framework/issues/I4QEKI) | 【新增特性】驱动开发工具支持标准系统驱动开发                           | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 70    | [I4QEKJ](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKJ) |【新增特性】HDI接口适配linux-input驱动                           | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 71    | [I4QEKM](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKM) | 【新增特性】提供power HDI接口能力                           | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 72    | [I4QEKK](https://gitee.com/openharmony/drivers_framework/issues/I4QEKK) | 【新增特性】基于HDF驱动框架提供硬件TIMER驱动                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 73    | [I4QEKL](https://gitee.com/openharmony/drivers_framework/issues/I4QEKL) | 【新增特性】基于HDF驱动框架构建统一的平台驱动对象模型                           | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 74    | [I4QEKN](https://gitee.com/openharmony/usb_manager/issues/I4QEKN) | 【新增特性】USB Device功能实现                           | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 75    | [I4QEKO](https://gitee.com/openharmony/usb_manager/issues/I4QEKO) | 【新增特性】USB Host功能实现                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 76   | [I4QEPQ](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4QEPQ)|【资料】【RDB】支持QuerySql返回结果集，进一步支持更广泛的查询方式|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 77   | [I4NZP6](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4NZP6)|【RDB】增加多表查询能力|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 78   | [I4FZ6B](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4FZ6B)|【RDB】提供事务能力|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 79   | [I4HAMI](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4HAMI)|【data_share_ability】支持跨应用订阅数据库的变化|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 80   | [I4QC4U](https://gitee.com/openharmony/ace_ace_engine/issues/I4QC4U)|【新增特性】PC预览资源管理特性对接全球化规格|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 81   | [I4QC4S](https://gitee.com/openharmony/ace_ace_engine/issues/I4QC4S)|【新增规格】文本计时器组件支持|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 82   | [I4QC4R](https://gitee.com/openharmony/ace_ace_engine/issues/I4QC4R)|【新增规格】进度条组件能力增强|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 83   | [I4QC4P](https://gitee.com/openharmony/ace_ace_engine/issues/I4QC4P)|【新增规格】文字时钟组件支持|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 84   | [I4QC4N](https://gitee.com/openharmony/ace_ace_engine/issues/I4QC4N)|【新增规格】TextInput组件能力增强|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 85   | [I4QC4O](https://gitee.com/openharmony/ace_ace_engine/issues/I4QC4O)|【新增规格】Select组件支持|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 86   | [I4QC4K](https://gitee.com/openharmony/ace_ace_engine/issues/I4QC4K)|【新增规格】TextArea组件能力增强|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 87    | [I4QGHF](https://gitee.com/openharmony/powermgr_power_manager/issues/I4QGHF) | 【部件化专项】power_manage部件标准化 | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 88   |[I4QKLO](https://gitee.com/openharmony/applications_contacts/issues/I4QKLO) |【联系人】联系人列表 - 列表元素显示（姓名、工作单位、头像等）| 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 89   |[I4QKLM](https://gitee.com/openharmony/applications_contacts/issues/I4QKLM) |【联系人】联系人列表 - 新建/编辑联系人 | 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 90   |[I4IEJW](https://gitee.com/openharmony/communication_dsoftbus/issues/I4IEJW) |【新增特性】【RPC】RPC支持本设备收发及死亡通知 | 软总线子系统    | SIG_SoftBus   |[@liubb940516](https://gitee.com/liubb940516)|
| 91   |[I4IEJE](https://gitee.com/openharmony/communication_dsoftbus/issues/I4IEJE) |【新增特性】【RPC】RPC支持统一管理框架 | 软总线子系统    | SIG_SoftBus   |[@liubb940516](https://gitee.com/liubb940516)|
| 92   |[I4PXNS](https://gitee.com/openharmony/build/issues/I4PXNS) |【新增特性】 提供NDK的编译模板  | 标准系统 | SIG_CompileRuntime | [@anguanglin](https://gitee.com/anguanglin)
| 93   |[I4PW9P](https://gitee.com/openharmony/build/issues/I4PW9P) |【新增特性】 提供图形配置界面，通过界面操作选择部件列表  | 标准系统 | SIG_CompileRuntime | [@anguanglin](https://gitee.com/anguanglin)
| 94   |[I4PWA3](https://gitee.com/openharmony/build/issues/I4PWA3) |【新增特性】支持利用Kconfig输出生成支持编译产品配置   | 标准系统 | SIG_CompileRuntime | [@anguanglin](https://gitee.com/anguanglin)
| 95   |[I4PWAB](https://gitee.com/openharmony/build/issues/I4PWAB) |【新增特性】提供Kconfig的使用指导   | 标准系统 | SIG_CompileRuntime | [@anguanglin](https://gitee.com/anguanglin)
| 96 | [I4WTQM](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTQM) | 【新增特性】支持三方应用发起分享页面呈现                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 97 | [I4WTPK](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTPK) | 【新增规格】DatePicker组件能力增强                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 98 | [I4WTPO](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTPO) | 【新增规格】TextPicker组件能力增强                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 99 | [I4WTPY](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTPY) | 【新增规格】文本组件支持鼠标拖拽选择文字规格                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 100 | [I4WTR6](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTR6) | 【新增特性】XComponent组件特性支持声明式范式规格                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 101 | [I4WTR7](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTR7) | 【新增特性】XComponent组件特性支持获取Surface规格                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 102 | [I4WTRA](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTRA) | 【新增特性】鼠标按键、滚轮事件支持                          | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 103 | [I4WTQI](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTQI) | 【新增规格】拖拽能力增加鼠标拖拽规格                          | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 104 | [I4WWRX](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWRX) | 【多模】带键盘类按键状态的鼠标事件 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 105 | [I4WWRY](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWRY) | 【多模】鼠标连续滚动事件增加开始和结束 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 106 | [I4WWSY](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWSY) | 【多模】触摸板水平轴&垂直轴同时滑动 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 107 | [I4WWT3](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWT3) | 【多模】触摸板输入事件监听功能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 108 | [I4WWT4](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWT4) | 【多模】触摸板输入事件拦截功能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 109 | [I4WWS4](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWS4) | 【多模】鼠标图标显示 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
## OpenHarmony 3.1.3.3版本特性清单：

| no   | issue                                                        | feture description                                           | platform       | sig                      | owner                                                 |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :------------- | :----------------------- | :---------------------------------------------------- |
| 1    | [I4GYCD](https://gitee.com/openharmony/powermgr_battery_statistics/issues/I4GYCD) | 【新增特性】支持软件耗电统计                                 | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 2    | [I4GYCN](https://gitee.com/openharmony/powermgr_battery_statistics/issues/I4GYCN) | 【新增特性】支持硬件耗电统计                                 | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 3    | [I4GYDQ](https://gitee.com/openharmony/powermgr_battery_statistics/issues/I4GYDQ) | 【新增特性】支持耗电详情记录                                 | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 4    | [I4GY9U](https://gitee.com/openharmony/powermgr_thermal_manager/issues/I4GY9U) | 【新增特性】支持内核温控服务                                 | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 5    | [I4GYAF](https://gitee.com/openharmony/powermgr_thermal_manager/issues/I4GYAF) | 【新增特性】支持用户层和服务温控服务                         | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 6    | [I4P7FB](https://gitee.com/openharmony/third_party_musl/issues/I4P7FB) | [语言编译运行时子系统]提供NDK中HOS与OHOS两种target，ABI的clang/llvm编译工具链 提供NDK中调试工具链，支持lldb，asan等功能 | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 7    | [I4P7F2](https://gitee.com/openharmony/third_party_musl/issues/I4P7F2) | [语言编译运行时子系统]支持基于lldb的断点调试                 | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 8    | [I4P7F3](https://gitee.com/openharmony/third_party_musl/issues/I4P7F3) | [语言编译运行时子系统]支持C/C++应用调试栈/变量的查看能力     | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 9    | [I4P7EQ](https://gitee.com/openharmony/ark_ts2abc/issues/I4P7EQ) | [语言编译运行时子系统]ts类型信息提取                         | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 10   | [I4P7ER](https://gitee.com/openharmony/ark_ts2abc/issues/I4P7ER) | [语言编译运行时子系统]Ts2abc中的类型信息增强                 | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 11   | [I4P7ET](https://gitee.com/openharmony/ark_ts2abc/issues/I4P7ET) | [语言编译运行时子系统]Panda file中的类型系统存储             | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 12   | [I4P7EU](https://gitee.com/openharmony/ark_ts2abc/issues/I4P7EU) | [语言编译运行时子系统]abc支持列号信息                        | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 13   | [I4P7FY](https://gitee.com/openharmony/js_util_module/issues/I4P7FY) | [语言编译运行时子系统]container特性/LightWeightMap接口规格   | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 14   | [I4P7FX](https://gitee.com/openharmony/js_util_module/issues/I4P7FX) | [语言编译运行时子系统]container特性/Deque接口规格            | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 15   | [I4P7FV](https://gitee.com/openharmony/js_util_module/issues/I4P7FV) | [语言编译运行时子系统]container特性/HashSet接口规格          | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 16   | [I4P7FT](https://gitee.com/openharmony/js_util_module/issues/I4P7FT) | [语言编译运行时子系统]container特性/TreeSet接口规格          | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 17   | [I4P7FR](https://gitee.com/openharmony/js_util_module/issues/I4P7FR) | [语言编译运行时子系统]container特性/TreeMap接口规格          | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 18   | [I4P7FP](https://gitee.com/openharmony/js_util_module/issues/I4P7FP) | [语言编译运行时子系统]container特性/Queue接口规格            | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 19   | [I4P7FO](https://gitee.com/openharmony/js_util_module/issues/I4P7FO) | [语言编译运行时子系统]container特性/Vector接口规格           | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 20   | [I4P7FM](https://gitee.com/openharmony/js_util_module/issues/I4P7FM) | [语言编译运行时子系统]container特性/PlainArray接口规格       | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 21   | [I4P7FL](https://gitee.com/openharmony/js_util_module/issues/I4P7FL) | [语言编译运行时子系统]container特性/ArrayList接口规格        | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 22   | [I4P7FK](https://gitee.com/openharmony/js_util_module/issues/I4P7FK) | [语言编译运行时子系统]container特性/LightWeightSet接口规格   | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 23   | [I4P7FH](https://gitee.com/openharmony/js_util_module/issues/I4P7FH) | [语言编译运行时子系统]container特性/HashMap接口规格          | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 24   | [I4P7FF](https://gitee.com/openharmony/js_util_module/issues/I4P7FF) | [语言编译运行时子系统]container特性/List接口规格             | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 25   | [I4P7FE](https://gitee.com/openharmony/js_util_module/issues/I4P7FE) | [语言编译运行时子系统]container特性/Stack接口规格            | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 26   | [I4OGCO](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCO?from=project-issue) | 【新增特性】【DMS】提供跨设备迁移接口                        | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 27   | [I4OGCL](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCL?from=project-issue) | 【增强特性】【框架】迁移数据保存                             | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 28   | [I4OH9B](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4OH9B?from=project-issue) | 【samgr】动态加载未启动的本地系统服务                        | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 29   | [I4OH9A](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4OH9A?from=project-issue) | 【samgr】系统服务启动性能跟踪                                | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 30   | [I4OH98](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4OH98?from=project-issue) | 【samgr】SAMGR异常恢复                                       | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 31   | [I4OH94](https://gitee.com/openharmony/device_profile_core/issues/I4OH94?from=project-issue) | 【device_profile】校验DP客户端访问profile记录的权限          | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 32   | [I4OGD6](https://gitee.com/openharmony/device_profile_core/issues/I4OGD6?from=project-issue) | 【部件化专项】分布式DeviceProfile子系统部件标准化            | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 33   | [I4PBJF](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBJF) | 【部件化专项】distributed_notification_service部件标准化     | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 34   | [I4PBNF](https://gitee.com/openharmony/notification_ces_standard/issues/I4PBNF) | 【部件化专项】common_event部件标准化                         | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 35   | [I4PBO1](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBO1) | 【资料】通知能力开发者材料                                   | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 36   | [I4PBPM](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBPM) | 【增强特性】分布式通知支持流控                               | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 37   | [I4PBRM](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBRM) | 【新增特性】支持其他设备的通知点击后在本设备跳转             | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 38   | [I4PBRW](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBRW) | 【新增特性】支持设备级的分布式通知使能控制                   | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 39   | [I4PBSE](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBSE) | 【新增特性】支持通知管理应用设置和查询应用级的分布式通知使能 | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 40   | [I4PBSP](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBSP) | 【新增特性】支持应用设置分布式通知能力是否使能               | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 41   | [I4PBT7](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBT7) | 【新增特性】分布式通知同步                                   | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 42   | [I4PBU3](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBU3) | 【新增特性】分布式通知联动取消                               | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 43   | [I4PBUU](https://gitee.com/openharmony/notification_ces_standard/issues/I4PBUU) | 【新增规格】 支持通过config.json静态配置公共事件，支持通过wokscheduler静态拉起订阅者 | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 44   | [I4PBV9](https://gitee.com/openharmony/notification_ces_standard/issues/I4PBV9) | 【新增规格】 支持静态订阅者管控                              | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 45   | [I4PCH4](https://gitee.com/openharmony/aafwk_standard/issues/I4PCH4) | 【新增特性】卡片支持多用户                                   | 元能力子系统   | SIG_ApplicationFramework | [@lsq1474521181](https://gitee.com/lsq1474521181)     |
| 46   | [I4PCHC](https://gitee.com/openharmony/aafwk_standard/issues/I4PCHC) | 【元能力-运行管理部件化】基于SysCap的平台代码解耦和部件化改造 | 元能力子系统   | SIG_ApplicationFramework | [@lsq1474521181](https://gitee.com/lsq1474521181)     |
| 47   | [I4PCHF](https://gitee.com/openharmony/aafwk_standard/issues/I4PCHF) | 【元能力-卡片管理部件化】基于SysCap的平台代码解耦和部件化改造 | 元能力子系统   | SIG_ApplicationFramework | [@lsq1474521181](https://gitee.com/lsq1474521181)     |
| 48   | [I4PPVU](https://gitee.com/openharmony/aafwk_standard/issues/I4PPVU) | 【新增特性】进程多实例                                       | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)                 |
| 49   | [I4PPW2](https://gitee.com/openharmony/aafwk_standard/issues/I4PPW2) | 【资料】提供测试框架新增/增强特性资料说明                    | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)                 |
| 50   | [I4PCLO](https://gitee.com/openharmony/aafwk_standard/issues/I4PCLO) | 【增强特性】Ability多实例                                    | 元能力子系统   | SIG_ApplicationFramework | [@silent-dye](https://gitee.com/silent-dye)           |
| 51   | [I4PCLR](https://gitee.com/openharmony/aafwk_standard/issues/I4PCLR) | 【新增特性】动态加载多Hap包                                  | 元能力子系统   | SIG_ApplicationFramework | [@silent-dye](https://gitee.com/silent-dye)           |
| 52   | [I4PCM1](https://gitee.com/openharmony/aafwk_standard/issues/I4PCM1) | 【新增特性】提供ce/de级上下文                                | 元能力子系统   | SIG_ApplicationFramework | [@silent-dye](https://gitee.com/silent-dye)           |
| 53   | [I4PCOQ](https://gitee.com/openharmony/aafwk_standard/issues/I4PCOQ) | 【新增特性】应用管理                                         | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 54   | [I4PCS2](https://gitee.com/openharmony/aafwk_standard/issues/I4PCS2) | 【资料】提供测试工具新增/增强特性资料说明                    | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 55   | [I4PCVN](https://gitee.com/openharmony/aafwk_standard/issues/I4PCVN) | 【新增特性】支持任务快照获取和更新                           | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)                 |
| 56   | [I4PCWF](https://gitee.com/openharmony/aafwk_standard/issues/I4PCWF) | 【资料】提供服务组件新增/增强特性资料说明                    | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)                 |
| 57   | [I4PPW6](https://gitee.com/openharmony/aafwk_standard/issues/I4PPW6) | 【增强特性】指定窗口模式启动组件                             | 元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 58   | [I4PPWA](https://gitee.com/openharmony/aafwk_standard/issues/I4PPWA) | 【增强特性】Ability框架适配配置文件变更                      | 元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 59   | [I4PPWD](https://gitee.com/openharmony/aafwk_standard/issues/I4PPWD) | 【增强特性】Extension框架适配配置文件变更                    | 元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 60   | [I4PPWI](https://gitee.com/openharmony/aafwk_standard/issues/I4PPWI) | 【新增特性】快速设置扩展                                     | 元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 61   | [I4PKY3](https://gitee.com/openharmony/aafwk_standard/issues/I4PKY3) | 【部件化专项】bundle_manager部件标准化                       | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 62   | [I4PKYB](https://gitee.com/openharmony/aafwk_standard/issues/I4PKYB) | 【增强特性】schema适配配置文件重构                           | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 63   | [I4PKYD](https://gitee.com/openharmony/aafwk_standard/issues/I4PKYD) | 【新增特性】安装能力适配config.json调整                      | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 64   | [I4PKYF](https://gitee.com/openharmony/aafwk_standard/issues/I4PKYF) | 【新增特性】支持查询指定Metadata资源profile配置文件的信息    | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 65   | [I4PKYH](https://gitee.com/openharmony/aafwk_standard/issues/I4PKYH) | 【新增特性】支持对Extension的查询                            | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 66   | [I4PKYI](https://gitee.com/openharmony/aafwk_standard/issues/I4PKYI) | 【新增特性】提供清除数据的能力                               | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 67   | [I4PKYR](https://gitee.com/openharmony/aafwk_standard/issues/I4PKYR) | 【新增特性】系统定义权限的初始化                             | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 68  | [I4PKYU](https://gitee.com/openharmony/aafwk_standard/issues/I4PKYU) | 【新增特性】支持对应用权限信息的查询                         | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 69  | [I4PXN0](https://gitee.com/openharmony/build/issues/I4PXN0)  | [编译构建子系统]【增强特性】编译构建日志优化，日志按级别显示               | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)       |
| 70  | [I4PXND](https://gitee.com/openharmony/build/issues/I4PXND)  | [编译构建子系统]【增强特性】hb命令安装、集成及扩展支持               | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)       |
| 71 | [I4PKY7](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKY7) | 【新增特性】跨设备信息同步 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 72 | [I4PKY8](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKY8) | 【新增特性】跨设备信息查询 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 73 | [I4PKYE](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYE) | 【新增特性】支持查询禁用的组件信息和应用信息 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 74 | [I4QA2V](https://gitee.com/openharmony/appexecfwk_standard/issues/I4QA2V) | 【部件化专项】bundle_tool部件标准化 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 79  | [I4Q9RC](https://gitee.com/openharmony/kernel_liteos_m/issues/I4Q9RC)  | [内核子系统]【增强特性】支持指令集融合    | 内核子系统 | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5) |
| 80  | [I4PZE7](https://gitee.com/openharmony/device_manager/issues/I4PZE7)  | 【增强特性】支持周边不可信设备的发现    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116)   |
| 81  | [I4PZE4](https://gitee.com/openharmony/device_manager/issues/I4PZE4)  | 【增强特性】支持JSAPI接口    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116)  |
| 82  | [I4PZE2](https://gitee.com/openharmony/device_manager/issues/I4PZE2)  | 【新增特性】支持多用户切换    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116) |
| 83  | [I4PZDZ](https://gitee.com/openharmony/device_manager/issues/I4PZDZ)  | 【增强特性】支持账号无关设备的PIN码认证    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116) |
| 84  | [I4PZDY](https://gitee.com/openharmony/device_manager/issues/I4PZDY)  | 【增强特性】支持可信设备列表查询    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116) |
| 85  | [I4PZDT](https://gitee.com/openharmony/device_manager/issues/I4PZDT)  | 【增强特性】支持可信设备的上下线监听    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116) |
| 86  | [I4PZDR](https://gitee.com/openharmony/device_manager/issues/I4PZDR)  | 【增强特性】分布式设备管理部件资料    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116) |
| 87  | [I4PZC9](https://gitee.com/openharmony/device_manager/issues/I4PZC9)  | 【新增特性】支持分布式设备管理接口授权控制    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116) |
| 88  | [I4PZC7](https://gitee.com/openharmony/device_manager/issues/I4PZC7)  | 【新增特性】支持设备被发现开关控制    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116)   |
| 89  | [I4QESH](https://gitee.com/openharmony/device_manager/issues/I4QESH)  | 【新增特性】设备Id的查询和转换    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116)|
| 90    | [I4QEKP](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKP) | 【新增特性】基于HDF驱动框架提供light驱动能力                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 91    | [I4QEKR](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKR) | 【新增特性】Display-Layer、Display-Gralloc、Display-Gfx针对轻量系统的增强参考实现                          | 轻量系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 92    | [I4QEKV](https://gitee.com/openharmony/usb_manager/issues/I4QEKV) | 【新增特性】USB服务 HDI接口实现                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 93    | [I4OEOZ](https://gitee.com/openharmony/powermgr_power_manager/issues/I4OEOZ) | 【新增特性】监控输入亮屏输入事件，并根据输入事件进行亮、灭屏   | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 94    | [I4MBRM](https://gitee.com/openharmony/powermgr_power_manager/issues/I4MBRM) | 【新增特性】支持接近光控制锁，通话时通过接近光控制亮灭屏的特性   | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 95    | [I4MBRL](https://gitee.com/openharmony/powermgr_power_manager/issues/I4MBRL) | 【新增特性】支持显示相关的能耗调节    | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 96    | [I4QGI0](https://gitee.com/openharmony/powermgr_power_manager/issues/I4QGI0) | 【新增特性】长按power Key弹出关机界面    | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 97    | [I4QGJH](https://gitee.com/openharmony/powermgr_thermal_manager/issues/I4QGJH) | 【部件化专项】thermal_manager部件标准化    | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 98    | [I4QGLI](https://gitee.com/openharmony/powermgr_battery_statistics/issues/I4QGLI) | 【部件化专项】battery_statistics部件标准化    | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 99    | [I4QT3R](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QT3R) | 【部件化专项】全局资源调度管控子系统部件标准化 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 100    | [I4QT3Y](https://gitee.com/openharmony/notification_ans_standard/issues/I4QT3Y) | 【新增特性】支持系统进程统一代理三方提醒 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 101    | [I4QT40](https://gitee.com/openharmony/notification_ans_standard/issues/I4QT40) | 【新增特性】提醒后台代理计时能力 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 102    | [I4QT41](https://gitee.com/openharmony/notification_ans_standard/issues/I4QT41) | 【新增特性】提醒代理管理 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 103    | [I4QT42](https://gitee.com/openharmony/notification_ans_standard/issues/I4QT42) | 【新增特性】提醒代理相关资料文档 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 104    | [I4QT43](https://gitee.com/openharmony/resourceschedule_resource_schedule_service/issues/I4QT43) | 【新增特性】全局资源调度框架 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 105    | [I4QU0N](https://gitee.com/openharmony/resourceschedule_resource_schedule_service/issues/I4QU0N) | 【新增特性】支持Soc调频 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 106    | [I4QU0V](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QU0V) | 【新增特性】支持短时任务申请/注销/查询 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 107    | [I4QU0W](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QU0W) | 【新增特性】短时任务后台管理 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 108    | [I4QU0X](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QU0X) | 【新增特性】短时任务可维可测 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 109    | [I4QU0Z](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QU0Z) | 【新增特性】短时任务相关资料文档 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 110    | [I4R2MH](https://gitee.com/openharmony/global_i18n_standard/issues/I4R2MH) | 【新增特性】时区数据更新                         | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 111   | [I4R2M3](https://gitee.com/openharmony/global_i18n_standard/issues/I4R2M3) | 【新增特性】时区数据部署                         | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 112   | [I4R2C2](https://gitee.com/openharmony/global_i18n_standard/issues/I4R2C2) | 【新增特性】多偏好语言                         | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 113   | [I4R2YF](https://gitee.com/openharmony/global_resmgr_standard/issues/I4R2YF) | 【增强特性】ResourceManager适配hap包结构和配置清单文件调整                                     | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 114   | [I4R3DO](https://gitee.com/openharmony/global_resmgr_standard/issues/I4R3DO) | 【增强特性】restool工具适配配置清单文件调整                                   | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 115   | [I4OWTZ](https://gitee.com/openharmony/kernel_linux_5.10/issues/I4OWTZ)  | [内核子系统]【外部依赖】内核实现进程的tokenID设置     | 内核子系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 116   | [I4QE9K](https://gitee.com/openharmony/utils/issues/I4QE9K)  | [内核子系统]【新增特性】提供内核态驱动与用户态之间、用户态与用户态之间的内核共享能力     | 内核子系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 117   | [I4QM8F](https://gitee.com/openharmony/kernel_linux_build/issues/I4QM8F)  | [内核子系统]【部件化专项】Linux内核部件标准化     | 内核子系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 118   | [I4Q79P](https://gitee.com/openharmony/communication_ipc/issues/I4Q79P)  | 【新增特性】【RPC】RPC支持跨设备收发及死亡通知 | 软总线子系统 | SIG_SoftBus | [@pilipala195](https://gitee.com/pilipala195)       |
| 119   | [I4Q79C](https://gitee.com/openharmony/communication_ipc/issues/I4Q79C)  | 【新增特性】【RPC】RPC支持跨设备服务管理 | 软总线子系统 | SIG_SoftBus | [@pilipala195](https://gitee.com/pilipala195)       |
| 120   | [I4IIRC](https://gitee.com/openharmony/communication_ipc/issues/I4IIRC)  | 【新增特性】【RPC】IPC实现tokenid的传递和查询 | 软总线子系统 | SIG_SoftBus | [@Xi_Yuhao](https://gitee.com/Xi_Yuhao)       |
| 121   | [I4RCE2](https://gitee.com/openharmony/security_selinux/issues/I4RCE2)  | 【部件化专项】【selinux部件】部件标准化     | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 122   | [I4RCDU](https://gitee.com/openharmony/security_selinux/issues/I4RCDU)  | 【新增规格】支持只读镜像的文件的标签设置     | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 123   | [I4RCDS](https://gitee.com/openharmony/security_selinux/issues/I4RCDS)  | 【新增规格】支持native进程标签设置     | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 124   | [I4RCD9](https://gitee.com/openharmony/security_selinux/issues/I4RCD9)  | 【新增规格】支持SELinux虚拟文件标签设置     | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 125   | [I4RCD6](https://gitee.com/openharmony/security_selinux/issues/I4RCD6)  | 【新增规格】支持SELinux文件标签设置     | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 126   | [I4RCD0](https://gitee.com/openharmony/security_selinux/issues/I4RCD0)  | 【新增特性】支持SELinux策略加载和使能     | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 127   | [I4RCBT](https://gitee.com/openharmony/security_selinux/issues/I4RCBT)  | 【新增规格】提供hap应用selinux domain设置接口库    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 128   | [I4RCB7](https://gitee.com/openharmony/security_selinux/issues/I4RCB7)  | 【新增规格】提供hap应用数据目录的selinux标签设置接口库    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 129   | [I4RCAA](https://gitee.com/openharmony/security_selinux/issues/I4RCAA)  | 【新增特性】实现文件系统二级目录的selinux标签设置    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 130   | [I4JBEK](https://gitee.com/openharmony/account_os_account/issues/I4JBEK)  | [帐号子系统]支持分布式组网账号ID的派生    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 131   | [I4JBFB](https://gitee.com/openharmony/account_os_account/issues/I4JBFB)  | [账号子系统]支持分布式组网账号状态管理    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 132   | [I4IU33](https://gitee.com/openharmony/account_os_account/issues/I4IU33)  | [帐号子系统]支持本地多用户功能设置与内容修改    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 133   | [I4JBFI](https://gitee.com/openharmony/account_os_account/issues/I4JBFI)  | [账号子系统]支持本地多用户分布式信息查询    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 134   | [I4IU3V](https://gitee.com/openharmony/account_os_account/issues/I4IU3V)  | [帐号子系统]支持域账户和本地用户关联    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 135   | [I4IU6A](https://gitee.com/openharmony/account_os_account/issues/I4IU6A)  | [帐号子系统]支持本地用户约束条件配置    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 136   | [I4IU6N](https://gitee.com/openharmony/account_os_account/issues/I4IU6N)  | [帐号子系统]支持本地多用户基础信息管理    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 137   | [I4IU74](https://gitee.com/openharmony/account_os_account/issues/I4IU74)  | [帐号子系统]支持本地用户的创建和删除    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 138   | [I4RCGG](https://gitee.com/openharmony/account_os_account/issues/I4RCGG)  | [帐号子系统]支持用户信息查询    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 139   | [I4QJVZ](https://gitee.com/openharmony/security_huks/issues/I4QJVZ)  | 【新增特性】HUKS CORE 算法能力模块Ability化    | 安全子系统 | SIG_Security | [@chaos-liang](https://gitee.com/Chaos-Liang)       |
| 140    | [I4RCRT](https://gitee.com/openharmony/usb_manager/issues/I4RCRT) | [驱动子系统]SR000GNFHL：【新增特性】USB服务 部件标准化                          | 标准系统 | SIG_Driver         | [@wu-chengwen](https://gitee.com/wu-chengwen)           |
| 141   | [I4RBA4](https://gitee.com/openharmony-sig/developtools_hapsigner/issues/I4RBA4)  | [新增特性]PKI应用签名工具支持生成签名密钥 | 安全子系统       | SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)      |
| 142   | [I4RBEN](https://gitee.com/openharmony-sig/developtools_hapsigner/issues/I4RBEN)  | [新增特性]PKI应用签名工具支持生成证书签名请求（CSR）| 安全子系统      | SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)      |
| 143   | [I4RBEA](https://gitee.com/openharmony-sig/developtools_hapsigner/issues/I4RBEA)  | [新增特性]PKI应用签名工具支持生成CA密钥和证书 | 安全子系统      | SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)       |
| 144   | [I4RFJP](https://gitee.com/openharmony-sig/developtools_hapsigner/issues/I4RFJP)  | [部件化专项]【PKISignCentre部件】PKISignCentre部件标准化 | 安全子系统      | SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)       |
| 145   | [I4PNX7](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4PNX7)|【分布式RDB】数据存储需求|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 146   | [I4R6T4](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4R6T4)|【部件化专项】【native_appdatamgr部件】native_appdatamgr部件标准化|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 147   | [I4RFYC](https://gitee.com/openharmony/distributeddatamgr_objectstore/issues/I4RFYC)|【部件化专项】【objectstore部件】分布式数据对象部件标准化|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 148   | [I4H3LS](https://gitee.com/openharmony/distributeddatamgr_objectstore/issues/I4H3LS)|分布式数据对象提供JS接口|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 149   | [I4NUD5](https://gitee.com/openharmony/ark_js_runtime/issues/I4NUD5)|方舟C++ FFI支持继承关系|语言编译运行时子系统|SIG_CompileRuntime|[@weng-changcheng](https://gitee.com/weng-changcheng)|
| 150   | [I4P86T](https://gitee.com/openharmony/js_worker_module/issues/I4P86T)|支持Worker中可以再创建Worker，子Worker可以跟父Worker通信|语言编译运行时子系统|SIG_CompileRuntime|[@weng-changcheng](https://gitee.com/weng-changcheng)|
| 151   | [I4P7FN](https://gitee.com/openharmony/js_util_module/issues/I4P7FN)|【新增规格】container特性/LinkedList接口规格|语言编译运行时子系统|SIG_CompileRuntime|[@gongjunsong](https://gitee.com/gongjunsong)|
| 152   | [I4RG4R](https://gitee.com/openharmony/useriam_user_idm/issues/I4RG4R)  | 【DFX】用户IAM框架DFX需求 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 153   | [I4RG4X](https://gitee.com/openharmony/useriam_user_idm/issues/I4RG4X)  | 【user_idm】支持用户本地人脸的删除 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 154   | [I4RG55](https://gitee.com/openharmony/useriam_user_idm/issues/I4RG55)  | 【user_idm】支持用户本地认证凭据信息查询 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 155   | [I4RG59](https://gitee.com/openharmony/useriam_user_idm/issues/I4RG59)  | 【user_idm】支持用户本地口令的录入 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 156   | [I4RG5G](https://gitee.com/openharmony/useriam_user_idm/issues/I4RG5G)  | 【user_idm】支持用户本地口令的删除 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 157   | [I4RG5M](https://gitee.com/openharmony/useriam_user_idm/issues/I4RG5M)  | 【user_idm】支持用户本地人脸的录入 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 158   | [I4RG5R](https://gitee.com/openharmony/useriam_user_idm/issues/I4RG5R)  | 【user_idm】支持删除用户时，删除该用户的身份认证凭据 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 159   | [I4RGMX](https://gitee.com/openharmony/useriam_user_idm/issues/I4RGMX)  | 【部件化专项】【user_idm部件】部件标准化 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 160   | [I4RGMD](https://gitee.com/openharmony/useriam_faceauth/issues/I4RGMD)  | 【部件化专项】【face_auth部件】部件标准化 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 161   | [I4RGND](https://gitee.com/openharmony/useriam_user_auth/issues/I4RGND)  | 【部件化专项】【user_auth部件】部件标准化 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 162   | [I4RG8D](https://gitee.com/openharmony/useriam_user_auth/issues/I4RG8D)  | 【user_auth】支持用户本地口令认证 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 163   | [I4RG7W](https://gitee.com/openharmony/useriam_user_auth/issues/I4RG7W)  | 【user_auth】支持用户本地人脸认证 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 164   | [I4RGNO](https://gitee.com/openharmony/useriam_pin_auth/issues/I4RGNO)  | 【部件化专项】【pin_auth部件】部件标准化 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 165   | [I4RG9E](https://gitee.com/openharmony/useriam_pin_auth/issues/I4RG9E)  | 【DFX】口令认证框架DFX需求 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 166   | [I4RG91](https://gitee.com/openharmony/useriam_pin_auth/issues/I4RG91)  | 【pin_auth】支持用户本地口令认证 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 167   | [I4RG8W](https://gitee.com/openharmony/useriam_pin_auth/issues/I4RG8W)  | 【pin_auth】支持用户本地口令录入 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 168   | [I4RGO7](https://gitee.com/openharmony/useriam_auth_executor_mgr/issues/I4RGO7)  | 【部件化专项】【auth_executor_mgr部件】部件标准化| 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 169   | [I4RGU3](https://gitee.com/openharmony/useriam_pin_auth/issues/I4RGU3)  | 【pin_auth】提供软实现| 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 170   | [I4RGUM](https://gitee.com/openharmony/useriam_user_idm/issues/I4RGUM)  | 【useriam】提供软实现| 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 171   | [I4RGWU](https://gitee.com/openharmony/useriam_pin_auth/issues/I4RGWU)  | 【pin_auth】支持用户本地口令删除| 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 172   | [I4RCRM](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRM)|【IDE工具支持】交互事件回调耗时打印|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 173   | [I4RCRL](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRL)|【IDE工具支持】渲染流水线耗时打印|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 174   | [I4RCRK](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRK)|【DFX】ACE框架超时检测机制|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 175   | [I4RCRI](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRI)|【新增规格】卡片支持鼠标悬停事件|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 176   | [I4RCRH](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRH)|【新增特性】自定义builder|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 177   | [I4RCRG](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRG)|【新增特性】$$双向绑定编译转换支持|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)||
| 178   | [I4RCRF](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRF)|【新增特性】新增自定义组件支持访问子组件数据|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 179   | [I4RCRE](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRE)|【新增特性】新增NAPI继承机制|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 180   | [I4RCRD](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRD)|【新增规格】新增OffscreenCanvas支持抗锯齿特性|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 181   | [I4RCRC](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRC)|【新增特性】样式状态编译转换支持|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 182   | [I4RCRA](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRA)|【新增特性】ArkUI对接窗口新架构|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 183   | [I4Q8ZH](https://gitee.com/openharmony/hiviewdfx_faultloggerd/issues/I4Q8ZH)|【跟踪】【hiappevent部件】应用事件功能增强|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 184  | [I4RCR0](https://gitee.com/openharmony/hiviewdfx_hilog/issues/I4RCR0)|【跟踪】【hilog部件】流水日志功能增强|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 185   | [I4Q6AS](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AS)|【资料】faultloggerd部件 南北向文档需求|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 186 | [I4NJTS](https://gitee.com/openharmony/ace_engine_lite/issues/I4NJTS) |[轻量级图形子系统]支持通用touch事件、list组件支持scrollbottom/scrolltop事件|轻量系统|SIG_AppFramework|[@piggyguy](https://gitee.com/piggyguy)|
| 187 | [I4NJTD](https://gitee.com/openharmony/graphic_ui/issues/I4NJTD) |[轻量级图形子系统]list组件支持scrollbottom/scrolltop事件|轻量系统|SIG_AppFramework|[@piggyguy](https://gitee.com/piggyguy)|
| 188   | [I4RFBD](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4RFBD)|【新增特性】【local_file_system】支持fat/exfat/ntfs等可插拔文件系统能力|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 189   | [I4RDNG](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4RDNG)|【新增特性】【local_file_system】支持ext4/f2fs等用户态工具的能力|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 190  | [I4RE2G](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4RE2G)|【新增特性】【local_file_system】支持ext4/f2fs格式镜像打包能力|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 191   | [I4RENG](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4RENG)|【新增特性】【local_file_system】支持ext4/f2fs文件系统开机resize和fsck|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 192   | [I4RF6Z](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4RF6Z)|【新增特性】【local_file_system】支持fat/exfat/ntfs等可插拔文件系统能力|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 193   | [I4RFEQ](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4RFEQ)|【新增特性】【storage_service部件】支持密钥存储管理|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 194   | [I4RG9F](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4RG9F)|【新增特性】【storage_service】CE/DE文件软加密策略管理|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 195   | [I4PW8P](https://gitee.com/openharmony/build/issues/I4PW8P)|【新增特性】支持生成部件列表和部件依赖关系|编译构建子系统|SIG_CompileRuntime|@烈烈(https://gitee.com/xiaolielie)|
| 196   |[I4ROL2](https://gitee.com/openharmony/applications_call/issues/I4ROL2) |【通话】-通话中支持DTMF键盘 | 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 197   |[I4ROL0](https://gitee.com/openharmony/applications_call/issues/I4ROL0) |【通话】-通话中显示状态及计时 | 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 198   |[I4ROKZ](https://gitee.com/openharmony/applications_call/issues/I4ROKZ) |【通话】-来电支持通话页面拉起| 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 199   |[I4ROKY](https://gitee.com/openharmony/applications_call/issues/I4ROKY) |【通话】-通话中显示联系人号码和姓名 | 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 200   |[I4ROLD](https://gitee.com/openharmony/applications_call/issues/I4ROLD) |【通话】-来电显示通知 | 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 201   |[I4ROLG](https://gitee.com/openharmony/applications_call/issues/I4ROLG) |【短信】- 短信 - 短信接收 | 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 202   |[I4ROKU](https://gitee.com/openharmony/applications_mms/issues/I4ROKU) |【短信】- 短信 - 短信接收 | 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 203   |[I4QKLL](https://gitee.com/openharmony/applications_mms/issues/I4QKLL) |【短信】- 短信 - 短信单发（单卡）| 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 204   |[I4QKLK](https://gitee.com/openharmony/applications_mms/issues/I4QKLK) |【短信】- 短信 - 短信送达报告 | 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 205 |[I4WTGK](https://gitee.com/openharmony/notification_ans_standard/issues/I4WTGK) |【新增特性】支持模板通知注册、查询和发送 | 事件通知子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 206 |[I4WTJA](https://gitee.com/openharmony/appexecfwk_standard/issues/I4WTJA) |【增强特性】新增字段的Schema校验 | 包管理子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 207 |[I4WTJV](https://gitee.com/openharmony/appexecfwk_standard/issues/I4WTJV) |【新增特性】支持查询包含metadata的组件信息和应用信息 | 包管理子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 208 |[I4WTM0](https://gitee.com/openharmony/appexecfwk_standard/issues/I4WTM0) |【增强特性】数据目录创建 | 包管理子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 209 |[I4WTM9](https://gitee.com/openharmony/appexecfwk_standard/issues/I4WTM9) |【增强特性】支持安装包信息新增字段查询 | 包管理子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 210 |[I4WU7S](https://gitee.com/openharmony/appexecfwk_standard/issues/I4WU7S) |【增强特性】支持可卸载预置鸿蒙应用的卸载 | 包管理子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 211 |[I4WU8F](https://gitee.com/openharmony/appexecfwk_standard/issues/I4WU8F) |【增强特性】支持可卸载预置应用的升级 | 包管理子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 212 |[I4WU8Z](https://gitee.com/openharmony/appexecfwk_standard/issues/I4WU8Z) |【新增特性】支持可卸载预置应用卸载后的恢复 | 包管理子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 213 |[I4WU9N](https://gitee.com/openharmony/appexecfwk_standard/issues/I4WU9N) |【增强特性】支持hap包中config文件新增字段的解析和存储 | 包管理子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 214 |[I4WUA1](https://gitee.com/openharmony/appexecfwk_standard/issues/I4WUA1) |【新增特性】禁用/使能APP、组件 | 包管理子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 215 |[I4WUAL](https://gitee.com/openharmony/appexecfwk_standard/issues/I4WUAL) |【新增特性】支持查询仅系统应用的组件信息 | 包管理子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 216 |[I4PKZ3](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKZ3) |【新增特性】应用AccessToken的初始化及删除 | 包管理子系统 | SIG_ApplicationFramework |[@shuaytao](https://gitee.com/shuaytao)|
| 217 |[I4WV7N](https://gitee.com/openharmony/aafwk_standard/issues/I4WV7N) |【新增特性】【任务管理】查询任务列表 | 元能力子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 218 |[I4WV8I](https://gitee.com/openharmony/aafwk_standard/issues/I4WV8I) |【新增特性】【任务管理】同步任务列表 | 元能力子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 219 |[I4WV8Y](https://gitee.com/openharmony/aafwk_standard/issues/I4WV8Y) |【资料】跨设备启动组件新增/增强特性资料说明 | 元能力子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 220 |[I4WV9L](https://gitee.com/openharmony/aafwk_standard/issues/I4WV9L) |【增强特性】【DMS】跨设备组件调用验收demo | 元能力子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 221 |[I4WVA1](https://gitee.com/openharmony/aafwk_standard/issues/I4WVA1) |【增强特性】【DMS】跨设备启动组件验收demo | 元能力子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 222 |[I4WVAG](https://gitee.com/openharmony/aafwk_standard/issues/I4WVAG) |【增强特性】【框架】支持跨设备任务迁移 | 元能力子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 223 |[I4WVBL](https://gitee.com/openharmony/aafwk_standard/issues/I4WVBL) |【增强特性】【AMS】支持跨设备任务迁移 | 元能力子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 224 |[I4WVC4](https://gitee.com/openharmony/aafwk_standard/issues/I4WVC4) |【增强特性】【AMS】支持迁移结果通知客户端 | 元能力子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 225 |[I4WVCI](https://gitee.com/openharmony/aafwk_standard/issues/I4WVCI) |【增强特性】【DMS】支持跨设备任务迁移 | 元能力子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 226 |[I4WVCW](https://gitee.com/openharmony/aafwk_standard/issues/I4WVCW) |【增强特性】【DMS】支持跨设备组件调用 | 元能力子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 227 |[I4WVDI](https://gitee.com/openharmony/aafwk_standard/issues/I4WVDI) |【增强特性】【AMS】支持跨设备组件调用 | 元能力子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 228 |[I4WVEG](https://gitee.com/openharmony/aafwk_standard/issues/I4WVEG) |【增强特性】【框架】支持跨设备组件调用 | 元能力子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 229 |[I4WVGL](https://gitee.com/openharmony/aafwk_standard/issues/I4WVGL) |【增强特性】【AMS】支持跨设备启动组件 | 元能力子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 230 |[I4WVH1](https://gitee.com/openharmony/aafwk_standard/issues/I4WVH1) |【增强特性】【框架】支持跨设备启动组件 | 元能力子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 231 |[I4WVHL](https://gitee.com/openharmony/aafwk_standard/issues/I4WVHL) |【增强特性】【DMS】支持跨设备启动组件 | 元能力子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 232 |[I4WVI9](https://gitee.com/openharmony/aafwk_standard/issues/I4WVI9) |【增强特性】上下文提供获取应用不同路径接口 | 元能力子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 233 |[I4MBT0](https://gitee.com/openharmony/aafwk_standard/issues/I4MBT0) |【增强特性】上下文提供获取资源管理器接口 | 元能力子系统 | SIG_ApplicationFramework |[@dongjinguang](https://gitee.com/dongjinguang)|
| 234 |[I4MBT1](https://gitee.com/openharmony/aafwk_standard/issues/I4MBT1) |【增强特性】提供创建指定应用上下文的能力 | 元能力子系统 | SIG_ApplicationFramework |[@dongjinguang](https://gitee.com/dongjinguang)|
| 235 |[I4WVJJ](https://gitee.com/openharmony/aafwk_standard/issues/I4WVJJ) |【增强特性】系统环境变化通知AbilityStage和Extension | 元能力子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 236 |[I4WVJY](https://gitee.com/openharmony/aafwk_standard/issues/I4WVJY) |【增强特性】系统环境变化通知支持深浅色模式 | 元能力子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 237 |[I4WVKB](https://gitee.com/openharmony/aafwk_standard/issues/I4WVKB) |【增强特性】系统环境变化通知支持字体大小变化 | 元能力子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 238 |[I4WVKN](https://gitee.com/openharmony/aafwk_standard/issues/I4WVKN) |【增强特性】系统环境变化通知支持转屏 | 元能力子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 239 |[I4WVL2](https://gitee.com/openharmony/aafwk_standard/issues/I4WVL2) |【增强特性】系统环境变化通知支持displayid | 元能力子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 240 |[I4WVLM](https://gitee.com/openharmony/aafwk_standard/issues/I4WVLM) |【增强特性】系统环境变化通知支持densitydpi | 元能力子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 241 |[I4MBT5](https://gitee.com/openharmony/aafwk_standard/issues/I4MBT5) |【新增特性】启动拉起调试引擎 | 元能力子系统 | SIG_ApplicationFramework |[@dongjinguang](https://gitee.com/dongjinguang)|
| 242 |[I4MBT6](https://gitee.com/openharmony/aafwk_standard/issues/I4MBT6) |【新增特性】支持分享图片、视频、文件等到其他应用 | 元能力子系统 | SIG_ApplicationFramework |[@dongjinguang](https://gitee.com/dongjinguang)|
| 243 |[I4MBT8](https://gitee.com/openharmony/aafwk_standard/issues/I4MBT8) |【新增特性】通用组件启动过程联调 | 元能力子系统 | SIG_ApplicationFramework |[@dongjinguang](https://gitee.com/dongjinguang)|
| 244 |[I4MBT9](https://gitee.com/openharmony/aafwk_standard/issues/I4MBT9) |【新增特性】通用组件从前台调度到后台的过程联调 | 元能力子系统 | SIG_ApplicationFramework |[@dongjinguang](https://gitee.com/dongjinguang)|
| 245 |[I4MBTA](https://gitee.com/openharmony/aafwk_standard/issues/I4MBTA) |【新增特性】结束通用组件 | 元能力子系统 | SIG_ApplicationFramework |[@dongjinguang](https://gitee.com/dongjinguang)|
| 246 |[I4MBTB](https://gitee.com/openharmony/aafwk_standard/issues/I4MBTB) |【新增特性】通用组件支持带返回值调用 | 元能力子系统 | SIG_ApplicationFramework |[@dongjinguang](https://gitee.com/dongjinguang)|
| 247 |[I4MBTD](https://gitee.com/openharmony/aafwk_standard/issues/I4MBTD) |【新增特性】支持Ability的可见性配置 | 元能力子系统 | SIG_ApplicationFramework |[@dongjinguang](https://gitee.com/dongjinguang)|
| 248 |[I4MBTE](https://gitee.com/openharmony/aafwk_standard/issues/I4MBTE) |【新增特性】支持从系统服务启动Ability | 元能力子系统 | SIG_ApplicationFramework |[@dongjinguang](https://gitee.com/dongjinguang)|
| 249 |[I4MBTF](https://gitee.com/openharmony/aafwk_standard/issues/I4MBTF) |【新增特性】提供Ability和上下文 | 元能力子系统 | SIG_ApplicationFramework |[@dongjinguang](https://gitee.com/dongjinguang)|
| 250 |[I4MBTG](https://gitee.com/openharmony/aafwk_standard/issues/I4MBTG) |【新增特性】创建通用组件 | 元能力子系统 | SIG_ApplicationFramework |[@dongjinguang](https://gitee.com/dongjinguang)|
| 251 |[I4MBTH](https://gitee.com/openharmony/aafwk_standard/issues/I4MBTH) |【新增特性】启动通用组件 | 元能力子系统 | SIG_ApplicationFramework |[@dongjinguang](https://gitee.com/dongjinguang)|
| 252 |[I4MBTI](https://gitee.com/openharmony/aafwk_standard/issues/I4MBTI) |【新增特性】调度通用组件从前台到后台 | 元能力子系统 | SIG_ApplicationFramework |[@dongjinguang](https://gitee.com/dongjinguang)|
| 253 |[I4MBTJ](https://gitee.com/openharmony/aafwk_standard/issues/I4MBTJ) |【新增特性】获取Scene对象 | 元能力子系统 | SIG_ApplicationFramework |[@dongjinguang](https://gitee.com/dongjinguang)|
| 254 |[I4WVO4](https://gitee.com/openharmony/aafwk_standard/issues/I4WVO4) |【增强特性】PA对接方舟引擎 | 元能力子系统 | SIG_ApplicationFramework |[@xzz_0810](https://gitee.com/xzz_0810)|
| 255 |[I4MBTK](https://gitee.com/openharmony/aafwk_standard/issues/I4MBTK) |【新增特性】提供Service和上下文 | 元能力子系统 | SIG_ApplicationFramework |[@dongjinguang](https://gitee.com/dongjinguang)|
| 256 |[I4MBTL](https://gitee.com/openharmony/aafwk_standard/issues/I4MBTL) |【增强特性】服务组件生命周期 | 元能力子系统 | SIG_ApplicationFramework |[@dongjinguang](https://gitee.com/dongjinguang)|
| 257 |[I4MBTM](https://gitee.com/openharmony/aafwk_standard/issues/I4MBTM) |【增强特性】服务组件启动/调用 | 元能力子系统 | SIG_ApplicationFramework |[@dongjinguang](https://gitee.com/dongjinguang)|
| 258 |[I4QZVB](https://gitee.com/openharmony/security_access_token/issues/I4QZVB)  | 【部件化专项】【Access Token部件】部件标准化     | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 259 |[I4WVPH](https://gitee.com/openharmony/security_access_token/issues/I4WVPH)  | 【新增规格】AT同步服务基本框架     | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 260 |[I4WVQT](https://gitee.com/openharmony/security_access_token/issues/I4WVQT)  | 【新增规格】native的Token创建和更新机制      | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 261 | [I4WTQ5](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTQ5) | 【新增规格】ACE打包工具适配包结构和配置文件变化                          | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 262 | [I4WTPI](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTPI) | 【新增规格】video组件重新对接窗口新架构                          | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 263 | [I4WTPR](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTPR) | 【新增规格】Swiper组件动画自定义能力支持                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 264 | [I4WTQ7](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTQ7) | 【新增规格】低代码编译支持                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 265 | [I4WTR8](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTR8) | 【新增特性】Web组件能力支持                          | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 266 | [I4WTRB](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTRB) | 【新增特性】路由信息分布式迁移支持                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 267 | [I4WTQJ](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTQJ) | 【新增规格】输入组件键盘快捷键支持                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 268 | [I4WTR5](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTR5) | 【新增特性】XComponent组件特性支持基础渲染规格                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 269 | [I4WTR2](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTR2) | 【新增规格】XComponent组件支持Workder特性支持多级Worker规格和调试                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 270 | [I4WTQV](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTQV) | 【新增特性】增加场景数据存储特性                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 271 | [I4WTPS](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTPS) | 【新增规格】新增Touch事件支持多点触控信息                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 272 | [I4WWTB](https://gitee.com/openharmony/sensors_sensor/issues/I4WWTB) | 【泛sensor】系统部件标准化 | 标准系统 | SIG_DistributedHardwareManagement | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 273 | [I4WWRO](https://gitee.com/openharmony/msdp_device_status/issues/I4WWRO) | 【msdp】device_status部件标准化 | 标准系统 | SIG_DistributedHardwareManagement | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 274 | [I4WWRP](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWRP) | 【多模】input部件标准化 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 275 | [I4R304](https://gitee.com/openharmony/windowmanager/issues/I4R304) | 【可测试性】提供命令行截图能力 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 276 | [I4R9OU](https://gitee.com/openharmony/windowmanager/issues/I4R9OU) | 【window_manager】【新增特性】提供沉浸式能力 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 277 | [I4R306](https://gitee.com/openharmony/windowmanager/issues/I4R306) | 【window_manager】【新增特性】快照能力 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 278 | [I4R307](https://gitee.com/openharmony/windowmanager/issues/I4R307) | 【window_manager】【新增特性】快照能力 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 279 | [I4R30C](https://gitee.com/openharmony/windowmanager/issues/I4R30C) | 【window_manager】【新增特性】应用主窗口支持分屏显示 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 280 | [I4R2ZY](https://gitee.com/openharmony/windowmanager/issues/I4R2ZY) | 【window_manager】【增强特性】：提供输入法窗口 重构特性 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 281 | [I4R300](https://gitee.com/openharmony/windowmanager/issues/I4R300) | 【window_manager】【增强特性】提供输入法窗口 新增特性 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 282 | [I4R9OW](https://gitee.com/openharmony/windowmanager/issues/I4R9OW) | 【window_manager】【新增规格】系统窗口创建和管理能力 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 283 | [I4R9OX](https://gitee.com/openharmony/windowmanager/issues/I4R9OX) | 【window_manager】【新增规格】系统窗口创建和管理能力 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 284 | [I4RKT8](https://gitee.com/openharmony/graphic_standard/issues/I4RKT8) | 【render_service部件】【新增规格】新增ACE1.0控件接入RenderService渲染后端规格 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 285 | [I4RKT6](https://gitee.com/openharmony/graphic_standard/issues/I4RKT6) | 【render_service部件】【新增特性】新增RenderService支持多屏绘制特性 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 286 | [I4RKT4](https://gitee.com/openharmony/graphic_standard/issues/I4RKT4) | 【render_service部件】【新增特性】新增RenderService支持窗口绘制特性 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 287 | [I4R9PB](https://gitee.com/openharmony/graphic_standard/issues/I4R9PB) | 【composer部件】【新增特性 显示设备管理】显示设备管理 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 288 | [I4RKSY](https://gitee.com/openharmony/graphic_standard/issues/I4RKSY) | 【2d_engine部件】 skia及其依赖库开源引入 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 289 | [I4RKSW](https://gitee.com/openharmony/graphic_standard/issues/I4RKSW) | 【drawing部件】提供3D 图形能力支持NDK能力 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 290 | [I4RKSV](https://gitee.com/openharmony/graphic_standard/issues/I4RKSV) | 【drawing部件】提供webgl 图形能力 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 291 | [I4ZEKH](https://gitee.com/openharmony/windowmanager/issues/I4ZEKH) | 【window_manager】【新增特性】支持亮屏灭屏流程 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |

## OpenHarmony 3.1.3.5版本特性清单：

| no   | issue                                                        | feture description                           | platform       | sig                      | owner                                             |
| :--- | ------------------------------------------------------------ | :------------------------------------------- | :------------- | :----------------------- | :------------------------------------------------ |
| 1    | [I4P7F1](https://gitee.com/openharmony/ark_js_runtime/issues/I4P7F1) | [语言编译运行时子系统]方舟支持列号显示       | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)     |
| 2    | [I4PC3R](https://gitee.com/openharmony/aafwk_standard/issues/I4PC3R) | 【新增特性】提供卡片开发基础能力             | 元能力子系统   | SIG_ApplicationFramework | [@lsq1474521181](https://gitee.com/lsq1474521181) |
| 3    | [I4PCGJ](https://gitee.com/openharmony/aafwk_standard/issues/I4PCGJ) | 【资料】提供卡片框架新增/增强特性资料说明    | 元能力子系统   | SIG_ApplicationFramework | [@lsq1474521181](https://gitee.com/lsq1474521181) |
| 4    | [I4PCO3](https://gitee.com/openharmony/aafwk_standard/issues/I4PCO3) | 【新增特性】主线程EventRunner耗时检测        | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)     |
| 5    | [I4PPZF](https://gitee.com/openharmony/aafwk_standard/issues/I4PPZF) | 【增强特性】卡片框架适配配置文件变更         | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 6    | [I4PQ0I](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ0I) | 【增强特性】获取启动参数                     | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 7    | [I4PQ0K](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ0K) | 【增强特性】扩展Extension独立进程运行        | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 8    | [I4PQ0M](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ0M) | 【增强特性】上下文提供消息发送和监听能力     | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 9    | [I4PQ0Z](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ0Z) | 【新增特性】支持导出客户端应用信息           | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 10   | [I4PQ12](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ12) | 【新增特性】支持导出AMS信息                  | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 11   | [I4PQ13](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ13) | 【增强特性】上下文提供权限校验及权限申请接口 | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 12   | [I4PQ19](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ19) | 【新增特性】支持桌面进程异常恢复             | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 13   | [I4PQ1E](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ1E) | 【增强特性】支持常驻进程异常恢复             | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 14   | [I4PQ1O](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ1O) | 【新增特性】支持NewWant                      | 元能力子系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)           |
| 15   | [I4PKYG](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYG) | 【新增特性】支持关键流程hitrace              | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)           |
| 16   | [I4PKYK](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYK) | 【增强特性】启动扫描                         | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)           |
| 17   | [I4SIH9](https://gitee.com/openharmony/appexecfwk_standard/issues/I4SIH9) | 【新增特性】应用申请权限管理                 | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)           |
| 18   | [I4PBSZ](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBSZ) | 【新增特性】根据设备状态决策通知是否提醒     | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)           |
| 19  | [I4QESE](https://gitee.com/openharmony/device_manager/issues/I4QESE)  | 【新增特性】PIN码认证过程中的界面实现    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116)
| 20  | [I4QESK](https://gitee.com/openharmony/device_manager/issues/I4QESK)  | 【部件化专项】【device_manager部件】device_manager部件标准化   | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116)
| 21    | [I4QEKW](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKW) | 【新增特性】提供codec设备驱动模型，支持codec类型设备                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 22    | [I4QEKX](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKX) | 【特性增强】pipeline模块能力增强                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 23    | [I4QT44](https://gitee.com/openharmony/resourceschedule_resource_schedule_service/issues/I4QT44) | 【新增特性】短时任务相关资料文档 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 24   | [I4R2CV](https://gitee.com/openharmony/global_i18n_standard/issues/I4R2CV) | 【增强特性】大小写转换                         | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 25   | [I4R2X9](https://gitee.com/openharmony/global_resmgr_standard/issues/I4R2X9) | 【部件化专项】全球化子系统部件标准化                                     | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 26   | [I4R3CG](https://gitee.com/openharmony/global_resmgr_standard/issues/I4R3CG) | 【新增特性】资源预览优化                                     | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 27   | [I4OH95](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4OH95?from=project-issue) | 【部件化专项】系统服务管理子系统部件标准化                   | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 28   | [I4RD2M](https://gitee.com/openharmony/kernel_liteos_m/issues/I4RD2M?from=project-issue) | 【部件化专项】LiteOS-M内核部件标准化                  | 轻内核子系统       | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5)
| 29   | [I4RD2U](https://gitee.com/openharmony/kernel_liteos_a/issues/I4RD2U?from=project-issue) | 【部件化专项】LiteOS-A内核部件标准化                  | 轻内核子系统       | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5)
| 30   | [I4R6SK](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4R6SK)|【部件化专项】【key_value_store部件】key_value_store部件标准化|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 31    | [I4QEKQ](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKQ) | 【新增特性】Display的Gralllo、Gfx和Device的HDI接口实现服务                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 32    | [I4QEKS](https://gitee.com/openharmony/drivers_framework/issues/I4QEKS) | 【新增特性】platform_driver部件标准化                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 33    | [I4QEKT](https://gitee.com/openharmony/drivers_framework/issues/I4QEKT) | 【新增特性】peripheral_driver部件标准化                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 34    | [I4QEKU](https://gitee.com/openharmony/drivers_framework/issues/I4QEKU) | 【新增特性】driver_framework部件标准化                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 35  | [I4RTX4](https://gitee.com/openharmony/security_device_security_level/issues/I4RTX4)              | 【部件化专项】设备安全等级管理（DSLM）部件标准             | 标准系统        | SIG_Security                      | [@zhirenx](https://gitee.com/zhirenx)                    |
| 36   | [I4P7EX](https://gitee.com/openharmony/ark_js_runtime/issues/I4P7EX) | [语言编译运行时子系统]DFX维测支持运行时支持DFX/在异常时需要hook机制以及栈列信息特性 | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 37   | [I4RFWF](https://gitee.com/openharmony/security_deviceauth/issues/I4RFWF) | 【部件化专项】【deviceauth部件】deviceauth部件标准化 | 标准系统       | SIG_Security       | [@lvyuanmin](https://gitee.com/lvyuanmin)         |
| 38   | [I4S36A](https://gitee.com/openharmony/js_util_module/issues/I4S36A) | [语言编译运行时子系统]【部件化专项】ts_js_common_api部件标准化 | 标准系统     | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
|  39  | [I4S36C](https://gitee.com/openharmony/third_party_musl/issues/I4S36C) | [语言编译运行时子系统]【部件化专项】cpp_compiler_toolchain部件标准化 | 标准系统     | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 40   | [I4S36D](https://gitee.com/openharmony/ark_js_runtime/issues/I4S36D) | [语言编译运行时子系统]【部件化专项】ark_ide_support部件标准化 | 标准系统     | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 41   | [I4S36F](https://gitee.com/openharmony/ark_ts2abc/issues/I4S36F) | [语言编译运行时子系统]【部件化专项】ark_frontend_compiler部件标准化 | 标准系统     | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 42   | [I4S36G](https://gitee.com/openharmony/ark_js_runtime/issues/I4S36G) | [语言编译运行时子系统]【部件化专项】ark_runtime部件标准化 | 标准系统     | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 43    | [I4RXK2](https://gitee.com/openharmony/startup_init_lite/issues/I4RXK2) | 【新增特性】支持为进程代持fd                      | 标准系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 44    | [I4RXJM](https://gitee.com/openharmony/startup_init_lite/issues/I4RXJM) | 【新增特性】支持进程根据热插拔事件按需启动                                | 标准系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 45    | [I4RXJ9](https://gitee.com/openharmony/startup_init_lite/issues/I4RXJ9) | 【新增特性】支持socket类进程按需启动                               | 标准系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 46    | [I4RXJ2](https://gitee.com/openharmony/startup_init_lite/issues/I4RXJ2) | 【新增规格】统一init维护命令                                | 标准系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 47    | [I4S6QC](https://gitee.com/openharmony/useriam_user_auth/issues/I4S6QC) | 【资料】用户IAM子系统介绍资料需求                                | 用户IAM子系统 | SIG_Security       | [@wangxu](https://gitee.com/wangxu43)     |
| 48   | [I4LRGQ](https://gitee.com/openharmony/kernel_linux_5.10/issues/I4LRGQ)  | [内核子系统]【新增特性】OpenHarmony内核基线使能     | 内核子系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 49   | [I4RU58](https://gitee.com/openharmony/security_dataclassification/issues/I4RU58)   | 【部件化专项】【data_transit_mgr_lib部件】data_transit_mgr_lib部件标准化  | 标准系统   | SIG_Security  | [@wangyongzhong2](https://gitee.com/wangyongzhong2)  |
| 50   | [I4SEZD](https://gitee.com/openharmony/security_access_token/issues/I4SEZD)  | 【新增规格】动态权限弹窗界面实现    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 51   | [I4SEZ7](https://gitee.com/openharmony/security_access_token/issues/I4SEZ7)  | 【动态权限设置】实现动态权限授权机制    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 52   | [I4ITYY](https://gitee.com/openharmony/account_os_account/issues/I4ITYY)  | [帐号子系统]支持应用账号基础鉴权功能    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 53   | [I4SHYL](https://gitee.com/openharmony/account_os_account/issues/I4SHYL)  | 【资料】app_account_standard部件应用账号管理需求    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 54   | [I4QT4W](https://gitee.com/openharmony/account_os_account/issues/I4QT4W)  | [账号子系统]【部件化专项】【os_account部件】部件标准化    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 55    |[I4WV31](https://gitee.com/openharmony/startup_init_lite/issues/I4WV31) | 【部件化专项】启动子系统部件标准化       | 标准系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 56 | [I4WTQK](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTQK) | 【新增特性】鼠标双击选字                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 57 | [I4WTPE](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTPE) | 【新增规格】动画能力增强                          | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 58 | [I4WTPG](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTPG) | 【新增规格】基础动画参数配置增强                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 59 | [I4WTPH](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTPH) | 【新增规格】row 和column 容器的孩子节点支持  flex属性                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 60 | [I4WTP2](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTP2) | 【测试框架】HiTrace能力对接                          | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 61 | [I4WTR9](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTR9) | 【新增特性】焦点设置支持                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 62 | [I4WTPC](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTPC) | 【部件化专项】ACE开发框架子系统部件标准化                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 63    |[I4X0Z3](https://gitee.com/openharmony/distributed_hardware_fwk/issues/I4X0Z3) | 【部件化专项】【distributed_hardware_fwk部件】distributed_hardware_fwk部件标准化       | 标准系统 | SIG_DistributedHardwareManagement       | [@hwzhangchuang](https://gitee.com/hwzhangchuang)     |
| 64    |[I4X0WN](https://gitee.com/openharmony/distributed_camera/issues/I4X0WN) | 【部件专项化】【distributed_camera部件】distributed_camera部件标准化       | 标准系统 | SIG_DistributedHardwareManagement       | [@hwzhangchuang](https://gitee.com/hwzhangchuang)     |
| 65    |[I4WXHW](https://gitee.com/openharmony/update_updater/issues/I4WXHW) | 【部件化专项】升级服务子系统部件标准化       | 标准系统 | SIG_BscSoftSrv       | [@hughes802](https://gitee.com/hughes802)     |
| 66 | [I4R9OS](https://gitee.com/openharmony/windowmanager/issues/I4R9OS) | 【window_manager】【新增特性】支持窗口装饰 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 67 | [I4R309](https://gitee.com/openharmony/windowmanager/issues/I4R309) | 【window_manager】【新增规格】增强特性 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 68 | [I4R30G](https://gitee.com/openharmony/windowmanager/issues/I4R30G) | 【window_manager】【新增特性】应用主窗口支持自由窗口显示 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 69 | [I4ZCG4](https://gitee.com/openharmony/graphic_standard/issues/I4ZCG4) | 【部件化专项】图形图像子系统部件标准化 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 70 | [I4XXHF](https://gitee.com/openharmony/third_party_libdrm/issues/I4XXHF) | 【SysCap】图形子系统支持SysCap机制 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 71 | [I4ZELE](https://gitee.com/openharmony/windowmanager/issues/I4ZELE) | 【部件化专项】窗口管理子系统部件标准化口管理子系统部件标准化 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |

## OpenHarmony 3.1.5.1版本特性清单：

| no   | issue                                                        | feture description                                           | platform       | sig                      | owner                                                 |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :------------- | :----------------------- | :---------------------------------------------------- |
| 1    | [I4NJTS](https://gitee.com/openharmony/ace_engine_lite/issues/I4NJTS) | [轻量级图形子系统]支持通用touch事件、list组件支持scrollbottom/scrolltop事件 | 轻量系统       | SIG_AppFramework         | [@piggyguy](https://gitee.com/piggyguy)               |
| 2    | [I4NJTD](https://gitee.com/openharmony/graphic_ui/issues/I4NJTD) | [轻量级图形子系统]list组件支持scrollbottom/scrolltop事件     | 轻量系统       | SIG_AppFramework         | [@piggyguy](https://gitee.com/piggyguy)               |
| 3    | [I4OGF0](https://gitee.com/openharmony/powermgr_thermal_manager/issues/I4OGF0) | 【资料】thermal_manager部件类资料需求                        | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 4    | [I4OEQT](https://gitee.com/openharmony/powermgr_power_manager/issues/I4OEQT) | 【集成验证】不亮屏检测                                       | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 5    | [I4P7EY](https://gitee.com/openharmony/ark_js_runtime/issues/I4P7EY) | [语言编译运行时子系统]支持TS Runtime                         | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 6    | [I4P7F5](https://gitee.com/openharmony/third_party_musl/issues/I4P7F5) | [语言编译运行时子系统]支持LLDB命令窗口                       | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 7    | [I4P7EV](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4P7EV) | [语言编译运行时子系统]支持ts/js模块化编译                    | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 8    | [I4OGD1](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGD1?from=project-issue) | 【新增特性】【DMS】支持组件间跨设备的onCall调用              | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 9   | [I4OGCY](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCY?from=project-issue) | 【新增特性】【AMS/框架】支持组件间跨设备的onCall调用         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 10   | [I4OGCX](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCX?from=project-issue) | 【新增特性】【任务管理】快照变化更新                         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 11   | [I4OGCW](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCW?from=project-issue) | 【新增特性】【任务管理】任务变化更新                         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 12   | [I4OGCV](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCV?from=project-issue) | 【新增特性】【任务管理】同步任务快照                         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 13   | [I4OGCU](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCU?from=project-issue) | 【新增特性】【任务管理】结束任务同步                         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 14   | [I4OGCT](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCT?from=project-issue) | 【新增特性】【任务管理】发起任务同步                         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 15   | [I4OGCS](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCS?from=project-issue) | 【新增特性】【任务管理】查询任务快照                         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 16   | [I4OGCP](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCP?from=project-issue) | 【新增特性】【DMS】跨设备组件权限校验                        | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 17   | [I4OGCK](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCK?from=project-issue) | 【增强特性】框架等待分布式对象同步完成后返回迁移结果         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 18   | [I4OH99](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4OH99?from=project-issue) | 【samgr】注册/查询本地系统服务权限控制                       | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 19   | [I4TS0Z](https://gitee.com/openharmony/distributedschedule_samgr_lite/issues/I4TS0Z?from=project-issue) | 【新增】轻量系统samgr支持远程服务管理                            | 轻量系统      | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 20   | [I4OH96](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4OH96?from=project-issue) | 【samgr】samgr资料                                           | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 21   | [I4OH93](https://gitee.com/openharmony/device_profile_core/issues/I4OH93?from=project-issue) | 【device_profile】同步功能适配可信群组                       | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 22   | [I4OGD7](https://gitee.com/openharmony/device_profile_core/issues/I4OGD7?from=project-issue) | 【device_profile】deviceProfile资料                          | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 23   | [I4PBBV](https://gitee.com/openharmony/notification_ces_standard/issues/I4PBBV) | 【新增特性】事件耗时调用                                     | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 24   | [I4PD0O](https://gitee.com/openharmony/notification_ans_standard/issues/I4PD0O) | 【特性增强】通知发送使能能力增强                             | 事件通知子系统 | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)                 |
| 25   | [I4PBQ1](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBQ1) | 【增强特性】分布式通知能力支持dump命令                       | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 26   | [I4PBR0](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBR0) | 【新增特性】支持其他设备的通知点击后在跨设备跳转             | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 27  | [I4PC2S](https://gitee.com/openharmony/notification_ces_standard/issues/I4PC2S) | 【新增特性】公共事件支持多用户特性                           | 事件通知子系统 | SIG_ApplicationFramework | [@lsq1474521181](https://gitee.com/lsq1474521181)     |
| 28  | [I4PCHK](https://gitee.com/openharmony/aafwk_standard/issues/I4PCHK) | 【新增特性】C++/JS 客户端与服务端互通                        | 元能力子系统   | SIG_ApplicationFramework | [@lsq1474521181](https://gitee.com/lsq1474521181)     |
| 29 | [I4PCS0](https://gitee.com/openharmony/aafwk_standard/issues/I4PCS0) | 【新增特性】提供Ability启动停止方法                          | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 30  | [I4PQ5F](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ5F) | 【资料】提供ZIDL部件新增/增强特性资料说明                    | 元能力子系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 31  | [I4PQ5N](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ5N) | 【增强特性】支持IAbilityController                           | 元能力子系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 32  | [I4PCNK](https://gitee.com/openharmony/aafwk_standard/issues/I4PCNK) | 【资料】提供运行管理新增/增强特性资料说明                    | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 33  | [I4PCPA](https://gitee.com/openharmony/aafwk_standard/issues/I4PCPA) | 【资料】提供环境变化新增/增强特性资料说明                    | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 34  | [I4PCV0](https://gitee.com/openharmony/aafwk_standard/issues/I4PCV0) | 【资料】提供通用组件新增/增强特性资料说明                    | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)                 |
| 35  | [I4PQ5X](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ5X) | 【增强特性】Extension接口整改                                | 元能力子系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 36  | [I4PCLY](https://gitee.com/openharmony/aafwk_standard/issues/I4PCLY) |【新增特性】对外接口权限校验验收|元能力子系统|SIG_ApplicationFramework|[@silent-dye](https://gitee.com/silent-dye)|
| 37  | [I4PCPI](https://gitee.com/openharmony/aafwk_standard/issues/I4PCPI) |【增强特性】支持系统环境查询|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu](https://gitee.com/xuezhongzhu)|
| 38  | [I4PCRG](https://gitee.com/openharmony/aafwk_standard/issues/I4PCRG) |【新增特性】提供Ability监听器|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 39 | [I4PCRL](https://gitee.com/openharmony/aafwk_standard/issues/I4PCRL) |【新增特性】测试框架整体功能|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 40  | [I4PCRO](https://gitee.com/openharmony/aafwk_standard/issues/I4PCRO) |【新增特性】测试框架需要提供如下查询相关的功能|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 41  | [I4PCRQ](https://gitee.com/openharmony/aafwk_standard/issues/I4PCRQ) |【新增特性】提供调度组件生命周期相关的功能|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 42  | [I4PCRX](https://gitee.com/openharmony/aafwk_standard/issues/I4PCRX) |【新增特性】测试框架提供可以执行shell命令能力|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 43  | [I4PCS6](https://gitee.com/openharmony/aafwk_standard/issues/I4PCS6) |【新增特性】AA适配测试框架|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 44  | [I4PCVU](https://gitee.com/openharmony/aafwk_standard/issues/I4PCVU) |【新增特性】通用组件call调用|元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 45  | [I4PKYA](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYA?from=project-issue) | 【新增特性】包命令行工具                                     | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 46  | [I4PKYS](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYS?from=project-issue) | 【新增特性】支持现有js查询接口的权限管控                     | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 47  | [I4PKYW](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYW?from=project-issue) | 【新增特性】支持现有js监听安装、卸载、更新状态变化接口的权限管控 | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 48  | [I4PKYX](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYX) | 【新增特性】支持现有js清理缓存接口的权限管控                 | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 49 | [I4PKYY](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYY) | 【新增特性】支持现有js使能/禁用应用/组件接口的权限管控       | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 50  | [I4PKYZ](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYZ) | 【新增特性】返回应用类型                                     | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 51  | [I4PKZ1](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKZ1) | 【新增特性】应用数据目标标签设置                             | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 52  | [I4PKYT](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYT) | 【新增特性】支持现有js安装接口的权限管控 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 53  | [I4QA3D](https://gitee.com/openharmony/appexecfwk_standard/issues/I4QA3D?from=project-issue) | 【增强特性】新增zlib解压、压缩数据native接口 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 54  | [I4SY5T](https://gitee.com/openharmony/appexecfwk_standard/issues/I4SY5T?from=project-issue) | 【SysCap】包管理子系统支持SysCap机制 | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 55  | [I4Q9S6](https://gitee.com/openharmony/kernel_liteos_m/issues/I4Q9S6)  | [内核子系统]【增强特性】支持内核接口融合    | 内核子系统 | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5)       |
| 56 | [I4QESV](https://gitee.com/openharmony/device_manager/issues/I4QESV)  | 【新增特性】设备可用状态上报性能跟踪   | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116)           |
| 57   | [I4QEKZ](https://gitee.com/openharmony/drivers_framework/issues/I4QEKZ) | 【新增特性】支持用户态平台驱动接口                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 58 | [I4QEL1](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEL1) | 【新增特性】【新增特性】支持基于FB显示架构的基础功能                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 59  | [I4QEL2](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEL2) | 【增强特性】马达驱动模型能力增强                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 60   | [I4QT3S](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QT3S) | 【新增特性】长时任务管理 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 61   | [I4QT3T](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QT3T) | 【新增特性】长时任务规范 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 62   | [I4QT3U](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QT3U) | 【新增特性】长时任务可维可测 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 63   | [I4QT3V](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QT3V) | 【新增特性】长时任务相关资料文档 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 64   | [I4QT3W](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QT3W) | 【新增特性】长时任务申请/注销 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 65   | [I4QU0O](https://gitee.com/openharmony/resourceschedule_work_scheduler/issues/I4QU0O) | 【新增特性】设备和系统相关状态检测 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 66   | [I4QU0P](https://gitee.com/openharmony/resourceschedule_work_scheduler/issues/I4QU0P) | 【新增特性】延迟任务调度 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 67   | [I4QU0R](https://gitee.com/openharmony/resourceschedule_work_scheduler/issues/I4QU0R) | 【新增特性】延迟任务后台管理 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 68   | [I4QU0S](https://gitee.com/openharmony/resourceschedule_work_scheduler/issues/I4QU0S) | 【新增特性】延迟任务可维可测 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 69  | [I4QU0T](https://gitee.com/openharmony/resourceschedule_work_scheduler/issues/I4QU0T) | 【新增特性】延迟任务适配PC多账户 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 70   | [I4U076](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4U076) | 【SysCap】全局资源调度管控子系统支持SysCap机制 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 71  | [I4R2Y1](https://gitee.com/openharmony/global_cust_lite/issues/I4R2Y1) | 【增强特性】提供定制框架js接口                               | 标准系统 | SIG_ApplicationFramework          | [@zhengbin5](https://gitee.com/zhengbin5)         |
| 72  | [I4R2Y8](https://gitee.com/openharmony/global_resmgr_standard/issues/I4R2Y8) | 【新增特性】支持颜色模式限定词                                     | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 73  | [I4R2YA](https://gitee.com/openharmony/global_resmgr_standard/issues/I4R2YA) | 【新增特性】新增资源管理NDK接口                                     | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 74  | [I4R2YC](https://gitee.com/openharmony/global_resmgr_standard/issues/I4R2YC) | 【新增特性】资源overlay                                     | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 75  | [I4R2MP](https://gitee.com/openharmony/global_i18n_standard/issues/I4R2MP) | 【新增特性】时区数据编译                         | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 76  | [I4R2LF](https://gitee.com/openharmony/global_i18n_standard/issues/I4R2LF) | 【新增特性】时区数据解析                         | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 77  | [I4R3FR](https://gitee.com/openharmony/global_i18n_standard/issues/I4R3FR) | 【资料】全球化新增API资料更新                       | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 78  | [I4RD3H](https://gitee.com/openharmony/kernel_liteos_m/issues/I4RD3H?from=project-issue) | 【liteos_m部件】增加支持POSIX接口                  | 轻内核子系统       | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5) |
| 79  | [I4QEKY](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKY) | 【新增特性】Audio支持模拟耳机设备                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
|  80  | [I4RTYX](https://gitee.com/openharmony/security_device_security_level/issues/I4RTYX?from=project-issue) | 【新增特性】【接口】提供查询本机或者组网内其它设备的设备安全等级信息的接口 | 标准系统 | SIG_Security | [@zhirenx](https://gitee.com/zhirenx) |
|  81  | [I4RTYW](https://gitee.com/openharmony/security_device_security_level/issues/I4RTYW?from=project-issue) | 【新增特性】【服务】支持获取自己或者组网内其它设备的设备安全等级信息 | 标准系统 | SIG_Security | [@zhirenx](https://gitee.com/zhirenx) |
|  82  | [I4RTYU](https://gitee.com/openharmony/security_device_security_level/issues/I4RTYU?from=project-issue) | 【新增特性】【服务】支持被组网内其它设备查询自己的设备安全等级信息 | 标准系统 | SIG_Security | [@zhirenx](https://gitee.com/zhirenx) |
|  83  | [I4RTYN](https://gitee.com/openharmony/security_device_security_level/issues/I4RTYN?from=project-issue) | 【新增特性】【南向】支持OEM厂家接入设备安全等级模块 | 标准系统 | SIG_Security | [@zhirenx](https://gitee.com/zhirenx) |
| 84 | [I4SY5E](https://gitee.com/openharmony/appexecfwk_standard/issues/I4SY5E?from=project-issue) | 【packing_tool部件】支持打包工具将syscap二进制文件打包到hap包中 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 85 | [I4SY5G](https://gitee.com/openharmony/appexecfwk_standard/issues/I4SY5G?from=project-issue) | 【bundle_manager部件】支持基于SysCap的应用安装 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 86    | [I4TNBV](https://gitee.com/openharmony/startup_init_lite/issues/I4TNBV) | 【新增规格】进程启动配置能力增强                                | 标准系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 87    | [I4TNBL](https://gitee.com/openharmony/startup_init_lite/issues/I4TNBL) | 【新增特性】支持UHDF类进程按需启动                                | 标准系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 88    | [I4TNBQ](https://gitee.com/openharmony/startup_init_lite/issues/I4TNBQ) | 【SysCap】启动恢复子系统支持SysCap机制                                | 标准系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 89    | [I4HAMD](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4HAMD) | 【data_share_ability】支持对数据访问方式的控制                    | 标准系统   | SIG_DataManagement | [@verystone](https://gitee.com/verystone)         |
| 90    | [I4TJFZ](https://gitee.com/openharmony/security_deviceauth/issues/I4TJFZ) | 【增强特性】DeviceAuth部件支持设备间互信关系认证的多用户隔离，使用指定系统用户下管理的互信关系进行认证                   | 标准系统   | SIG_Security | [@lvyuanmin](https://gitee.com/lvyuanmin)         |
| 91    | [I4TJG1](https://gitee.com/openharmony/security_deviceauth/issues/I4TJG1) | 【增强特性】DeviceAuth部件实现互信群组数据多实例，支持指定用户的数据查询                    | 标准系统   | SIG_Security | [@lvyuanmin](https://gitee.com/lvyuanmin)         |
| 92    | [I4TSJE](https://gitee.com/openharmony/useriam_faceauth/issues/I4TSJE) |  【新增规格】【face_auth】支持用户本地人脸录入   | 标准系统   | SIG_Security      | [@tianshi_liu](https://gitee.com/tianshi_liu)     |
| 93    | [I4TSJY](https://gitee.com/openharmony/useriam_faceauth/issues/I4TSJY) |  【新增规格】【face_auth】支持用户本地人脸认证   | 标准系统   | SIG_Security      | [@tianshi_liu](https://gitee.com/tianshi_liu)     |
| 94    | [I4TSK7](https://gitee.com/openharmony/useriam_faceauth/issues/I4TSK7) |  【新增规格】【face_auth】支持用户本地人脸删除   | 标准系统   | SIG_Security      | [@tianshi_liu](https://gitee.com/tianshi_liu)     |
| 95    | [I4TSKP](https://gitee.com/openharmony/useriam_faceauth/issues/I4TSKP) |  【需求描述】独立需求跟踪人脸认证框架DFX相关需求   | 标准系统   | SIG_Security      | [@tianshi_liu](https://gitee.com/tianshi_liu)     |
| 96    | [I4SAI0](https://gitee.com/openharmony/security_dataclassification/issues/I4SAI0) |  【新增特性】提供DataTransitMgrLib部件，支持数据跨设备流转时的管控策略   | 标准系统   | SIG_Security      | [@wangyongzhong2](https://gitee.com/wangyongzhong2)     |
| 97   | [I4H4FH](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4H4FH)|【distributed_kv_store】分布式数据库支持分类分级|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 98   | [I4H3M8](https://gitee.com/openharmony/distributeddatamgr_objectstore/issues/I4H3M8)|【新增特性】分布式数据对象支持复杂类型|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 99   | [I4HAMD](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4HAMD)|【data_share_ability】支持对数据访问方式的控制|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 100   | [I4PO00](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4PO00)|【分布式RDB】数据同步需求|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 101   | [I4OTW6](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4OTW6)|【distributed_kv_store】分布式数据库Query支持InKeys谓词|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 102   | [I4TXSP](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4TXSP)|【SysCap】分布式数据管理子系统支持SysCap机制|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 103   | [I4TTK5](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTK5)|【新增特性】支持磁盘管理查询的特性|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 104   | [I4TTJV](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTJV)|【新增特性】支持卷信息查询和管理特性|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 105   | [I4TTJN](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTJN)|【新增特性】支持外卡设备相关事件分发特性|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 106   | [I4TTJJ](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTJJ)|【资料】storage_manag   er部件资料需求|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 107   | [I4TTHQ](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTHQ)|【新增特性】支持外部存储访问需求|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 108   | [I4TTHF](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTHF)|【新增特性】支持外卡上下线管理特性|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 109   | [I4TTGR](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTGR)|【新增特性】【storage_manager部件】文件加密特性使能|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 110   | [I4TTH9](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTH9)|【新增特性】【storage_manager部件】CE密钥生命周期管理|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 111   | [I4TT7Y](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TT7Y)|【新增特性】【storage_manager部件】文件加密特性对外接口|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 112   | [I4SNSU](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4SNSU)|【新增特性】支持应用沙箱隔离能力|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 113   | [I4TTM7](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTM7)|【新增特性】支持设备间的sendfile能力|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 114   | [I4TTOU](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTOU)|【新增特性】支持提供用户态跨设备分享接口能力规格|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 115   | [I4TTOP](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTOP)|【新增特性】支持建立本地文件与分布式文件的映射规格|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 116   | [I4TTOJ](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTOJ)|【新增特性】为数据标签访问提供应用接口|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 117   | [I4TTOF](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTOF)|【新增特性】支持异账号能力规格|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 118   | [I4TTNG](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTNG)|【新增特性】支持数据分类设备分级，控制数据流转规格|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 119   | [I4TTN8](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTN8)|【新增特性】支持分布式文件系统的基础功能|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 120   | [I4TTMN](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTMN)|【新增特性】支持应用包名级权限配置|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 121 | [I4TT8L](https://gitee.com/openharmony/security_huks/issues/I4TT8L) |【新增规格】HUKS提供三段式密钥管理接口|安全子系统|SIG_Security|[@chaos-liang](https://gitee.com/Chaos-Liang)|
| 122 | [I4TT8P](https://gitee.com/openharmony/security_huks/issues/I4TT8P)|【新增规格】HUKS 架构优化，提供抽象统一的HAL层。|安全子系统|SIG_Security|[@chaos-liang](https://gitee.com/Chaos-Liang)|
| 123 | [I4TT8Z](https://gitee.com/openharmony/security_huks/issues/I4TT8Z)|【新增规格】HUKS支持多用户隔离|安全子系统|SIG_Security|[@chaos-liang](https://gitee.com/Chaos-Liang)|
| 124 | [I4S5RB](https://gitee.com/openharmony/developtools_hapsigner/issues/I4S5RB) |【新增特性】PKI应用签名工具支持生成Profile签名调试/发布证书|安全子系统| SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)|
| 125 | [I4S5R7](https://gitee.com/openharmony/developtools_hapsigner/issues/I4S5R7) |【新增特性】PKI应用签名工具支持生成应用调试/发布证书|安全子系统| SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)|
| 126 | [I4S5RC](https://gitee.com/openharmony/developtools_hapsigner/issues/I4S5RC) |【新增特性】PKI应用签名工具支持Profile文件签名|安全子系统| SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)|
| 127 | [I4S5QZ](https://gitee.com/openharmony/developtools_hapsigner/issues/I4S5QZ) |【新增特性】PKI应用签名工具支持Hap包签名|安全子系统| SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)|
| 128   | [I4U08D](https://gitee.com/openharmony/build/issues/I4U08D) | [语言编译运行时]【SysCap】语言编译器运行时子系统支持SysCap  | 标准系统 | SIG_CompileRuntime       | [@huanghuijin]()         |
| 129   | [I4P7F7](https://gitee.com/openharmony/third_party_musl/issues/I4P7F7) | [语言编译运行时，图形图像，DRF]NDK整体集成 | 标准系统 | SIG_CompileRuntime       | [@huanghuijin]()         |
| 130   | [I4QQ1E](https://gitee.com/openharmony/ark_runtime_core/issues/I4QQ1E) | [语言编译运行时]【新增规格】Pandafile模块中zlib替换miniz | 标准系统 | SIG_CompileRuntime       | [@huanghuijin]()         |
| 131   | [I4TMTO]() | [语言编译运行时]【c_cpp_runtime部件】动态库卸载 | 标准系统 | SIG_CompileRuntime       | [@huanghuijin]()         |
| 132   | [I4U73N](https://gitee.com/openharmony/filemanagement_file_api/issues/I4U73N)|【SysCap】文件管理子系统支持SysCap机制|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 133   | [I4Q6AQ](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AQ)|【【新增特性】Watchdog机制标准系统|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 134   | [I4Q6AP](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AP)|【新增特性】支持JS app性能分析信息调试调优能力|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 135   | [I4Q6AN](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AN)|【新增特性】：命令行调试通道|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 136   | [I4PJE6](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4PJE6)|【【新增特性】支持JS app内存信息调试调优能力|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 137   | [I4PJE5](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4PJE5)|【新增特性】支持JS app native内存信息调试调优能力|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 138   | [I4PJDO](https://gitee.com/openharmony/hiviewdfx_hicollie/issues/I4PJDO)|【新增特性】hungtask卡死-内核hungtask监控D状态进程|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 139   | [I4PJDN](https://gitee.com/openharmony/hiviewdfx_hicollie/issues/I4PJDN)|【新增特性】SCREEN ON卡死-不亮屏故障卡死|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 140   | [I4PJDP](https://gitee.com/openharmony/hiviewdfx_hicollie/issues/I4PJDP)|【新增特性】Power按键-长按、短按Power按键事件上报|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 141   | [I4PJDM](https://gitee.com/openharmony/hiviewdfx_hicollie/issues/I4PJDM)|【新增特性】INIT卡死-init长时间不执行|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 142   | [I4Q6AR](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AR)|【新增特性】鸿蒙芯片维测框架|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 143   | [I4Q6AZ](https://gitee.com/openharmony/hiviewdfx_hilog/issues/I4Q6AZ)|【资料】hilog部件的js api需求，kmsg支持需求，init日志支持需求|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 144   | [I4Q6B2](https://gitee.com/openharmony/hiviewdfx_hilog/issues/I4Q6B2)|【新增特性】hilog支持开机阶段的日志打印和保存|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 145   | [I4Q6B4](https://gitee.com/openharmony/hiviewdfx_hilog/issues/I4Q6B4)|【新增特性】hilog支持js接口|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 146   | [I4Q6BA](https://gitee.com/openharmony/hiviewdfx_hitrace/issues/I4Q6BA)|【增强特性】支持HiLog关联traceid|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 147   | [I4Q6B9](https://gitee.com/openharmony/hiviewdfx_hitrace/issues/I4Q6B9)|【增强特性】支持HiSysEvent关联traceid|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 148   | [I4Q6B7](https://gitee.com/openharmony/hiviewdfx_hitrace/issues/I4Q6B7)|【增强特性】支持HiAppEvent关联traceid|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 149   | [I4Q6B6](https://gitee.com/openharmony/hiviewdfx_hitrace/issues/I4Q6B6)|【增强特性】支持HiTrace JS接口|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 150   | [I4U0JZ](https://gitee.com/openharmony/hiviewdfx_hisysevent/issues/I4U0JZ)|【新增特性】供鸿蒙hisysevent系统事件管理|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 151   | [I4PUKP](https://gitee.com/openharmony/hiviewdfx_hisysevent/issues/I4PUKP)|【新增特性】hisysevent订阅接口支持TAG方式订阅事件|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 152   | [I4U0KL](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4U0KL)|【SysCap】DFX子系统支持SysCap机制|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 153   | [I4U0KP](https://gitee.com/openharmony/developtools_profiler/issues/I4U0KP)|【profiler部件】cpu profiler功能|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 154   | [I4U89V](https://gitee.com/openharmony/xts_acts/issues/I4U89V)|【新增】驱动/输入驱动XTS用例|轻量系统|compatibility|[@jiyong](https://gitee.com/jiyong)|
| 155   | [I4U8I3](https://gitee.com/openharmony/xts_acts/issues/I4U8I3)|【新增】智能开关面板样例|轻量系统|SIG_Kernel|[@kkup180](https://gitee.com/kkup180)|
| 156   | [I4U8KB](https://gitee.com/openharmony/xts_acts/issues/I4U8KB)|【新增】驱动/显示驱动适配|轻量系统|SIG_Driver|[@zianed](https://gitee.com/zianed)|
| 157   | [I4U8LZ](https://gitee.com/openharmony/xts_acts/issues/I4U8LZ)|【新增】驱动/显示驱动XTS用例|轻量系统|compatibility|[@jiyong](https://gitee.com/jiyong)|
| 158   | [I4U8N4](https://gitee.com/openharmony/xts_acts/issues/I4U8N4)|【新增】驱动/输入驱动适配|轻量系统|SIG_Driver|[@zianed](https://gitee.com/zianed)|
| 159   | [I4U8O1](https://gitee.com/openharmony/xts_acts/issues/I4U8O1)|【新增】芯片架构SoC与Board解耦端到端联调|轻量系统|SIG_Kernel|[@kkup180](https://gitee.com/kkup180)|
| 160   | [I4U8VP](https://gitee.com/openharmony/xts_acts/issues/I4U8VP)|【新增】编译构建/Module rules 编译构建流程适配SoC与Board分离机制|轻量系统|SIG_Kernel|[@kkup180](https://gitee.com/kkup180)|
| 161   | [I4U8WL](https://gitee.com/openharmony/xts_acts/issues/I4U8WL)|【新增】硬件芯片/hisilico跟随SoC与Board分离机制进行调整|轻量系统|SIG_DevBoard|[@northking-super](https://gitee.com/northking-super)|
| 162   | [I4S878](https://gitee.com/openharmony/xts_acts/issues/I4S878)|【新增】用户程序框架/轻设备包管理 XTS用例|轻量系统|compatibility|[@jiyong](https://gitee.com/jiyong)|
| 163   | [I4TY9D](https://gitee.com/openharmony/ace_engine_lite/issues/I4TY9D)|【新增】 轻量系统图形/UI组件和图形基础组件 XTS用例|轻量系统|compatibility|[@jiyong](https://gitee.com/jiyong)|
| 164   | [I4TFTB](https://gitee.com/openharmony/drivers_framework/issues/I4TFTB)|【关联】轻量系统新增HCS宏式解析接口|轻量系统|SIG_Driver|[@fx_zhang](https://gitee.com/fx_zhang)|
| 165   | [I4T1JA](https://gitee.com/openharmony/xts_acts/issues/I4T1JA)|【新增】轻量系统分布式软总线/分布式软总线 XTS用例|轻量系统|compatibility|[@jiyong](https://gitee.com/jiyong)|
| 166   | [I4TS0Z](https://gitee.com/openharmony/distributedschedule_samgr_lite/issues/I4TS0Z)|【新增】轻量系统samgr支持远程服务管理|轻量系统|SIG_DistributedSchedule|[@zjucx](https://gitee.com/zjucx)|
| 167   | [I4RXQ3](https://gitee.com/openharmony/kernel_linux_5.10/issues/I4RXQ3)  | [内核子系统]【新增特性】内存管理基础特性     | 内核子系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 168   | [I4TEGS](https://gitee.com/openharmony/kernel_linux_5.10/issues/I4TEGS)  | [内核子系统]【新增特性】F2FS末端性能优化     | 内核子系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 169   | [I4SRVK](https://gitee.com/openharmony/kernel_linux_5.10/issues/I4SRVK)  | [内核子系统]【新增特性】支持CPU轻量级隔离特性     | 内核子系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 170   | [I4SE2N](https://gitee.com/openharmony/resourceschedule_memmgr/issues/I4SE2N)  | [内核子系统]【新增特性】支持按照用户维度进行内存资源管控的能力     | 内核子系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 171 | [I4T402](https://gitee.com/openharmony/notification_ans_standard/issues/I4T402) | SR000GRI6M: 【SysCap】事件通知子系统支持SysCap机制 | 事件通知子系统 | SIG_ApplicationFramework | [@zero-cyc](https://gitee.com/zero-cyc) |
| 172 | [I4TEF8](https://gitee.com/openharmony/interface_sdk-js/issues/I4TEF8) | aafwk模块添加syscap字段信息 | 元能力子系统 | SIG_ApplicationFramework | @[guyuanzhang](https://gitee.com/guyuanzhang) |
| 173 | [I4STSP](https://gitee.com/openharmony/aafwk_standard/issues/I4STSP) | 支持灭屏、解锁场景应用生命周期变化 | 元能力子系统 | SIG_ApplicationFramework | @[jiangwensai](https://gitee.com/jiangwensai) |
| 174 | [I4T8CK](https://gitee.com/openharmony/aafwk_standard/issues/I4T8CK) | 常驻进程启动默认组件 | 元能力子系统 | SIG_ApplicationFramework | @[zhoujun62](https://gitee.com/zhoujun62) |
| 175 | [I4RV2A](https://gitee.com/openharmony/aafwk_standard/issues/I4RV2A) | extension读写权限配置 | 元能力子系统 | SIG_ApplicationFramework | @[zhoujun62](https://gitee.com/zhoujun62) |
| 176 | [I4S8X4](https://gitee.com/openharmony/aafwk_standard/issues/I4S8X4) | Uri权限动态授权 | 元能力子系统 | SIG_ApplicationFramework | @[altay](https://gitee.com/altay) |
| 177 | [I4U457](https://gitee.com/openharmony/aafwk_standard/issues/I4U457) | Uri权限校验接口 | 元能力子系统 | SIG_ApplicationFramework | @[altay](https://gitee.com/altay) |
| 178 | [I4UI3X](https://gitee.com/openharmony/aafwk_standard/issues/I4UI3X) | Uri权限生命周期管理 | 元能力子系统 | SIG_ApplicationFramework | @[xzz_0810](https://gitee.com/xzz_0810) |
| 179   | [I4TYDO](https://gitee.com/openharmony/security_access_token/issues/I4TYDO)  | 【新增规格】设备下线时的token信息删除    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 180   | [I4TYCK](https://gitee.com/openharmony/security_access_token/issues/I4TYCK)  | 【新增规格】分布式权限校验接口和机制    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 181   | [I4TYE7](https://gitee.com/openharmony/security_access_token/issues/I4TYE7)  | 【SysCap】程序访问控制子系统支持SysCap机制    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 182   | [I4IU5G](https://gitee.com/openharmony/account_os_account/issues/I4IU5G)  | [帐号子系统]支持本地多用户辅助管理工具    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 183   | [I4IU51](https://gitee.com/openharmony/account_os_account/issues/I4IU51)  | [帐号子系统]本地多用户基础信息约束    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 184   | [I4IU4D](https://gitee.com/openharmony/account_os_account/issues/I4IU4D)  | [帐号子系统]支持本地多用户接口权限检查及服务访问约束    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 185   | [I4TXW3](https://gitee.com/openharmony/account_os_account/issues/I4TXW3)  | 【SysCap】账号子系统支持SysCap机制    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 186   | [I4TYET](https://gitee.com/openharmony/security_selinux/issues/I4TYET)  | 【新增规格】提供parameter的selinux标签设置接口库    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 187   | [I4TIWQ](https://gitee.com/openharmony/usb_manager/issues/I4TIWQ)  | 【SysCap】USB服务子系统支持SysCap机制    | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 188    | [I4TJG3](https://gitee.com/openharmony/security_deviceauth/issues/I4TJG3) | 【增强特性】DeviceAuth部件支持帐号无关点对点信任关系建立、解除的多用户数据隔离                  | 标准系统   | SIG_Security | [@lvyuanmin](https://gitee.com/lvyuanmin)         |
| 189    | [I4QELC](https://gitee.com/openharmony/drivers_peripheral/issues/I4QELC) | 【新增特性】【HDF框架】支持UHDF类进程按需启动                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 190    | [I4QEL5](https://gitee.com/openharmony/usb_manager/issues/I4QEL5) | 【DFX】USB DFX实现                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 191    | [I4QEL6](https://gitee.com/openharmony/usb_manager/issues/I4QEL6) | 【新增特性】USB Port功能实现                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 192 | [I4U5WZ](https://gitee.com/openharmony/ace_ace_engine/issues/I4U5WZ) | 【测试框架】控件树dump能力特性增强                           | 标准系统               | SIG_ApplicationFramework          | [@zcdqs](https://gitee.com/zcdqs) |
| 193 | [I4U5XM](https://gitee.com/openharmony/ace_ace_engine/issues/I4U5XM) | 【新增规格】ACE Loader支持JS文件条件编译能力                 | 标准系统               | SIG_ApplicationFramework          | [@qieqiewl](https://gitee.com/qieqiewl) |
| 194 | [I4U0L8](https://gitee.com/openharmony/communication_ipc/issues/I4U0L8) | 【SysCap】分布式软总线子系统支持SysCap机制                   | 标准系统               | SIG_SoftBus                       | [@Xi_Yuhao](https://gitee.com/Xi_Yuhao) |
| 196 | [I4WVOC](https://gitee.com/openharmony/distributed_hardware_fwk/issues/I4WVOC) | 【新增特性】分布式硬件使能/去使能                   | 标准系统               | SIG_DistributedHardwareManagement                       | [@hwzhangchuang](https://gitee.com/hwzhangchuang) |
| 197 | [I4WVO0](https://gitee.com/openharmony/distributed_hardware_fwk/issues/I4WVO0) | 【新增特性】分布式硬件部件配置和动态加载                   | 标准系统               | SIG_DistributedHardwareManagement                       | [@hwzhangchuang](https://gitee.com/hwzhangchuang) |
| 198 | [I4WVNF](https://gitee.com/openharmony/distributed_hardware_fwk/issues/I4WVNF) | 【新增特性】分布式硬件上下线管理                   | 标准系统               | SIG_DistributedHardwareManagement                       | [@hwzhangchuang](https://gitee.com/hwzhangchuang) |
| 199 | [I4WVMX](https://gitee.com/openharmony/distributed_hardware_fwk/issues/I4WVMX) | 【新增特性】分布式硬件接入管理                   | 标准系统               | SIG_DistributedHardwareManagement                       | [@hwzhangchuang](https://gitee.com/hwzhangchuang) |
| 200 | [I4WVLU](https://gitee.com/openharmony/distributed_hardware_fwk/issues/I4WVLU) | 【新增特性】分布式硬件版本管理                   | 标准系统               | SIG_DistributedHardwareManagement                       | [@hwzhangchuang](https://gitee.com/hwzhangchuang) |
| 201 | [I4WTQN](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTQN) | 【新增特性】新增RichText标签                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 202 | [I4WTPV](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTPV) | 【新增规格】Image组件新增分布式图片路径加载能力                          | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 203 | [I4WTQP](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTQP) | 【新增特性】拖拽手势支持跨窗口拖拽特性                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 204 | [I4WTR3](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTR3) | 【新增特性】富文本组件支持                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 205 | [I4WTR4](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTR4) | 【新增特性】分布式状态管理特性                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 206 | [I4WTRC](https://gitee.com/openharmony/ace_ace_engine/issues/I4WTRC) | 【新增特性】组件状态数据分布式迁移支持                           | ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl) |
| 207 | [I4WWRN](https://gitee.com/openharmony/msdp_device_status/issues/I4WWRN) | 【msdp】支持设备皮套开合事件订阅    | 标准系统 | SIG_DistributedHardwareManagement | [@mayunteng_1](https://gitee.com/mayunteng_1)       |
| 208 | [I4WWRR](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWRR) | 【多模】JS API性能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 209 | [I4WWRT](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWRT) | 【多模】注入工具 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 210 | [I4WWRV](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWRV) | 【多模】ANR检测 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 211 | [I4WWTB](https://gitee.com/openharmony/sensors_sensor/issues/I4WWTB) | 【泛sensor】Sensor支持周边依赖 | 标准系统 | SIG_DistributedHardwareManagement | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 212 | [I4WWTB](https://gitee.com/openharmony/sensors_sensor/issues/I4WWTB) | 【泛sensor】Miscdevice支持周边依赖 | 标准系统 | SIG_DistributedHardwareManagement | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 213 | [I4X49M](https://gitee.com/openharmony/customization_enterprise_device_management/issues/I4X49M) | 【新增特性】设备管理应用开发框架 | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 214 | [I4X49T](https://gitee.com/openharmony/customization_enterprise_device_management/issues/I4X49T) | 【新增特性】企业设备管理服务基本能力 | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 215 | [I4X49Z](https://gitee.com/openharmony/customization_enterprise_device_management/issues/I4X49Z) | 【新增特性】设置系统时间 | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 216 | [I4X4A5](https://gitee.com/openharmony/customization_enterprise_device_management/issues/I4X4A5) | 【资料】EDM API资料 | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 217 | [I4X4AF](https://gitee.com/openharmony/applications_admin_provisioning/issues/I4X4AF) | 【新增特性】DA业务发放能力 | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 218 | [I4XGLQ](https://gitee.com/openharmony-sig/wukong/issues/I4XGLQ) | 【新增特性】UI随机压测工具 | 标准系统 | SIG_WuKong          | [@dwq](https://gitee.com/currydavids)         |
| 219 | [I4R30D](https://gitee.com/openharmony/windowmanager/issues/I4R30D) | 【window_manager】【新增特性】应用主窗口支持自由窗口显示 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 220 | [I4R30F](https://gitee.com/openharmony/windowmanager/issues/I4R30F) | 【window_manager】【新增特性】应用主窗口支持自由窗口显示 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 221 | [I4R9OT](https://gitee.com/openharmony/windowmanager/issues/I4R9OT) | 【window_manager】【新增特性】支持窗口装饰 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 222 | [I4R9OZ](https://gitee.com/openharmony/windowmanager/issues/I4R9OZ) | 【window_manager】【新增规格】增强特性 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 223 | [I4R9P0](https://gitee.com/openharmony/windowmanager/issues/I4R9P0) | 【window_manager】【新增规格】增强特性：提供Display管理能力 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 224 | [I4R9P2](https://gitee.com/openharmony/windowmanager/issues/I4R9P2) | 【window_manager】【新增规格】增强特性：提供Display管理能力 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 225 | [I4R302](https://gitee.com/openharmony/windowmanager/issues/I4R302) | 【window_manager】【新增特性】支持窗口效果 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 226 | [I4R303](https://gitee.com/openharmony/windowmanager/issues/I4R303) | 【window_manager】【新增特性】支持窗口动画 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 227 | [I4R308](https://gitee.com/openharmony/windowmanager/issues/I4R308) | 【window_manager】【新增规格】增强特性：提供应用窗口创建管理能力 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 228 | [I4R30B](https://gitee.com/openharmony/windowmanager/issues/I4R30B) | 【window_manager】【新增规格】增强特性：提供应用窗口创建管理能力 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 229 | [I4R30H](https://gitee.com/openharmony/windowmanager/issues/I4R30H) | 【window_manager】【新增特性】应用主窗口支持自由窗口显示 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 230 | [I4RKT5](https://gitee.com/openharmony/graphic_standard/issues/I4RKT5) | 【render_service部件】【新增特性】新增RenderService支持自绘制通路特性 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 231 | [I4ZEL9](https://gitee.com/openharmony/windowmanager/issues/I4ZEL9) | 【window_manager】【新增规格】增强特性：提供Display管理能力 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 232 | [I4ZEIW](https://gitee.com/openharmony/windowmanager/issues/I4ZEIW) | 【window_manager】【新增特性】支持亮屏灭屏流程 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 233 | [I4R9P9](https://gitee.com/openharmony/graphic_standard/issues/I4R9P9) | 【animation部件】支持窗口动画 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 324 | [I4RKT3](https://gitee.com/openharmony/graphic_standard/issues/I4RKT3) | 【composer部件】提供合成和送显的能力 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 235 | [I4R9PA](https://gitee.com/openharmony/graphic_standard/issues/I4R9PA) | 【composer部件】【新增特性 显示设备管理】vsync管理 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 236 | [I4RKSP](https://gitee.com/openharmony/graphic_standard/issues/I4RKSP) | 【effect部件】【新增特性 Render Effect】提供窗口级非实时模糊特效 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 237 | [I4RKSN](https://gitee.com/openharmony/graphic_standard/issues/I4RKSN) | 【effect部件】【新增特性 Image Effect】提供饱和度，对比度，亮度算法组件 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 238 | [I4RKSO](https://gitee.com/openharmony/graphic_standard/issues/I4RKSO) | 【effect部件】【新增特性Image Effect】提供图片效果处理框架 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |

## OpenHarmony 3.1.5.2版本特性清单：

| no   | issue                                                        | feture description                           | platform     | sig                      | owner                                                 |
| :--- | ------------------------------------------------------------ | :------------------------------------------- | :----------- | :----------------------- | :---------------------------------------------------- |
| 1    | [I4OGLE](https://gitee.com/openharmony/powermgr_battery_statistics/issues/I4OGLE) | 【资料】batterystatistic部件资料需求         | 标准系统     | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 2    | [I40IRO](https://gitee.com/openharmony/powermgr_power_manager/issues/I40IRO) | 【系统电源管理服务】 支持休眠和混合睡眠模式  | 标准系统     | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 3    | [I4OGD4](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGD4?from=project-issue) | 【资料】跨设备任务迁移新增/增强特性资料说明  | 标准系统     | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 4    | [I4OGCZ](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCZ?from=project-issue) | 【资料】跨设备组件oncall调用新增特性资料说明 | 标准系统     | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 5    |[I4PCLE](https://gitee.com/openharmony/aafwk_standard/issues/I4PCLE)|【新增特性】应用环境创建和管理优化|元能力子系统|SIG_ApplicationFramework|[@silent-dye](https://gitee.com/silent-dye)|
| 6    |[I4PCLJ](https://gitee.com/openharmony/aafwk_standard/issues/I4PCLJ)|【新增特性】应用启动功耗优化|元能力子系统|SIG_ApplicationFramework|[@silent-dye](https://gitee.com/silent-dye)|
| 7    |[I4PCLU](https://gitee.com/openharmony/aafwk_standard/issues/I4PCLU)|【新增特性】对外接口性能验收|元能力子系统|SIG_ApplicationFramework|[@silent-dye](https://gitee.com/silent-dye)|
| 8    |[I4PCMC](https://gitee.com/openharmony/aafwk_standard/issues/I4PCMC)|【新增特性】应用启动性能优化|元能力子系统|SIG_ApplicationFramework|[@silent-dye](https://gitee.com/silent-dye)|
| 9    | [I4PKY4](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKY4) | 【showcase特性】DBMS启动与退出               | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 10   | [I4PKY5](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKY5) | 【showcase特性】DBMS异常退出的恢复           | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 11   | [I4PKY6](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKY6) | 【showcase特性】跨设备获取icon和label        | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 12   | [I4PKY9](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKY9) | 【资料】包命令行工具资料说明                 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 13   | [I4PKYC](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYC) | 【资料】config.json配置文件字段说明          | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 14   | [I4PKYJ](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYJ) | 【资料】提供包管理服基本功能的资料说明       | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 15  | [I4Q9SU](https://gitee.com/openharmony/kernel_liteos_m/issues/I4Q9SU)  | [内核子系统]【增强特性】支持北向接口融合   | 内核子系统 | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5)       |
| 16    | [I4QEL3](https://gitee.com/openharmony/drivers_framework/issues/I4QEL3) | 【资料】驱动子系统资料刷新                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 17    | [I4QEL4](https://gitee.com/openharmony/usb_manager/issues/I4QEL4) | 【资料】USB子系统文档资料                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 20    | [I4QT3X](https://gitee.com/openharmony/notification_ans_standard/issues/I4QT3X) | 【新增特性】支持日历类及延迟提醒功能 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 21    |[I4RTYQ](https://gitee.com/openharmony/security_device_security_level/issues/I4RTYQ?from=project-issue) |【资料】DSLM部件设备安全等级凭据格式开放和交换协议开放需求| 标准系统 | SIG_Security | [@zhirenx](https://gitee.com/zhirenx) |
| 22    | [I4RGFY](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4RGFY) | 【DataShare】基于ExtensionAbility新框架重构并提供单设备上跨应用数据共享能力                    | 标准系统   | SIG_DataManagement | [@verystone](https://gitee.com/verystone)         |
| 23   | [I4QU0U](https://gitee.com/openharmony/resourceschedule_work_scheduler/issues/I4QU0U) | 【新增特性】延迟任务相关资料文档 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 24   | [I4T9KH](https://gitee.com/openharmony/developtools_hapsigner/issues/I4T9KH)|【资料】PKI应用签名工具资料需求|安全子系统| SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)|
| 25    | [I4UTCF](https://gitee.com/openharmony/startup_init_lite/issues/I4UTCF) | 【新增特性】进程分组及并行启动基础框架   | 标准系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 26    | [I4UTCM](https://gitee.com/openharmony/startup_init_lite/issues/I4UTCM) | 【新增规格】【新增特性】进程分组启动支持整机不同的启动功能   | 标准系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 27 | [I4UUUU](https://gitee.com/openharmony/security_huks/issues/I4UUUU) | 【新增规格】DEMO测试，提供密钥管理全能力集的测试DEMO | 安全子系统 | SIG_Security | [@chaos-liang](https://gitee.com/Chaos-Liang) |
| 28 | [I4TYFR](https://gitee.com/openharmony/security_huks/issues/I4TYFR) | 【新增规格】HUKS在删除应用的情况下，HUKS需要删除相关的密钥数据 | 安全子系统 | SIG_Security | [@chaos-liang](https://gitee.com/Chaos-Liang) |
| 29 | [I4TYFI](https://gitee.com/openharmony/security_huks/issues/I4TYFI) | 【新增规格】HUKS在删除子用户情况下，需要删除相关的密钥数据 | 安全子系统 | SIG_Security | [@chaos-liang](https://gitee.com/Chaos-Liang) |
| 30 |[I4TYFA](https://gitee.com/openharmony/security_huks/issues/I4TYFA) | 【新增规格】HUKS支持密钥应用基于APP UID的访问隔离 | 安全子系统 | SIG_Security | [@chaos-liang](https://gitee.com/Chaos-Liang) |
| 31 | [I4TYF1](https://gitee.com/openharmony/security_huks/issues/I4TYF1) | 【新增规格】HUKS支持key attestation和id attestation. | 安全子系统 | SIG_Security | [@chaos-liang](https://gitee.com/Chaos-Liang) |
| 32 | [I4TYEM](https://gitee.com/openharmony/security_huks/issues/I4TYEM)| 【新增规格】HUKS支持安全等级凭据的导入签发及验证 | 安全子系统 | SIG_Security | [@chaos-liang](https://gitee.com/Chaos-Liang) |
| 33   | [I4H4FR](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4H4FR)|【distributed_kv_store】支持多用户数据隔离和共享|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 34   | [I4RGFY](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4RGFY)|【DataShare】基于ExtensionAbility新框架重构并提供单设备上跨应用数据共享能力|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 35   | [I4UZJ6](https://gitee.com/openharmony/distributeddatamgr_objectstore/issues/I4UZJ6)|【资料】提供分布式数据对象能力资料跟踪需求|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 36   | [I4UZK0](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4UZK0)|【资料】RDB提供分布式关系型数据库|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 37   | [I4UZL4](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4UZL4)|【资料】data_share提供对数据访问方式的控制|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 38   | [I4V038](https://gitee.com/openharmony/security_access_token/issues/I4V038)  | 【新增规格】实现通过应用权限管理界面设置应用权限    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 39   | [I4V032](https://gitee.com/openharmony/security_access_token/issues/I4V032)  | 【新增规格】应用权限权限管理界面实现    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 40   | [I4V02Y](https://gitee.com/openharmony/security_access_token/issues/I4V02Y)  | 【新增规格】主体设备上应用卸载时同步    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 41   | [I4V02P](https://gitee.com/openharmony/security_access_token/issues/I4V02P)  | 【DFX】Token信息的dump机制    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 42   | [I4V02K](https://gitee.com/openharmony/security_access_token/issues/I4V02K)  | 【新增规格】主体设备上应用授权状态更新同步    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 43   | [I4TYDA](https://gitee.com/openharmony/security_access_token/issues/I4TYDA)  | 【新增规格】token信息跨设备同步    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 44   | [I4TYCV](https://gitee.com/openharmony/security_access_token/issues/I4TYCV)  | 【新增规格】设备上线时的native进程的token信息同步    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 45   | [I4V03D](https://gitee.com/openharmony/security_selinux/issues/I4V03D)  | 【新增特性】支持SELinux开关的dump机制    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 46   | [I4JBF2](https://gitee.com/openharmony/account_os_account/issues/I4JBF2)  | [账号子系统]os_account_standard部件分布式组网账号管理需求    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 47   | [I4IU5W](https://gitee.com/openharmony/account_os_account/issues/I4IU5W)  | [帐号子系统]os_account_standard部件本地多用户生命周期管理需求    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 48   | [I4V74F](https://gitee.com/openharmony/communication_dsoftbus/issues/I4V74F)  | 【新增特性】【组网】拓扑管理    | 软总线子系统 | SIG_SoftBus | [@fanxiaoyu321](https://gitee.com/fanxiaoyu321)       |
| 49   | [I4Q6B5](https://gitee.com/openharmony/hiviewdfx_hitrace/issues/I4Q6B5)|【SR000GIILK:【资料】hitrace部件的js api需求|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 50   | [I4PJE4](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4PJE4)|【SR000GI6P9:【资料】hiprofiler部件资料需求|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 51 | [I4WW3H](https://gitee.com/openharmony/distributed_camera/issues/I4WW3H) | 【新增特性】支持分布式Camera使能和去使能 | 标准系统 | SIG_DistributedHardwareManagement | [@hwzhangchuang](https://gitee.com/hwzhangchuang) |
| 52 | [I4WVZS](https://gitee.com/openharmony/distributed_camera/issues/I4WVZS) | 【新增特性】支持通过分布式相机进行图像预览 | 标准系统 | SIG_DistributedHardwareManagement | [@hwzhangchuang](https://gitee.com/hwzhangchuang) |
| 53 | [I4WVXW](https://gitee.com/openharmony/distributed_camera/issues/I4WVXW) | 【新增特性】支持基于HDF实现分布式相机驱动 | 标准系统 | SIG_DistributedHardwareManagement | [@hwzhangchuang](https://gitee.com/hwzhangchuang) |
| 54 | [I4WVW9](https://gitee.com/openharmony/distributed_camera/issues/I4WVW9) | 【新增特性】主控端相机控制参数支持传递到被控端生效 | 标准系统 | SIG_DistributedHardwareManagement | [@hwzhangchuang](https://gitee.com/hwzhangchuang) |
| 55 | [I4WVVK](https://gitee.com/openharmony/distributed_camera/issues/I4WVVK) | 【新增特性】支持通过分布式相机进行拍照 | 标准系统 | SIG_DistributedHardwareManagement | [@hwzhangchuang](https://gitee.com/hwzhangchuang) |
| 56 | [I4WWS6](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWS6) | 【多模】trace功能 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 57 | [I4WWTB](https://gitee.com/openharmony/sensors_sensor/issues/I4WWTB) | 【资料】JS API资料 | 标准系统 | SIG_DistributedHardwareManagement | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 58 | [I4WWS7](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWS7) | 【多模】inputMonitor JS API资料 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 59 | [I4WWS8](https://gitee.com/openharmony/multimodalinput_input/issues/I4WWS8) | 【多模】inputDevice JS API资料 | 标准系统 | SIG_DistributedHardwareManagement | [@hhh2](https://gitee.com/hhh2) |
| 60 | [I4WVOX](https://gitee.com/openharmony/distributed_hardware_fwk/issues/I4WVOX) | 【新增特性】分布式硬件管理资料                   | 标准系统               | SIG_DistributedHardwareManagement                       | [@hwzhangchuang](https://gitee.com/hwzhangchuang) |
| 61 | [I4ZCG0](https://gitee.com/openharmony/graphic_standard/issues/I4ZCG0) | 【backstore部件】提供SurfaceID以及工具类 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 62 | [I4ZCGG](https://gitee.com/openharmony/graphic_standard/issues/I4ZCGG) | 【drawing部件】提供图形NDK能力 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 63 | [I4ZCGC](https://gitee.com/openharmony/graphic_standard/issues/I4ZCGC) | 【广色域】【新增特性 图形支持广色域】提供图层，窗口的色彩管理 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 64 | [I4R9P6](https://gitee.com/openharmony/graphic_standard/issues/I4R9P6) | 【广色域】【新增特性 图形支持广色域】渲染支持色彩管理 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 65 | [I4R9P7](https://gitee.com/openharmony/graphic_standard/issues/I4R9P7) | 【广色域】【新增特性 图形支持广色域】投屏,合成支持色彩管理 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 66 | [I4R9P8](https://gitee.com/openharmony/graphic_standard/issues/I4R9P8) | 【广色域】【新增特性 图形支持广色域】提供色域描述，定义和单点色域转换工具类 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |

## OpenHarmony 3.1.5.3版本特性清单：

| no   | issue                                                        | feture description                                     | platform | sig                | owner                                         |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------- | :------- | :----------------- | :-------------------------------------------- |
| 1    | [I4P7F7](https://gitee.com/openharmony/third_party_musl/issues/I4P7F7) | [语言编译运行时子系统]提供Windows，linux，Mac多平台NDK | 标准系统 | SIG_CompileRuntime | [@huanghuijin](https://gitee.com/huanghuijin) |
| 2  | [I4Q9F2](https://gitee.com/openharmony/kernel_liteos_m/issues/I4Q9F2)  | [内核子系统]【新增特性】支持native动态加载机制   | 内核子系统 | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5)       |
| 3  | [I4Q9OQ](https://gitee.com/openharmony/kernel_liteos_m/issues/I4Q9OQ)  | [内核子系统]【新增特性】支持Cortex-M55  | 内核子系统 | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5)       |
| 4    | [I4QEL7](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEL7) | 【新增特性】EDP、HDMI屏幕支持；支持显示设备热插拔事件上报；支持查询显示设备相关信息；支持修改显示分辨率                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 5    | [I4QEL8](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEL8) | 【新增特性】Audio支持录音缓存阈值上报、设备加载成功后事件上报                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 6    | [I4U76A](https://gitee.com/openharmony/drivers_peripheral/issues/I4U76A) | 【新增特性】基于HDF驱动框架提供计步器Sensor驱动能力                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 7    | [I4QT45](https://gitee.com/openharmony/resourceschedule_device_usage_statistics/issues/I4QT45) | 【新增特性】查询指定时间范围内的应用使用历史统计数据 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 8    | [I4QT46](https://gitee.com/openharmony/resourceschedule_device_usage_statistics/issues/I4QT46) | 【新增特性】查询指定时间范围内的应用事件集合 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 9    | [I4QU0G](https://gitee.com/openharmony/resourceschedule_device_usage_statistics/issues/I4QU0G) | 【新增特性】查询应用程序当前是否不常用应用 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 10    | [I4QU0I](https://gitee.com/openharmony/resourceschedule_device_usage_statistics/issues/I4QU0I) | 【新增特性】采集数据处理 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 11    | [I4QU0K](https://gitee.com/openharmony/resourceschedule_device_usage_statistics/issues/I4QU0K) | 【新增特性】配置信息变更更新 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 12    | [I4QU0M](https://gitee.com/openharmony/resourceschedule_device_usage_statistics/issues/I4QU0M) |  资料专项 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 13    | [I4UTC6](https://gitee.com/openharmony/startup_init_lite/issues/I4UTC6) | 【新增规格】系统进程运行环境沙盒构建   | 标准系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 14    | [I4UTCO](https://gitee.com/openharmony/startup_init_lite/issues/I4UTCO) | 【增强特性】支持app进程孵化能力增强   | 标准系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 15    | [I4UTCY](https://gitee.com/openharmony/startup_init_lite/issues/I4UTCY) | 【增强特性】appspawn支持孵化的应用进程回收   | 标准系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 16    | [I4UTD0](https://gitee.com/openharmony/startup_init_lite/issues/I4UTD0) | 【资料】init部件南向文档需求   | 标准系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 17    | [I4U27N](https://gitee.com/openharmony/global_resmgr_standard/issues/I4U27N) | 【新增特性】提供获取RawFileDescriptor的js接口                                     | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 18   | [I4VMGZ](https://gitee.com/openharmony/frame_aware_sched/issues/I4VMGZ)  | [内核子系统]【新增特性】支持基础FPS智能感知调度功能     | 内核子系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 19   | [I4U089](https://gitee.com/openharmony/kernel_linux_5.10/issues/I4U089)  | [内核子系统]【新增特性】内核调度支持绘帧线程优先供给机制     | 内核子系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 20 | [I4V226](https://gitee.com/openharmony/docs/issues/I4V226) | 【资料】HUKS密钥管理开发指南和API接口说明 | 安全子系统 | SIG_Security | [@chaos-liang](https://gitee.com/Chaos-Liang) |
| 21   | [I4W1E4](https://gitee.com/openharmony/security_access_token/issues/I4W1E4)  | 【性能】AccessToken性能需求    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 22   | [I4W1E0](https://gitee.com/openharmony/security_access_token/issues/I4W1E0)  | 【资料】AccessToken资料需求    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 23 | [I4W7ZO](https://gitee.com/openharmony/ark_js_runtime/issues/I4W7ZO?from=project-issue) | [语言编译器运行时]【新增特性】Runtime性能调优 | 标准系统 | SIG_CompileRuntime | [@huanghuijin](https://gitee.com/huanghuijin) |
| 24 | [I4W7ZR](https://gitee.com/openharmony/ark_js_runtime/issues/I4W7ZR?from=project-issue) | [语言编译器运行时]【新增规格】内存管理分配回收功能/HPP GC性能调优 | 标准系统 | SIG_CompileRuntime | [@huanghuijin](https://gitee.com/huanghuijin) |
| 25 | [I4WWDA](https://gitee.com/openharmony/ark_js_runtime/issues/I4WWDA?from=project-issue) | [语言编译器运行时]【资料】语言编译器运行时资料刷新 | 标准系统 | SIG_CompileRuntime | [@huanghuijin](https://gitee.com/huanghuijin) |
| 26 | [I4WWHD](https://gitee.com/openharmony/ark_js_runtime/issues/I4WWHD?from=project-issue) | [语言编译器运行时]【新增特性】TS benchmark 的设计与实现 | 标准系统 | SIG_CompileRuntime | [@huanghuijin](https://gitee.com/huanghuijin) |
| 27 | [I4WWKK](https://gitee.com/openharmony/ark_js_runtime/issues/I4WWKK?from=project-issue) | [语言编译器运行时]【增强特性】Actor轻量化1.0 | 标准系统 | SIG_CompileRuntime | [@huanghuijin](https://gitee.com/huanghuijin) |
| 28 | [I4WWMK](https://gitee.com/openharmony/ark_js_runtime/issues/I4WWMK?from=project-issue) | [语言编译器运行时]【资料】TS/JS语言基础库资料刷新 | 标准系统 | SIG_CompileRuntime | [@huanghuijin](https://gitee.com/huanghuijin) |
| 29 | [I4WW76](https://gitee.com/openharmony/distributed_screen/issues/I4WW76) | 【新增特性】支持分布式Screen的使能和去使能 | 标准系统 | SIG_DistributedHardwareManagement | [@hwzhangchuang](https://gitee.com/hwzhangchuang) |
| 30 | [I4WW6U](https://gitee.com/openharmony/distributed_screen/issues/I4WW6U) | 【新增特性】支持分布式Screen的镜像/扩展显示 | 标准系统 | SIG_DistributedHardwareManagement | [@hwzhangchuang](https://gitee.com/hwzhangchuang) |
| 31 | [I4WW60](https://gitee.com/openharmony/distributed_screen/issues/I4WW60) | 【新增特性】支持分布式Screen在被控端的显示 | 标准系统 | SIG_DistributedHardwareManagement | [@hwzhangchuang](https://gitee.com/hwzhangchuang) |
| 32   | [I4X2EL](https://gitee.com/openharmony/accessibility/issues/I4X2EL) | 【部件化专项】无障碍软件服务子系统部件标准化    | 标准系统 | SIG_BasicSoftwareService | [@dubingjian](https://gitee.com/bj1010) |
| 33   | [I4X2EM](https://gitee.com/openharmony/accessibility/issues/I4X2EM) | 【新增特性 信息交换机制】按键拦截               | 标准系统 | SIG_BasicSoftwareService | [@dubingjian](https://gitee.com/bj1010) |
| 34   | [I4X2EN](https://gitee.com/openharmony/accessibility/issues/I4X2EN) | 【新增特性 信息交换机制】支持上报窗口节点信息   | 标准系统 | SIG_BasicSoftwareService | [@dubingjian](https://gitee.com/bj1010) |
| 35   | [I4X2EO](https://gitee.com/openharmony/accessibility/issues/I4X2EO) | 【新增特性 无障碍服务管理】目标应用连接         | 标准系统 | SIG_BasicSoftwareService | [@dubingjian](https://gitee.com/bj1010) |
| 36   | [I4X2EP](https://gitee.com/openharmony/accessibility/issues/I4X2EP) | 【新增特性 无障碍服务管理】辅助应用连接         | 标准系统 | SIG_BasicSoftwareService | [@dubingjian](https://gitee.com/bj1010) |
| 37   | [I4X2ER](https://gitee.com/openharmony/accessibility/issues/I4X2ER) | 【新增特性 无障碍服务管理】辅助应用更新         | 标准系统 | SIG_BasicSoftwareService | [@dubingjian](https://gitee.com/bj1010) |
| 38   | [I4X2ES](https://gitee.com/openharmony/accessibility/issues/I4X2ES) | 【新增特性 无障碍服务管理】无障碍字幕配置       | 标准系统 | SIG_BasicSoftwareService | [@dubingjian](https://gitee.com/bj1010) |
| 39   | [I4X2ET](https://gitee.com/openharmony/accessibility/issues/I4X2ET) | 【新增特性 信息交换机制】支持控件节点信息上报   | 标准系统 | SIG_BasicSoftwareService | [@dubingjian](https://gitee.com/bj1010) |
| 40   | [I4X2EV](https://gitee.com/openharmony/accessibility/issues/I4X2EV) | 【新增特性 信息交换机制】焦点查询               | 标准系统 | SIG_BasicSoftwareService | [@dubingjian](https://gitee.com/bj1010) |
| 41   | [I4X2EY](https://gitee.com/openharmony/accessibility/issues/I4X2EY) | 【新增特性 信息交换机制】无障碍事件列表         | 标准系统 | SIG_BasicSoftwareService | [@dubingjian](https://gitee.com/bj1010) |
| 42   | [I4X2EZ](https://gitee.com/openharmony/accessibility/issues/I4X2EZ) | 【新增特性 信息交换机制】无障碍事件信息         | 标准系统 | SIG_BasicSoftwareService | [@dubingjian](https://gitee.com/bj1010) |
| 43   | [I4X2F0](https://gitee.com/openharmony/accessibility/issues/I4X2F0) | 【新增特性 信息交换机制】无障碍动作发起         | 标准系统 | SIG_BasicSoftwareService | [@dubingjian](https://gitee.com/bj1010) |
| 44   | [I4X2F1](https://gitee.com/openharmony/accessibility/issues/I4X2F1) | 【新增特性 信息交换机制】辅助应用列表查询       | 标准系统 | SIG_BasicSoftwareService | [@dubingjian](https://gitee.com/bj1010) |
| 45   | [I4X2F2](https://gitee.com/openharmony/accessibility/issues/I4X2F2) | 【新增特性 信息交换机制】辅助应用状态查询与监听 | 标准系统 | SIG_BasicSoftwareService | [@dubingjian](https://gitee.com/bj1010) |
| 46   | [I4X2F3](https://gitee.com/openharmony/accessibility/issues/I4X2F3) | 【新增特性 信息交换机制】手势模拟               | 标准系统 | SIG_BasicSoftwareService | [@dubingjian](https://gitee.com/bj1010) |
| 47   | [I4X2F4](https://gitee.com/openharmony/accessibility/issues/I4X2F4) | 【新增特性 信息交换机制】触摸拦截               | 标准系统 | SIG_BasicSoftwareService | [@dubingjian](https://gitee.com/bj1010) |
| 48   | [I4X2F6](https://gitee.com/openharmony/accessibility/issues/I4X2F6) | 【SysCap】无障碍软件服务子系统支持SysCap机制    | 标准系统 | SIG_BasicSoftwareService | [@dubingjian](https://gitee.com/bj1010) |
|49|[I4XXCD](https://gitee.com/openharmony/startup_syspara_lite/issues/I4XXCD)|【新增规格】Image组件支持同步、异步渲染设置|标准系统|SIG_BasicSoftwareService|[@handyohos](https://gitee.com/handyohos)|
|50|[I4XXCE](https://gitee.com/openharmony/web_webview/issues/I4XXCE)|【部件化专项】Web子系统部件标准化|标准系统|SIG_ApplicationFramework|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|51|[I4XXCG](https://gitee.com/openharmony/web_webview/issues/I4XXCG)|【资料】Web子系统JS接口说明|标准系统|SIG_ApplicationFramework|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|52|[I4XXCH](https://gitee.com/openharmony/web_webview/issues/I4XXCH)|【新增特性】应用内支持Web页面的加载和显示|标准系统|SIG_ApplicationFramework|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|53|[I4XXCI](https://gitee.com/openharmony/web_webview/issues/I4XXCI)|【新增特性】应用可设置Web页面状态的监听和回调|标准系统|SIG_ApplicationFramework|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|54|[I4XXCJ](https://gitee.com/openharmony/web_webview/issues/I4XXCJ)|【新增特性】应用可配置Web引擎的工作参数|标准系统|SIG_ApplicationFramework|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|55|[I4XXCK](https://gitee.com/openharmony/build_lite/issues/I4XXCK)|【syscap_codec部件】SysCap编解码工具|标准系统|SIG_CompileRuntime|[@wangxing-hw](https://gitee.com/wangxing-hw)|
|56|[I4XXCM](https://gitee.com/openharmony/test_developertest/issues/I4XXCM)|【测试工具】需要扩展对三方N设备的支持|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|57|[I4XXCN](https://gitee.com/openharmony/test_developertest/issues/I4XXCN)|【测试工具】开关机压力测试工具|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|58|[I4XXCO](https://gitee.com/openharmony/test_developertest/issues/I4XXCO)|【测试工具】休眠唤醒压力测试工具|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|59|[I4XXCQ](https://gitee.com/openharmony/test_developertest/issues/I4XXCQ)|【测试框架】UI测试框架测试资料|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|60|[I4XXCR](https://gitee.com/openharmony/test_developertest/issues/I4XXCR)|【测试框架】界面自动化测试|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|61|[I4XXCS](https://gitee.com/openharmony/test_developertest/issues/I4XXCS)|【测试框架】开发者测试框架资料|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|62|[I4XXCV](https://gitee.com/openharmony/test_developertest/issues/I4XXCV)|【测试框架】TS开发者测试框架|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|63|[I4XXCW](https://gitee.com/openharmony/test_developertest/issues/I4XXCW)|【测试框架】JS应用开发者测试框架|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|64|[I4XXCX](https://gitee.com/openharmony/test_developertest/issues/I4XXCX)|【测试框架】测试流水线测试套执行报表|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|65|[I4XXCY](https://gitee.com/openharmony/test_developertest/issues/I4XXCY)|【测试框架】执行测试用例|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|66|[I4XXCZ](https://gitee.com/openharmony/test_developertest/issues/I4XXCZ)|【测试框架】用例配置管理|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|67|[I4XXD0](https://gitee.com/openharmony/test_developertest/issues/I4XXD0)|【测试框架】执行器设备管理|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|68|[I4XXD1](https://gitee.com/openharmony/test_developertest/issues/I4XXD1)|【测试框架】分布式测试框架资料|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|69|[I4XXD2](https://gitee.com/openharmony/test_developertest/issues/I4XXD2)|【测试框架】TS/JS 分布式测试框架|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|70|[I4XXD3](https://gitee.com/openharmony/test_developertest/issues/I4XXD3)|【测试框架】C++分布式接口测试|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|71|[I4XXD4](https://gitee.com/openharmony/test_developertest/issues/I4XXD4)|【CERTMGR】支持以OH凭据到OH认证云进行端云验证|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|72|[I4XXD6](https://gitee.com/openharmony/test_developertest/issues/I4XXD6)|【认证测试】HATS3.1 HDF&HDI兼容性测试套件|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|73|[I4XXD7](https://gitee.com/openharmony/test_developertest/issues/I4XXD7)|【认证测试】DCTS3.1分布式兼容性测试套件|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|74|[I4XXD8](https://gitee.com/openharmony/test_developertest/issues/I4XXD8)|【cert_mgr_platform】资源管理|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|75|[I4XXDA](https://gitee.com/openharmony/test_developertest/issues/I4XXDA)|【cert_mgr_platform】字典管理|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|76|[I4XXDB](https://gitee.com/openharmony/test_developertest/issues/I4XXDB)|【cert_mgr_platform】发证管理|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|77|[I4XXDD](https://gitee.com/openharmony/test_developertest/issues/I4XXDD)|【cert_mgr_platform】公示管理|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|78|[I4XXDE](https://gitee.com/openharmony/test_developertest/issues/I4XXDE)|【cert_mgr_platform】认证度量|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|79|[I4XXDF](https://gitee.com/openharmony/test_developertest/issues/I4XXDF)|【cert_mgr_platform】Token申请与Token管理|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|80|[I4XXDH](https://gitee.com/openharmony/test_developertest/issues/I4XXDH)|【cert_mgr_platform】企业空间管理|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|81|[I4XXDI](https://gitee.com/openharmony/test_developertest/issues/I4XXDI)|【cert_mgr_platform】认证提交|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|82|[I4XXDJ](https://gitee.com/openharmony/test_developertest/issues/I4XXDJ)|【cert_mgr_platform】认证进度和认证结果查询|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|83|[I4XXDL](https://gitee.com/openharmony/test_developertest/issues/I4XXDL)|【cert_mgr_platform】问题反馈&问题处理|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|84|[I4XXDM](https://gitee.com/openharmony/test_developertest/issues/I4XXDM)|【cert_mgr_platform】认证审核|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|85|[I4XXDN](https://gitee.com/openharmony/test_developertest/issues/I4XXDN)|【cert_mgr_platform】OHCA协议签署与OHCA协议管理|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|86|[I4XXDO](https://gitee.com/openharmony/test_developertest/issues/I4XXDO)|【cert_mgr_platform】企业用户信息维护|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|87|[I4XXDP](https://gitee.com/openharmony/test_developertest/issues/I4XXDP)|【cert_mgr_platform】用户权限配置|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|88|[I4XXDR](https://gitee.com/openharmony/test_developertest/issues/I4XXDR)|【cert_mgr_platform】用户登陆|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|89|[I4XXDS](https://gitee.com/openharmony/test_developertest/issues/I4XXDS)|【cert_mgr_platform】OpenHarmony官网统一帐号支持兼容性认证账号登录和认证管理|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|90|[I4XXDU](https://gitee.com/openharmony/test_developertest/issues/I4XXDU)|【cert_mgr_platform】OpenHarmony官网兼容性认证结果公示和宣传专区|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|91|[I4XXDV](https://gitee.com/openharmony/test_developertest/issues/I4XXDV)|【cert_mgr_platform】OpenHarmony官网PCS规范/XTS套件/认证指导/授标规范发布和管理专区|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|92|[I4XXDX](https://gitee.com/openharmony/test_developertest/issues/I4XXDX)|【认证测试】XTS执行框架支持组件化配置(xdevice)|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|93|[I4XXDY](https://gitee.com/openharmony/test_developertest/issues/I4XXDY)|【认证测试】xts测试套件支持按部件构建挑选|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|94|[I4XXDZ](https://gitee.com/openharmony/test_developertest/issues/I4XXDZ)|【认证测试】测试框架支持部件化编译|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|95|[I4XXE1](https://gitee.com/openharmony/test_developertest/issues/I4XXE1)|【pcs】新增分布式文件兼容性规范|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|96|[I4XXE2](https://gitee.com/openharmony/test_developertest/issues/I4XXE2)|【pcs】新增分布式通知兼容性规范|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|97|[I4XXE4](https://gitee.com/openharmony/test_developertest/issues/I4XXE4)|【pcs】更新应用包结构兼容性规范|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|98|[I4XXE5](https://gitee.com/openharmony/test_developertest/issues/I4XXE5)|【pcs】更新兼容性测试规范|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|99|[I4XXE6](https://gitee.com/openharmony/test_developertest/issues/I4XXE6)|【pcs】新增SysCap和PCID兼容性规范|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|100|[I4XXE8](https://gitee.com/openharmony/test_developertest/issues/I4XXE8)|【pcs】更新mini/small/standard基础系统类型API最小集|标准系统|SIG_Test|[@buranfanchen](https://gitee.com/buranfanchen)|
|101|[I4XXEA](https://gitee.com/openharmony/communication_dsoftbus/issues/I4XXEA)|【部件化专项】分布式软总线子系统部件标准化|标准系统|SIG_SoftBus|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|102|[I4RKT7](https://gitee.com/openharmony/graphic_standard/issues/I4RKT7)|【DFX】增加图形栈基础维测能力，看护图形系统性能指标|标准系统|SIG-Graphics|[@lz-230](https://gitee.com/lz-230)|
|103|[I4XXED](https://gitee.com/openharmony/communication_dsoftbus/issues/I4XXED)|【RPC】RPC支持基础数据类型序列化/反序列化|标准系统|SIG_SoftBus|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|104|[I4XXEF](https://gitee.com/openharmony/communication_dsoftbus/issues/I4XXEF)|【RPC】支持RPC对象跨进程序列化|标准系统|SIG_SoftBus|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|105|[I4XXEL](https://gitee.com/openharmony/communication_dsoftbus/issues/I4XXEL)|【增强特性】软总线支持蓝牙|标准系统|SIG_SoftBus|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|106|[I4XXEN](https://gitee.com/openharmony/communication_dsoftbus/issues/I4XXEN)|【新增特性】【组网】软总线组网支持P2P连接|标准系统|SIG_SoftBus|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|107|[I4XXEO](https://gitee.com/openharmony/communication_dsoftbus/issues/I4XXEO)|【增强特性】【传输】文件传输增强（NSTACK组件能力）|标准系统|SIG_SoftBus|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|108|[I4XXEP](https://gitee.com/openharmony/communication_dsoftbus/issues/I4XXEP)|【新增特性】【连接】软总线支持P2P连接|标准系统|SIG_SoftBus|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|109|[I4IEDX](https://gitee.com/openharmony/communication_dsoftbus/issues/I4IEDX)|【RPC】RPC支持基础数据类型序列化/反序列化SR000GJUUQ|标准系统|SIG_SoftBus|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|110|[I4XXER](https://gitee.com/openharmony/communication_dsoftbus/issues/I4XXER)|【新增特性】【组网】软总线提供组网ExtAPI接口|标准系统|SIG_SoftBus|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|111|[I4XXES](https://gitee.com/openharmony/communication_dsoftbus/issues/I4XXES)|【新增特性】【传输】软总线支持P2P文件传输|标准系统|SIG_SoftBus|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|112|[I4XXET](https://gitee.com/openharmony/communication_dsoftbus/issues/I4XXET)|【新增特性】【传输】软总线支持P2P流传输|标准系统|SIG_SoftBus|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|113|[I4XXEV](https://gitee.com/openharmony/communication_dsoftbus/issues/I4XXEV)|【新增特性】【传输】流传输增强（NSTACK组件能力增强）|标准系统|SIG_SoftBus|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|114|[I4XXEX](https://gitee.com/openharmony/communication_dsoftbus/issues/I4XXEX)|【传输】文件传输（蓝牙)|标准系统|SIG_SoftBus|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|115|[I4XXEY](https://gitee.com/openharmony/communication_dsoftbus/issues/I4XXEY)|【增强特性】软总线支持获取设备名称|标准系统|SIG_SoftBus|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|116|[I3NIN8](https://gitee.com/openharmony/communication_dsoftbus/issues/I3NIN8)|【RPC】支持RPC对象跨进程序列化SR000FLG|标准系统|SIG_SoftBus|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|117|[I4XXF0](https://gitee.com/openharmony/communication_wifi/issues/I4XXF0)|【部件化专项】分布式软总线(基础通信)子系统WIFI部件标准化|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|118|[I4XXF3](https://gitee.com/openharmony/communication_nfc/issues/I4XXF3)|【部件化专项】分布式软总线(基础通信)子系统NFC部件标准化|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|119|[I4XXF4](https://gitee.com/openharmony/communication_bluetooth/issues/I4XXF4)|【部件化专项】分布式软总线(基础通信)子系统蓝牙部件标准化|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|120|[I4XXF5](https://gitee.com/openharmony/communication_bluetooth/issues/I4XXF5)|【bluetooth_standard】提供蓝牙功耗统计以及功耗优化能力|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|121|[I4XXF6](https://gitee.com/openharmony/communication_wifi/issues/I4XXF6)|【新增特性】支持STA基础特性的复杂加密方式|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|122|[I4XXF7](https://gitee.com/openharmony/communication_wifi/issues/I4XXF7)|【新增特性】支持SoftAP 5G及信道自动选择|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|123|[I4XXF8](https://gitee.com/openharmony/communication_wifi/issues/I4XXF8)|【新增特性】支持SoftAP基础特性的JS API接口|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|124|[I4XXF9](https://gitee.com/openharmony/communication_wifi/issues/I4XXF9)|【新增特性】支持SoftAP基础特性的JS API接口资料文档|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|125|[I4XXFB](https://gitee.com/openharmony/communication_wifi/issues/I4XXFB)|【新增特性】支持P2P magiclink连接特性|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|126|[I4XXFC](https://gitee.com/openharmony/communication_wifi/issues/I4XXFC)|【新增特性】支持P2P 基础特性能力的JS API接口|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|127|[I4XXFE](https://gitee.com/openharmony/communication_wifi/issues/I4XXFE)|【新增特性】支持P2P 基础特性能力的JS API接口文档|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|128|[I4XXFF](https://gitee.com/openharmony/communication_wifi/issues/I4XXFF)|【新增特性】提供WiFi P2P基本能力|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|129|[I4XXFG](https://gitee.com/openharmony/communication_wifi/issues/I4XXFG)|【新增特性】支持WiFi功耗统计以及功耗优化能力|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|130|[I4XXFH](https://gitee.com/openharmony/communication_wifi/issues/I4XXFH)|【新增特性】支持WiFi架构与WPA的HDI能力|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|131|[I4XXFK](https://gitee.com/openharmony/communication_wifi/issues/I4XXFK)|【新增特性】支持WiFi架构与驱动的HDI能力|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|132|[I4XXFL](https://gitee.com/openharmony/communication_bluetooth/issues/I4XXFL)|【bluetooth_standard】支持蓝牙SPP能力，提供BR的数据传输能力|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|133|[I4XXFM](https://gitee.com/openharmony/communication_bluetooth/issues/I4XXFM)|【bluetooth_standard】提供蓝牙SPP功能的开发指导手册、API说明文档的资料文档|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|134|[I4XXFN](https://gitee.com/openharmony/communication_bluetooth/issues/I4XXFN)|【bluetooth_standard】支持蓝牙BLE相关的基本能力|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|135|[I4XXFP](https://gitee.com/openharmony/communication_bluetooth/issues/I4XXFP)|【bluetooth_standard】支持蓝牙BLE相关的基本能力|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|136|[I4XXFQ](https://gitee.com/openharmony/communication_bluetooth/issues/I4XXFQ)|【bluetooth_standard】提供HFP profile能力以及相关API|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|137|[I4XXFS](https://gitee.com/openharmony/communication_bluetooth/issues/I4XXFS)|【bluetooth_standard】提供HFP profile能力以及相关API|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|138|[I4XXFT](https://gitee.com/openharmony/communication_bluetooth/issues/I4XXFT)|【bluetooth_standard】提供A2DP profile相关能力以及JS API|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|139|[I4XXFV](https://gitee.com/openharmony/communication_bluetooth/issues/I4XXFV)|【bluetooth_standard】提供A2DP profile相关能力以及JS API|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|140|[I4XXFW](https://gitee.com/openharmony/communication_bluetooth/issues/I4XXFW)|【bluetooth_standard】提供AVRCP profile相关能力以及JS API|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|141|[I4XXFY](https://gitee.com/openharmony/communication_bluetooth/issues/I4XXFY)|【bluetooth_standard】提供AVRCP profile相关能力以及JS API|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|142|[I4XXFZ](https://gitee.com/openharmony/communication_bluetooth/issues/I4XXFZ)|【bluetooth_standard】蓝牙PAN特性，支持通过蓝牙进行网络共享；|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|143|[I4XXG0](https://gitee.com/openharmony/communication_bluetooth/issues/I4XXG0)|【bluetooth_standard】蓝牙PAN特性，支持通过蓝牙进行网络共享；|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|144|[I4XXG1](https://gitee.com/openharmony/communication_bluetooth/issues/I4XXG1)|【bluetooth_standard】提供蓝牙DFX相关能力以及JS API|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|145|[I4XXG4](https://gitee.com/openharmony/communication_nfc/issues/I4XXG4)|【新增特性】提供NFC NDEF 有源标签的读写能力|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|146|[I4XXG5](https://gitee.com/openharmony/communication_bluetooth/issues/I4XXG5)|【资料】有源标签读取JS接口的资料开发|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|147|[I4XXG6](https://gitee.com/openharmony/communication_bluetooth/issues/I4XXG6)|【bluetooth_standard】提供蓝牙文件传输能力|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|148|[I4XXG7](https://gitee.com/openharmony/communication_bluetooth/issues/I4XXG7)|【bluetooth_standard】提供蓝牙文件传输能力|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|149|[I4XXG8](https://gitee.com/openharmony/communication_bluetooth/issues/I4XXG8)|【bluetooth_standard】支持人机接口设备接入能力，如蓝牙键盘、鼠标、游戏手柄等|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|150|[I4XXG9](https://gitee.com/openharmony/communication_bluetooth/issues/I4XXG9)|【bluetooth_standard】支持人机接口设备接入能力，如蓝牙键盘、鼠标、游戏手柄等|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|151|[I4XXGB](https://gitee.com/openharmony/communication_bluetooth/issues/I4XXGB)|【bluetooth_standard】支持蓝牙BR/EDR的基本能力|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|152|[I4XXGC](https://gitee.com/openharmony/communication_bluetooth/issues/I4XXGC)|【bluetooth_standard】支持蓝牙BR/EDR的基本能力|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|153|[I4XXGD](https://gitee.com/openharmony/communication_bluetooth/issues/I4XXGD)|【SysCap】分布式软总线子系统（基础通信）支持SysCap机制|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|154|[I4XXGF](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4XXGF)|【request部件】文件上传功能|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
|155|[I4XXGG](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4XXGG)|【部件化专项】【preference部件】preference部件标准化|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
|156|[I4XXGH](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4XXGH)|【部件化专项】【data_share_ability部件】data_share_ability部件标准化|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
|157|[I4XXGI](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4XXGI)|【部件化专项】【relational_store部件】relational_store部件标准化|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
|158|[I4XXGK](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4XXGK)|【SysCap】分布式数据管理子系统支持SysCap机制|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
|159|[I4XXGN](https://gitee.com/openharmony/distributed_hardware_fwk/issues/I4XXGN)|【SysCap】分布式硬件管理子系统支持SysCap机制|标准系统|SIG_DistributedHardwareManagement|[@hwzhangchuang](https://gitee.com/hwzhangchuang)|
|160|[I4XXGO](https://gitee.com/openharmony/multimedia_utils_lite/issues/I4XXGO)|【资料】提供场景化视频播放JS接口说明|标准系统|SIG_Media|[@zhu-mingliang](https://gitee.com/zhu-mingliang)|
|161|[I4XXGP](https://gitee.com/openharmony/multimedia_utils_lite/issues/I4XXGP)|【资料】提供场景化视频录制JS接口说明|标准系统|SIG_Media|[@zhu-mingliang](https://gitee.com/zhu-mingliang)|
|162|[I4XXGQ](https://gitee.com/openharmony/multimedia_utils_lite/issues/I4XXGQ)|【新增特性】图片位图，解码框架接口对齐API7|标准系统|SIG_Media|[@zhu-mingliang](https://gitee.com/zhu-mingliang)|
|163|[I4XXGR](https://gitee.com/openharmony/multimedia_utils_lite/issues/I4XXGR)|【SysCap】媒体子系统支持SysCap机制|标准系统|SIG_Media|[@zhu-mingliang](https://gitee.com/zhu-mingliang)|
|164|[I4XXGS](https://gitee.com/openharmony/kernel_linux_build/issues/I4XXGS)|【外部依赖】【内核子系统】内核实现进程的tokenID设置|标准系统|SIG_Kernel|[@liuyoufang](https://gitee.com/liuyoufang)|
|165|[I4XXGT](https://gitee.com/openharmony/kernel_linux_build/issues/I4XXGT)|【新增】编译构建/Module rules 编译构建流程适配SoC与Board分离机制。|标准系统|SIG_Kernel|[@leonchan5](https://gitee.com/leonchan5)|
|166|[I4XXGU](https://gitee.com/openharmony/kernel_linux_build/issues/I4XXGU)|【新增】硬件芯片/hisilico跟随SoC与Board分离机制进行调整。|标准系统|SIG_Kernel|[@leonchan5](https://gitee.com/leonchan5)|
|167|[I4XXGV](https://gitee.com/openharmony/startup_appspawn_lite/issues/I4XXGV)|【增强特性】appspawn归一|标准系统|SIG_BasicSoftwareService|[@handyohos](https://gitee.com/handyohos)|
|168|[I4XXGW](https://gitee.com/openharmony/startup_appspawn_lite/issues/I4XXGW)|【部件化专项】启动子系统提供SystemCapability API|标准系统|SIG_BasicSoftwareService|[@handyohos](https://gitee.com/handyohos)|
|169|[I4XXGX](https://gitee.com/openharmony/startup_appspawn_lite/issues/I4XXGX)|【SysCap】启动恢复子系统支持SysCap机制|标准系统|SIG_BasicSoftwareService|[@handyohos](https://gitee.com/handyohos)|
|170|[I4XXGZ](https://gitee.com/openharmony/drivers_adapter/issues/I4XXGZ)|【新增特性】基于HDF驱动框架提供计步器Sensor驱动能力|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|171|[I4XXH2](https://gitee.com/openharmony/global_i18n_standard/issues/I4XXH2)|【SysCap】全球化子系统支持SysCap机制|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|172|[I4XXH6](https://gitee.com/openharmony/update_app/issues/I4XXH6)|【SysCap】升级服务子系统支持SysCap机制|标准系统|SIG_BasicSoftwareService|[@ailorna](https://gitee.com/ailorna)|
|173|[I4XXH7](https://gitee.com/openharmony/notification_ces_standard/issues/I4XXH7)|【SysCap】事件通知子系统支持SysCap机制|标准系统|SIG_BasicSoftwareService|[@autumn330](https://gitee.com/autumn330)|
|174|[I4XXH8](https://gitee.com/openharmony/miscservices_inputmethod/issues/I4XXH8)|【部件化专项】inputmethod部件标准化|标准系统|SIG_BasicSoftwareService|[@zhouyongfei](https://gitee.com/zhouyongfei)|
|175|[I4XXH9](https://gitee.com/openharmony/miscservices_inputmethod/issues/I4XXH9)|【inputmethod部件】inputmethod JS API接口开放|标准系统|SIG_BasicSoftwareService|[@zhouyongfei](https://gitee.com/zhouyongfei)|
|176|[I4XXHA](https://gitee.com/openharmony/miscservices_inputmethod/issues/I4XXHA)|【新增规格】输入法管理服务|标准系统|SIG_BasicSoftwareService|[@zhouyongfei](https://gitee.com/zhouyongfei)|
|177|[I4XXHB](https://gitee.com/openharmony/miscservices_inputmethod/issues/I4XXHB)|【新增规格】输入法开发框架监听系统事件、输入框事件|标准系统|SIG_BasicSoftwareService|[@zhouyongfei](https://gitee.com/zhouyongfei)|
|178|[I4XXHC](https://gitee.com/openharmony/miscservices_inputmethod/issues/I4XXHC)|【新增规格】输入法开发框架支持输入法应用的文本输入|标准系统|SIG_BasicSoftwareService|[@zhouyongfei](https://gitee.com/zhouyongfei)|
|179|[I4XXHE](https://gitee.com/openharmony/miscservices_inputmethod/issues/I4XXHE)|【新增规格】输入法管理服务资料|标准系统|SIG_BasicSoftwareService|[@zhouyongfei](https://gitee.com/zhouyongfei)|
|180|[I4XXHF](https://gitee.com/openharmony/third_party_libdrm/issues/I4XXHF)|【SysCap】图形子系统支持SysCap机制|标准系统|SIG_GraphicsandMedia|[@lz-230](https://gitee.com/lz-230)|
|181|[I4XXHG](https://gitee.com/openharmony/communication_netmanager_base/issues/I4XXHG)|【wpa_supplicant】提供magiclink能力|标准系统|SIG-Telephony|[@clevercong](https://gitee.com/clevercong)|
|182|[I4XXHH](https://gitee.com/openharmony/communication_netmanager_base/issues/I4XXHH)|【wpa_supplicant】提供基于NL80211的wpa、p2p能力。|标准系统|SIG-Telephony|[@clevercong](https://gitee.com/clevercong)|
|183|[I4XXHJ](https://gitee.com/openharmony/communication_netmanager_base/issues/I4XXHJ)|【部件化专项】netmanager_base部件标准化|标准系统|SIG-Telephony|[@clevercong](https://gitee.com/clevercong)|
|184|[I4XXHK](https://gitee.com/openharmony/communication_netmanager_base/issues/I4XXHK)|【部件化专项】netstack部件标准化|标准系统|SIG-Telephony|[@clevercong](https://gitee.com/clevercong)|
|185|[I4XXHL](https://gitee.com/openharmony/communication_netmanager_base/issues/I4XXHL)|【增强特性】提供基础网络连接管理相关native和JS API接口|标准系统|SIG-Telephony|[@clevercong](https://gitee.com/clevercong)|
|186|[I4XXHM](https://gitee.com/openharmony/communication_netmanager_base/issues/I4XXHM)|【资料】网络管理子系统资料新增|标准系统|SIG-Telephony|[@clevercong](https://gitee.com/clevercong)|
|187|[I4XXHN](https://gitee.com/openharmony/communication_netmanager_base/issues/I4XXHN)|【增强特性】支持网络连接状态查询和网络连接状态变化通知|标准系统|SIG-Telephony|[@clevercong](https://gitee.com/clevercong)|
|188|[I4XXHP](https://gitee.com/openharmony/communication_netmanager_base/issues/I4XXHP)|【增强特性】支持DNS解析和配置|标准系统|SIG-Telephony|[@clevercong](https://gitee.com/clevercong)|
|189|[I4XXHR](https://gitee.com/openharmony/communication_netmanager_base/issues/I4XXHR)|【资料】基础网络连接管理部件资料新增|标准系统|SIG-Telephony|[@clevercong](https://gitee.com/clevercong)|
|190|[I4XXHS](https://gitee.com/openharmony/communication_netmanager_base/issues/I4XXHS)|【增强特性】支持对WIFI/蜂窝网络连接的管理和切换|标准系统|SIG-Telephony|[@clevercong](https://gitee.com/clevercong)|
|191|[I4XXHT](https://gitee.com/openharmony/communication_netmanager_base/issues/I4XXHT)|【增强特性】支持http 1.1/https/http2|标准系统|SIG-Telephony|[@clevercong](https://gitee.com/clevercong)|
|192|[I4XXHU](https://gitee.com/openharmony/communication_netmanager_base/issues/I4XXHU)|【增强特性】支持TCP/UDP Socket|标准系统|SIG-Telephony|[@clevercong](https://gitee.com/clevercong)|
|193|[I4XXHV](https://gitee.com/openharmony/base_location/issues/I4XXHV)|【部件化专项】位置服务子系统部件标准化|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|194|[I4XXHW](https://gitee.com/openharmony/base_location/issues/I4XXHW)|【location_gnss】支持GNSS辅助协议|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|195|[I4XXHY](https://gitee.com/openharmony/base_location/issues/I4XXHY)|【location_network】支持网络定位的mock demo能力|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|196|[I4XXHZ](https://gitee.com/openharmony/base_location/issues/I4XXHZ)|【location_network】支持网络定位能力|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|197|[I4XXI0](https://gitee.com/openharmony/base_location/issues/I4XXI0)|【location_geocode】支持经纬度和地址互相转换|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|198|[I4XXI1](https://gitee.com/openharmony/base_location/issues/I4XXI1)|【资料】geocode JS接口的资料开发|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|199|[I4XXI2](https://gitee.com/openharmony/base_location/issues/I4XXI2)|【资料】地理围栏JS接口的资料开发|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|200|[I4XXI3](https://gitee.com/openharmony/base_location/issues/I4XXI3)|【location_geofence】支持GNSS芯片相关的地理围栏功能|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|201|[I4XXI4](https://gitee.com/openharmony/base_location/issues/I4XXI4)|【location_locator】支持位置服务定位管理功能|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|202|[I4XXI5](https://gitee.com/openharmony/base_location/issues/I4XXI5)|【location_locator】支持管理多个定位请求，支持多应用同时请求定位|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|203|[I4XXI6](https://gitee.com/openharmony/base_location/issues/I4XXI6)|【资料】位置服务定位管理JS接口的资料开发|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|204|[I4XXI7](https://gitee.com/openharmony/base_location/issues/I4XXI7)|【location_locator】提供位置服务隐私的安全保障能力|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|205|[I4XXI8](https://gitee.com/openharmony/base_location/issues/I4XXI8)|【location_locator】支持位置服务的安全管理能力|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|206|[I4XXI9](https://gitee.com/openharmony/base_location/issues/I4XXI9)|【location_locator】提供位置服务DFX能力|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|207|[I4XXIA](https://gitee.com/openharmony/base_location/issues/I4XXIA)|【资料】位置服务dump JS接口的资料开发|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|208|[I4XXIB](https://gitee.com/openharmony/base_location/issues/I4XXIB)|【location_gnss】支持GNSS参数设置和信息上报功能|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|209|[I4XXIC](https://gitee.com/openharmony/base_location/issues/I4XXIC)|【location_gnss】提供GNSS Batching的能力|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|210|[I4XXIE](https://gitee.com/openharmony/base_location/issues/I4XXIE)|【资料】GNSS基本定位能力JS接口的资料开发|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|211|[I4XXIF](https://gitee.com/openharmony/base_location/issues/I4XXIF)|【location_gnss】提供GNSS基本定位功能|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|212|[I4XXIG](https://gitee.com/openharmony/base_location/issues/I4XXIG)|【location_gnss】提供GNSS硬件功耗统计以及功耗优化能力|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|213|[I4XXIH](https://gitee.com/openharmony/base_location/issues/I4XXIH)|【location_gnss】供GNSS性能提升的相关辅助信息请求和注入能力|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|214|[I4XXII](https://gitee.com/openharmony/base_location/issues/I4XXII)|【资料】GNSS辅助信息请求和注入JS接口的资料开发|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|215|[I4XXIJ](https://gitee.com/openharmony/base_location/issues/I4XXIJ)|【SysCap】位置服务子系统支持SysCap机制|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|216|[I4XXIL](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4XXIL)|【部件化专项】文件存储子系统部件标准化|标准系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
|217|[I4XXIM](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4XXIM)|【新增特性】【storage_manager部件】CE/DE 文件软加密策略管理|标准系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
|218|[I4XXIN](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4XXIN)|【新增特性】【storage_manager部件】支持秘钥存储管理|标准系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
|219|[I4XXIO](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4XXIO)|【新增特性】【local_file_system部件】支持ext4/f2fs等用户态工具能力|标准系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
|220|[I4XXIP](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4XXIP)|【新增特性】响应多用户创建删除，进行用户目录创建和删除|标准系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
|221|[I4XXIQ](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4XXIQ)|【资料】【file_api部件】文件系统增强接口文档|标准系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
|222|[I4XXIR](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4XXIR)|【新增特性】支持目录环境能力需求|标准系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
|223|[I4XXIT](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4XXIT)|【新增特性】支持应用文件分享能力|标准系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
|224|[I4XXIU](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4XXIU)|【资料】【user_file_manager部件】file picker需要的JS API文件接口文档|标准系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
|225|[I4XXIW](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4XXIW)|【新增特性】支持File Manager Service文件操作实现需求|标准系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
|226|[I4XXIX](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4XXIX)|【新增特性】支持file picker需要的JS API文件接口需求|标准系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
|227|[I4XXIY](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4XXIY)|【新增特性】支持应用占用空间统计特性|标准系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
|228|[I4XXIZ](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4XXIZ)|【新增特性】支持应用cache统计特性|标准系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
|229|[I4XXJ2](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4XXJ2)|【SysCap】文件管理子系统支持SysCap机制|标准系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
|230|[I4XXJ4](https://gitee.com/openharmony/accessibility/issues/I4XXJ4)|【SysCap】无障碍软件服务子系统支持SysCap机制|标准系统|SIG_BasicSoftwareService|[@bj1010](https://gitee.com/bj1010)|
|231|[I4XXJ5](https://gitee.com/openharmony/developtools_profiler/issues/I4XXJ5)|【wukong部件】UI随机压测工具|标准系统|SIG_R&DToolChain|[@wangzaishang](https://gitee.com/wangzaishang)|
|232|[I4XXJ6](https://gitee.com/openharmony/developtools_profiler/issues/I4XXJ6)|【hiperf部件】开发者使用资料开发|标准系统|SIG_R&DToolChain|[@wangzaishang](https://gitee.com/wangzaishang)|
|233|[I4XXJA](https://gitee.com/openharmony/useriam_user_auth/issues/I4XXJA)|【部件化专项】【coauth部件】部件标准化|标准系统|SIG_Security|[@wangxu43](https://gitee.com/wangxu43)|
|234|[I4XXJB](https://gitee.com/openharmony/build/issues/I4XXJB)|【增强特性】Builtin Stub功能汇编化|标准系统|SIG_CompileRuntime|[@wangxing-hw](https://gitee.com/wangxing-hw)|
|235|[I4XXJC](https://gitee.com/openharmony/build/issues/I4XXJC)|【SysCap】语言编译器运行时子系统支持SysCap机制|标准系统|SIG_CompileRuntime|[@wangxing-hw](https://gitee.com/wangxing-hw)|
|236|[I4XXJD](https://gitee.com/openharmony/build/issues/I4XXJD)|【新增特性】 [JS-Runtime]BigInt数据类型支持|标准系统|SIG_CompileRuntime|[@wangxing-hw](https://gitee.com/wangxing-hw)|
|237|[I4XXJE](https://gitee.com/openharmony/aafwk_standard/issues/I4XXJE)|【增强特性】运行管理API补齐|标准系统|SIG_ApplicationFramework|[@gwang2008](https://gitee.com/gwang2008)|
|238|[I4XXJF](https://gitee.com/openharmony/aafwk_standard/issues/I4XXJF)|【增强特性】ParticleAbility能力补齐|标准系统|SIG_ApplicationFramework|[@gwang2008](https://gitee.com/gwang2008)|
|239|[I4XXJG](https://gitee.com/openharmony/aafwk_standard/issues/I4XXJG)|【增强特性】DataAbilityHelper能力补齐|标准系统|SIG_ApplicationFramework|[@gwang2008](https://gitee.com/gwang2008)|
|240|[I4XXJH](https://gitee.com/openharmony/aafwk_standard/issues/I4XXJH)|【增强特性】上下文能力补齐|标准系统|SIG_ApplicationFramework|[@gwang2008](https://gitee.com/gwang2008)|
|241|[I4XXJI](https://gitee.com/openharmony/aafwk_standard/issues/I4XXJI)|【SysCap】元能力子系统支持SysCap机制|标准系统|SIG_ApplicationFramework|[@gwang2008](https://gitee.com/gwang2008)|
|242|[I4XXJJ](https://gitee.com/openharmony/account_os_account/issues/I4XXJJ)|【SysCap】账号子系统支持SysCap机制|标准系统|SIG_BasicSoftwareService|[@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)|
|243|[I4XXJL](https://gitee.com/openharmony/applications_mms/issues/I4XXJL)|【短信】资料|标准系统|SIG_SystemApplication|[@starr666](https://gitee.com/starr666)|
|244|[I4XXJM](https://gitee.com/openharmony/applications_mms/issues/I4XXJM)|【短信】信息管理 - 信息删除|标准系统|SIG_SystemApplication|[@starr666](https://gitee.com/starr666)|
|245|[I4XXJO](https://gitee.com/openharmony/applications_mms/issues/I4XXJO)|【短信】DFX - 日志|标准系统|SIG_SystemApplication|[@starr666](https://gitee.com/starr666)|
|246|[I4XXJP](https://gitee.com/openharmony/applications_mms/issues/I4XXJP)|【短信】全球化 - 全球化，多语言支持|标准系统|SIG_SystemApplication|[@starr666](https://gitee.com/starr666)|
|247|[I4XXJQ](https://gitee.com/openharmony/applications_mms/issues/I4XXJQ)|【短信】信息管理 - 信息查看|标准系统|SIG_SystemApplication|[@starr666](https://gitee.com/starr666)|
|248|[I4XXJR](https://gitee.com/openharmony/applications_photos/issues/I4XXJR)|【图库】大图浏览-收藏操作|标准系统|SIG_SystemApplication|[@sunjunxiong](https://gitee.com/sunjunxiong)|
|249|[I4XXJT](https://gitee.com/openharmony/applications_photos/issues/I4XXJT)|【图库】资料需求|标准系统|SIG_SystemApplication|[@sunjunxiong](https://gitee.com/sunjunxiong)|
|250|[I4XXJU](https://gitee.com/openharmony/applications_photos/issues/I4XXJU)|【图库】全局菜单|标准系统|SIG_SystemApplication|[@sunjunxiong](https://gitee.com/sunjunxiong)|
|251|[I4XXJV](https://gitee.com/openharmony/applications_photos/issues/I4XXJV)|【图库】沉浸式特性|标准系统|SIG_SystemApplication|[@sunjunxiong](https://gitee.com/sunjunxiong)|
|252|[I4XXJW](https://gitee.com/openharmony/applications_photos/issues/I4XXJW)|【图库】相册特性-相册显示|标准系统|SIG_SystemApplication|[@sunjunxiong](https://gitee.com/sunjunxiong)|
|253|[I4XXJY](https://gitee.com/openharmony/applications_photos/issues/I4XXJY)|【图库】相册特性-基础管理|标准系统|SIG_SystemApplication|[@sunjunxiong](https://gitee.com/sunjunxiong)|
|254|[I4XXJZ](https://gitee.com/openharmony/applications_photos/issues/I4XXJZ)|【图库】相册特性-最近删除相册|标准系统|SIG_SystemApplication|[@sunjunxiong](https://gitee.com/sunjunxiong)|
|255|[I4XXK1](https://gitee.com/openharmony/applications_photos/issues/I4XXK1)|【图库】大图浏览-基础浏览&操作|标准系统|SIG_SystemApplication|[@sunjunxiong](https://gitee.com/sunjunxiong)|
|256|[I4XXK2](https://gitee.com/openharmony/applications_photos/issues/I4XXK2)|【图库】大图浏览-视频播放|标准系统|SIG_SystemApplication|[@sunjunxiong](https://gitee.com/sunjunxiong)|
|257|[I4XXK3](https://gitee.com/openharmony/applications_photos/issues/I4XXK3)|【图库】大图浏览-其他操作|标准系统|SIG_SystemApplication|[@sunjunxiong](https://gitee.com/sunjunxiong)|
|258|[I4XXK4](https://gitee.com/openharmony/applications_photos/issues/I4XXK4)|【图库】分布式特性|标准系统|SIG_SystemApplication|[@sunjunxiong](https://gitee.com/sunjunxiong)|
|259|[I4XXK5](https://gitee.com/openharmony/applications_photos/issues/I4XXK5)|【图库】外部交互特性|标准系统|SIG_SystemApplication|[@sunjunxiong](https://gitee.com/sunjunxiong)|
|260|[I4XXK6](https://gitee.com/openharmony/applications_photos/issues/I4XXK6)|【图库】照片特性|标准系统|SIG_SystemApplication|[@sunjunxiong](https://gitee.com/sunjunxiong)|
|261|[I4XXK9](https://gitee.com/openharmony/applications_photos/issues/I4XXK9)|【图库】宫格特性-基础操作|标准系统|SIG_SystemApplication|[@sunjunxiong](https://gitee.com/sunjunxiong)|
|262|[I4XXKA](https://gitee.com/openharmony/applications_photos/issues/I4XXKA)|【图库】宫格特性-其他操作|标准系统|SIG_SystemApplication|[@sunjunxiong](https://gitee.com/sunjunxiong)|
|263|[I4XXKB](https://gitee.com/openharmony/applications_photos/issues/I4XXKB)|【图库】快速滑动|标准系统|SIG_SystemApplication|[@sunjunxiong](https://gitee.com/sunjunxiong)|
|264|[I4XXKD](https://gitee.com/openharmony/applications_notes/issues/I4XXKD)|【笔记】支持鼠标左右键|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|265|[I4XXKE](https://gitee.com/openharmony/applications_notes/issues/I4XXKE)|【笔记】支持文本编辑|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|266|[I4XXKF](https://gitee.com/openharmony/applications_notes/issues/I4XXKF)|【笔记】基础功能DFX|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|267|[I4XXKG](https://gitee.com/openharmony/applications_notes/issues/I4XXKG)|【笔记】笔记支持分栏|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|268|[I4XXKH](https://gitee.com/openharmony/applications_notes/issues/I4XXKH)|【笔记】笔记搜索能力|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|269|[I4XXKI](https://gitee.com/openharmony/applications_notes/issues/I4XXKI)|【笔记】笔记支持收藏|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|270|[I4XXKJ](https://gitee.com/openharmony/applications_notes/issues/I4XXKJ)|【笔记】移动到其他文件夹|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|271|[I4XXKK](https://gitee.com/openharmony/applications_notes/issues/I4XXKK)|【笔记】笔记支持置顶|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|272|[I4XXKL](https://gitee.com/openharmony/applications_notes/issues/I4XXKL)|【笔记】笔记支持删除|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|273|[I4XXKM](https://gitee.com/openharmony/applications_notes/issues/I4XXKM)|【笔记】笔记列表显示|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|274|[I4XXKO](https://gitee.com/openharmony/applications_notes/issues/I4XXKO)|【笔记】支持分类显示|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|275|[I4XXKQ](https://gitee.com/openharmony/applications_notes/issues/I4XXKQ)|【笔记】支持文件夹管理|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|276|[I4XXKR](https://gitee.com/openharmony/miscservices_inputmethod/issues/I4XXKR)|【新增特性】输入法资料|标准系统|SIG_BasicSoftwareService|[@zhouyongfei](https://gitee.com/zhouyongfei)|
|277|[I4XXKS](https://gitee.com/openharmony/miscservices_inputmethod/issues/I4XXKS)|【新增特性】输入法实现成service|标准系统|SIG_BasicSoftwareService|[@zhouyongfei](https://gitee.com/zhouyongfei)|
|278|[I4XXKU](https://gitee.com/openharmony/miscservices_inputmethod/issues/I4XXKU)|【新增特性】【输入法】文本输入|标准系统|SIG_BasicSoftwareService|[@zhouyongfei](https://gitee.com/zhouyongfei)|
|279|[I4XXKW](https://gitee.com/openharmony/miscservices_inputmethod/issues/I4XXKW)|【新增特性】【输入法】支持物理键盘|标准系统|SIG_BasicSoftwareService|[@zhouyongfei](https://gitee.com/zhouyongfei)|
|280|[I4XXKX](https://gitee.com/openharmony/miscservices_inputmethod/issues/I4XXKX)|【新增特性】【输入法】横屏下布局显示|标准系统|SIG_BasicSoftwareService|[@zhouyongfei](https://gitee.com/zhouyongfei)|
|281|[I4XXKZ](https://gitee.com/openharmony/miscservices_inputmethod/issues/I4XXKZ)|【设置公共数据存储】Settings数据管理API-兼容HarmonyOS API7|标准系统|SIG_BasicSoftwareService|[@zhouyongfei](https://gitee.com/zhouyongfei)|
|282|[I4XXL0](https://gitee.com/openharmony/applications_filepicker/issues/I4XXL0)|【资料】文件选择器-指导资料|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|283|[I4XXL2](https://gitee.com/openharmony/applications_filepicker/issues/I4XXL2)|【DFX】性能、功耗、打点等DFX特性|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|284|[I4XXL3](https://gitee.com/openharmony/applications_filepicker/issues/I4XXL3)|【文件保存路径】文件选择器-提供文件保存至选择路径的能力|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|285|[I4XXL4](https://gitee.com/openharmony/applications_filepicker/issues/I4XXL4)|【文件选择】文件选择器-提供文件选择能力|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|286|[I4XXL7](https://gitee.com/openharmony/applications_call/issues/I4XXL7)|【通话】应用界面ACE2.0适配|标准系统|SIG_SystemApplication|[@jyh926](https://gitee.com/jyh926)|
|287|[I4XXL8](https://gitee.com/openharmony/applications_call/issues/I4XXL8)|【资料】应用子系统通话应用需求|标准系统|SIG_SystemApplication|[@jyh926](https://gitee.com/jyh926)|
|288|[I4XXLA](https://gitee.com/openharmony/applications_call/issues/I4XXLA)|基础功能适配支持DFX|标准系统|SIG_SystemApplication|[@jyh926](https://gitee.com/jyh926)|
|289|[I4XXLD](https://gitee.com/openharmony/applications_call/issues/I4XXLD)|移动数据设置基础功能|标准系统|SIG_SystemApplication|[@jyh926](https://gitee.com/jyh926)|
|290|[I4XXLE](https://gitee.com/openharmony/applications_call/issues/I4XXLE)|基础功能适配支持全球化|标准系统|SIG_SystemApplication|[@jyh926](https://gitee.com/jyh926)|
|291|[I4XXLG](https://gitee.com/openharmony/applications_call/issues/I4XXLG)|来电支持接听拒接基本操作|标准系统|SIG_SystemApplication|[@jyh926](https://gitee.com/jyh926)|
|292|[I4XXLH](https://gitee.com/openharmony/applications_camera/issues/I4XXLH)|【相机】资料需求|标准系统|SIG_SystemApplication|[@blancwu](https://gitee.com/blancwu)|
|293|[I4XXLJ](https://gitee.com/openharmony/applications_camera/issues/I4XXLJ)|【相机】基础架构-DFX|标准系统|SIG_SystemApplication|[@blancwu](https://gitee.com/blancwu)|
|294|[I4XXLM](https://gitee.com/openharmony/applications_camera/issues/I4XXLM)|【相机】基础架构-动效|标准系统|SIG_SystemApplication|[@blancwu](https://gitee.com/blancwu)|
|295|[I4XXLP](https://gitee.com/openharmony/applications_camera/issues/I4XXLP)|【相机】基础架构-统一架构|标准系统|SIG_SystemApplication|[@blancwu](https://gitee.com/blancwu)|
|296|[I4XXLQ](https://gitee.com/openharmony/applications_camera/issues/I4XXLQ)|【相机】基础架构-屏幕形态适配|标准系统|SIG_SystemApplication|[@blancwu](https://gitee.com/blancwu)|
|297|[I4XXLR](https://gitee.com/openharmony/applications_camera/issues/I4XXLR)|【相机】基础架构-多语言适配|标准系统|SIG_SystemApplication|[@blancwu](https://gitee.com/blancwu)|
|298|[I4XXLS](https://gitee.com/openharmony/applications_camera/issues/I4XXLS)|【相机】缩略图显示和跳转|标准系统|SIG_SystemApplication|[@blancwu](https://gitee.com/blancwu)|
|299|[I4XXLT](https://gitee.com/openharmony/applications_camera/issues/I4XXLT)|【相机】三方调用|标准系统|SIG_SystemApplication|[@blancwu](https://gitee.com/blancwu)|
|300|[I4XXLX](https://gitee.com/openharmony/applications_camera/issues/I4XXLX)|【相机】后置普通拍照|标准系统|SIG_SystemApplication|[@blancwu](https://gitee.com/blancwu)|
|301|[I4XXLY](https://gitee.com/openharmony/applications_camera/issues/I4XXLY)|【相机】后置普通录像|标准系统|SIG_SystemApplication|[@blancwu](https://gitee.com/blancwu)|
|302|[I4XXM0](https://gitee.com/openharmony/applications_contacts/issues/I4XXM0)|【联系人】DFX - 日志|标准系统|SIG_SystemApplication|[@starr666](https://gitee.com/starr666)|
|303|[I4XXM1](https://gitee.com/openharmony/applications_contacts/issues/I4XXM1)|【联系人】资料|标准系统|SIG_SystemApplication|[@starr666](https://gitee.com/starr666)|
|304|[I4XXM2](https://gitee.com/openharmony/applications_contacts/issues/I4XXM2)|【联系人】拨号盘 - 号码格式化显示（空格区分等）|标准系统|SIG_SystemApplication|[@starr666](https://gitee.com/starr666)|
|305|[I4XXM3](https://gitee.com/openharmony/applications_contacts/issues/I4XXM3)|【联系人】拨号盘 - 按键音|标准系统|SIG_SystemApplication|[@starr666](https://gitee.com/starr666)|
|306|[I4XXM4](https://gitee.com/openharmony/applications_settings/issues/I4XXM4)|【设置】显示管理-分辨率设置|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|307|[I4XXM5](https://gitee.com/openharmony/applications_settings/issues/I4XXM5)|【设置】外部入口-权限管理|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|308|[I4XXM6](https://gitee.com/openharmony/applications_settings/issues/I4XXM6)|【设置】资料需求|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|309|[I4XXM8](https://gitee.com/openharmony/applications_settings/issues/I4XXM8)|【设置】系统-语言设置|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|310|[I4XXM9](https://gitee.com/openharmony/applications_settings/issues/I4XXM9)|【设置】开发者模式|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|311|[I4XXMB](https://gitee.com/openharmony/applications_settings/issues/I4XXMB)|【设置】WIFI管理|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|312|[I4XXMC](https://gitee.com/openharmony/applications_settings/issues/I4XXMC)|【设置】蓝牙管理|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|313|[I4XXMD](https://gitee.com/openharmony/applications_settings/issues/I4XXMD)|【设置】密码管理|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|314|[I4XXMG](https://gitee.com/openharmony/applications_settings/issues/I4XXMG)|【设置】应用管理|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|315|[I4XXMH](https://gitee.com/openharmony/applications_settings/issues/I4XXMH)|【设置】存储空间信息|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|316|[I4XXMI](https://gitee.com/openharmony/applications_settings/issues/I4XXMI)|【设置】定位服务开关|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|317|[I4XXMJ](https://gitee.com/openharmony/applications_settings/issues/I4XXMJ)|【设置】用户管理|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|318|[I4XXMK](https://gitee.com/openharmony/applications_settings/issues/I4XXMK)|【设置】系统-恢复出厂|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|319|[I4XXML](https://gitee.com/openharmony/applications_settings/issues/I4XXML)|【设置】基础能力-搜索|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|320|[I4XXMM](https://gitee.com/openharmony/applications_systemui/issues/I4XXMM)|【资料】【SystemUI】支持子系统应用资料类需求|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|321|[I4XXMO](https://gitee.com/openharmony/applications_systemui/issues/I4XXMO)|【SystemUI】【状态-控制开关】支持控制开关-蓝牙|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|322|[I4XXMQ](https://gitee.com/openharmony/applications_systemui/issues/I4XXMQ)|【SystemUI】【状态-控制开关】支持控制开关-响铃|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|323|[I4XXMR](https://gitee.com/openharmony/applications_systemui/issues/I4XXMR)|【SystemUI】【状态栏】支持状态栏图标-时间|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|324|[I4XXMS](https://gitee.com/openharmony/applications_systemui/issues/I4XXMS)|【SystemUI】【状态-控制开关】支持控制开关-位置信息|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|325|[I4XXMT](https://gitee.com/openharmony/applications_systemui/issues/I4XXMT)|【SystemUI】【通知】分布式通知|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|326|[I4XXMU](https://gitee.com/openharmony/applications_systemui/issues/I4XXMU)|【SystemUI】【通知管理】通知管理设置|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|327|[I4XXMV](https://gitee.com/openharmony/applications_systemui/issues/I4XXMV)|【SystemUI】【状态栏】状态栏-可配置接入|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|328|[I4XXMX](https://gitee.com/openharmony/applications_systemui/issues/I4XXMX)|【SystemUI】【状态-控制开关】支持控制开关-Wifi|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|329|[I4XXMY](https://gitee.com/openharmony/applications_systemui/issues/I4XXMY)|【SystemUI】【状态栏图标】沉浸式|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|330|[I4XXMZ](https://gitee.com/openharmony/applications_systemui/issues/I4XXMZ)|【SystemUI】【控制开关】控制中心面板及交互|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|331|[I4XXN0](https://gitee.com/openharmony/applications_systemui/issues/I4XXN0)|【SystemUI】支持多用户|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|332|[I4XXN1](https://gitee.com/openharmony/applications_systemui/issues/I4XXN1)|【SystemUI】支持键鼠操作|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|333|[I4XXN2](https://gitee.com/openharmony/applications_systemui/issues/I4XXN2)|【SystemUI】【通知】通知图标/属性处理|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|334|[I4XXN3](https://gitee.com/openharmony/applications_systemui/issues/I4XXN3)|【SystemUI】【通知】通知发送权限管理|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|335|[I4XXN5](https://gitee.com/openharmony/applications_systemui/issues/I4XXN5)|【SystemUI】【通知】通知支持扩展模板|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|336|[I4XXN6](https://gitee.com/openharmony/applications_systemui/issues/I4XXN6)|【SystemUI】【基础框架】SystemUI状态栏/导航栏窗属性可配置|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|337|[I4XXN7](https://gitee.com/openharmony/applications_systemui/issues/I4XXN7)|【SystemUI】【控制开关】截屏功能|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|338|[I4XXN9](https://gitee.com/openharmony/applications_systemui/issues/I4XXN9)|【SystemUI】【控制开关】亮度调节|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|339|[I4XXNA](https://gitee.com/openharmony/applications_systemui/issues/I4XXNA)|【SystemUI】支持音量面板|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|340|[I4XXNB](https://gitee.com/openharmony/applications_systemui/issues/I4XXNB)|【SystemUI】支持通知样式适配|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|341|[I4XXNC](https://gitee.com/openharmony/applications_systemui/issues/I4XXNC)|【SystemUI】【基础框架】SystemUI启动与显示|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|342|[I4XXND](https://gitee.com/openharmony/applications_systemui/issues/I4XXND)|【DFX】性能、功耗、日志等DFX特性|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|343|[I4XXNT](https://gitee.com/openharmony/applications_launcher/issues/I4XXNT)|【资料】应用子系统桌面应用需求|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|344|[I4XXNV](https://gitee.com/openharmony/applications_launcher/issues/I4XXNV)|【DFX】性能、功耗、安全、兼容性设计|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|345|[I4XXNW](https://gitee.com/openharmony/applications_launcher/issues/I4XXNW)|【鼠标适配】【动效】鼠标操作相关动效|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|346|[I4XXNY](https://gitee.com/openharmony/applications_launcher/issues/I4XXNY)|【鼠标适配】鼠标适配|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|347|[I4XXNZ](https://gitee.com/openharmony/applications_launcher/issues/I4XXNZ)|【手势导航】支持全局手势导航|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|348|[I4XXO0](https://gitee.com/openharmony/applications_launcher/issues/I4XXO0)|【桌面设置】桌面设置基础功能|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|349|[I4XXO2](https://gitee.com/openharmony/applications_launcher/issues/I4XXO2)|【Dock栏】【动效】Dock栏动效|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|350|[I4XXO3](https://gitee.com/openharmony/applications_launcher/issues/I4XXO3)|【Dock栏】Dock栏基础能力|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|351|[I4XXO4](https://gitee.com/openharmony/applications_launcher/issues/I4XXO4)|【布局管理】多设备布局自适应、横竖屏切换管理等布局管理能力|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|352|[I4XXO5](https://gitee.com/openharmony/applications_launcher/issues/I4XXO5)|【未读数字角标】【2D】未读数字角标管理|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|353|[I4XXO6](https://gitee.com/openharmony/applications_launcher/issues/I4XXO6)|【快捷方式Shortcut】Shortcut PopWindow显示等基础功能|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|354|[I4XXO8](https://gitee.com/openharmony/applications_launcher/issues/I4XXO8)|【应用图标】应用图标显示和点击图标触发应用启动|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|355|[I4XXOA](https://gitee.com/openharmony/applications_launcher/issues/I4XXOA)|【应用中心】【动效】全量应用列表基础功能|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|356|[I4XXOD](https://gitee.com/openharmony/applications_launcher/issues/I4XXOD)|【应用中心】全量应用列表基础功能|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|357|[I4XXOE](https://gitee.com/openharmony/applications_launcher/issues/I4XXOE)|【普通多任务】多任务卡片列表|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|358|[I4XXOF](https://gitee.com/openharmony/applications_launcher/issues/I4XXOF)|【普通多任务】【动效】多任务动效|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|359|[I4XXOG](https://gitee.com/openharmony/applications_launcher/issues/I4XXOG)|【智能文件夹】【动效】智能文件夹相关动效|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|360|[I4XXOI](https://gitee.com/openharmony/applications_launcher/issues/I4XXOI)|【智能文件夹】智能文件夹显示、合并等基础功能|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|361|[I4XXOJ](https://gitee.com/openharmony/applications_launcher/issues/I4XXOJ)|【万能卡片基本功能】【动效】卡片相关动效|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|362|[I4XXOK](https://gitee.com/openharmony/applications_launcher/issues/I4XXOK)|【万能卡片基本功能】卡片的呼出、关闭、固定、编辑等基础能力|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|363|[I4XXOL](https://gitee.com/openharmony/applications_settings/issues/I4XXOL)|【设置】系统-设备名称|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|364|[I4Y0ZI](https://gitee.com/openharmony/distributed_camera/issues/I4Y0ZI)|【新增特性】分布式相机性能规格|标准系统|SIG_DistributedHardwareManagement|[@hwzhangchuang](https://gitee.com/hwzhangchuang)|
|365|[I4YLKY](https://gitee.com/openharmony-tpc/XmlGraphicsBatikETS/issues/I4YLKY)|【三四方库】JSON处理|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|366|[I4YLKZ](https://gitee.com/openharmony-tpc/XmlGraphicsBatikETS/issues/I4YLKZ)|【三四方库】xmlgraphics-batik|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|367|[I4YLL0](https://gitee.com/openharmony-tpc/XmlGraphicsBatikETS/issues/I4YLL0)|【三四方库】aws-sdk|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|368|[I4YLL1](https://gitee.com/openharmony-tpc/XmlGraphicsBatikETS/issues/I4YLL1)|【三四方库】commons-codec|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|369|[I4YLL2](https://gitee.com/openharmony-tpc/arouter-api-onActivityResult/issues/I4YLL2)|【三四方库】arouter-api-onActivitResult|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|370|[I4YLL3](https://gitee.com/openharmony-tpc/MMKV/issues/I4YLL3)|【三四方库】MMKV|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|371|[I4YLL4](https://gitee.com/openharmony-tpc/MMKV/issues/I4YLL4)|【三四方库】ireader/sdk|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|372|[I4YLL5](https://gitee.com/openharmony-tpc/MMKV/issues/I4YLL5)|【三四方库】firebase|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|373|[I4YLL6](https://gitee.com/openharmony-tpc/CommonsCompressEts/issues/I4YLL6)|【三四方库】eventbus|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|374|[I4YLL7](https://gitee.com/openharmony-tpc/CommonsCompressEts/issues/I4YLL7)|【三四方库】压缩/解压缩|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|375|[I4YLL8](https://gitee.com/openharmony-tpc/CommonsCompressEts/issues/I4YLL8)|【三四方库】扫描/解析二维码|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|376|[I4YLL9](https://gitee.com/openharmony-tpc/VCard/issues/I4YLL9)|【三四方库】网络请求|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|377|[I4YLLA](https://gitee.com/openharmony-tpc/VCard/issues/I4YLLA)|【三四方库】Vcard|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|378|[I4YLLC](https://gitee.com/openharmony-tpc/VCard/issues/I4YLLC)|【三四方库】图片处理|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|379|[I4YLLD](https://gitee.com/openharmony-tpc/lottieETS/issues/I4YLLD)|【三四方库】动画处理|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|380|[I4YLLE](https://gitee.com/openharmony-tpc/flexsearch-ohos/issues/I4YLLE)|【三四方库】RxJS|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|381|[I4YLLF](https://gitee.com/openharmony-tpc/flexsearch-ohos/issues/I4YLLF)|【三四方库】Lucene|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|382|[I4YLLG](https://gitee.com/openharmony-tpc/flexsearch-ohos/issues/I4YLLG)|【三四方库】LibphoneNumber|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|383|[I4YLLH](https://gitee.com/openharmony-tpc/flexsearch-ohos/issues/I4YLLH)|【三四方库】拼音处理|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|384|[I4YLLI](https://gitee.com/openharmony-tpc/flexsearch-ohos/issues/I4YLLI)|【三四方库】其他TOP200应用依赖的三方库|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|385|[I4YLLJ](https://gitee.com/openharmony-tpc/rebound/issues/I4YLLJ)|【三四方库】Rocket.Chat.ohos|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|386|[I4YLLK](https://gitee.com/openharmony-tpc/thrift/issues/I4YLLK)|【三四方库】rebound|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|387|[I4YLLL](https://gitee.com/openharmony-tpc/LargeImage/issues/I4YLLL)|【三四方库】thrift|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|388|[I4YLLM](https://gitee.com/openharmony-tpc/LargeImage/issues/I4YLLM)|【三四方库】LargeImage|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|389|[I4YLLN](https://gitee.com/openharmony-tpc/LargeImage/issues/I4YLLN)|【三四方库】mixpanel-ohos|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
|390|[I4YLLP](https://gitee.com/openharmony-tpc/LargeImage/issues/I4YLLP)|【三四方库】realm|标准系统|SIG_ApplicationFramework|[@andyhm10000](https://gitee.com/andyhm10000)|
| 391 | [I4R2ZX](https://gitee.com/openharmony/windowmanager/issues/I4R2ZX) | 【window_manager】【新增特性】支持跨窗口拖拽框架 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 392 | [I4R9OY](https://gitee.com/openharmony/windowmanager/issues/I4R9OY) | 【window_manager】【新增规格】增强特性：提供虚拟屏幕 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 393 | [I4R9P3](https://gitee.com/openharmony/windowmanager/issues/I4R9P3) | 【DFX】窗口绘制超时检测/窗口焦点故障检测 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 394 | [I4R30E](https://gitee.com/openharmony/windowmanager/issues/I4R30E) | 【资料】提供窗口管理子系统资料说明 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 395 | [I4RKSS](https://gitee.com/openharmony/graphic_standard/issues/I4RKSS) | 【drawing部件】提供二次绘制能力 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
| 396 | [I4RKSU](https://gitee.com/openharmony/graphic_standard/issues/I4RKSU) | 【资料】提供图形drawing部件、composer部件、backstore部件的相关资料 | 标准系统 | SIG-Graphics | [@lz-230](https://gitee.com/lz-230) |
>  


###### 以上计划由OpenHarmony社区版本发布SIG组织发布
